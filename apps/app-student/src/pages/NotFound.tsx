import { LogoPrimary } from "@work-transfert/ui";

export const NotFound = () => {
    return ( 
        <div className="not-found">
            <LogoPrimary />
            <h1 className="">
                Page introuvable
            </h1>
        </div>
     );
}