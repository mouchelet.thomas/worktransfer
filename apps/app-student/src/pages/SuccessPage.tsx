import { useEffect, useState } from "react";
import uploadSuccess from "../assets/lottie/success.json"
import Lottie from 'react-lottie-player'
import Confetti from 'react-dom-confetti'
import { useParams } from "react-router-dom";
import { DelivrableService } from "@work-transfert/services";
import { Button } from "@work-transfert/ui";
import { useDelivrableUploadFile } from "@work-transfert/hooks";
import { ErrorResponse } from '@work-transfert/types';
import toast from 'react-hot-toast';

const config = {
    angle: 90,
    spread: 360,
    startVelocity: 23,
    elementCount: 170,
    dragFriction: 0.12,
    duration: 5000,
    stagger: 3,
    width: "10px",
    height: "10px",
    perspective: "500px",
    colors: ["#a864fd", "#29cdff", "#78ff44", "#ff718d", "#fdff6a"]
  };

export const SuccessPage = () => {
    const [isConfetti, setIsConfetti] = useState(false)
    const [delivrable, setDelivrable] = useState<any>(null)
    const { id } = useParams<{ id: string }>();
    const { handleDownload } = useDelivrableUploadFile()

    useEffect(() => {
        setTimeout(() => {
            setIsConfetti(true)
        }, 200);
        if(id) fetchDelivrable()
    }, [])

    const fetchDelivrable = async () => {
        if(!id) return 

        try {
            const response = await DelivrableService.findOne(id)
            setDelivrable(response)
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    return ( 
        <div className="mt-5 container">
            <div className="row justify-content-center">
                <div className="col-md-4">
                    <div className="p-5 success--header">
                        <Lottie
                            animationData={uploadSuccess}
                            play
                            loop={false}
                            style={{ width: 250, height: 250 }}
                        />
                        <p className="typography-caption typography-caption--large">
                            {delivrable?.project?.name}
                        </p>
                        <Confetti active={ isConfetti } config={ config }/>
                        <h1>Envoyé avec succès</h1>
                        <Button
                            primary={true}
                            label="Télécharger mon travail"
                            onClick={(e) => handleDownload(e, delivrable.delivrableUploadFile.id)}
                            size="large"
                        />
                        {/* <Button
                            label="Supprimer mon travail"
                            onClick={handleDelete}
                            className="mt-2"
                            color="danger"
                            size="large"
                        /> */}
                        <Button
                            label="Envoyer un autre travail"
                            to={`/projects/${delivrable?.project?.id}`}
                            className="mt-2"
                            size="large"
                        />
                    </div>
                </div>
            </div>
        </div>
     );
}