import "@work-transfert/styles/src/main.scss"
import { StepFormUploadDelivrable } from "./components/stepForm/StepFormUploadDelivrable"
import { BrowserRouter, Route, Routes } from "react-router-dom"
import { NotFound } from "./pages/NotFound"
import { SuccessPage } from "./pages/SuccessPage"

function App() {
  
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/projects/:id" element={<StepFormUploadDelivrable />} />
        <Route path="/success/:id" element={<SuccessPage />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter >
  )
}

export default App
