import { DropZonePrimary } from "@work-transfert/ui";

interface Step1Props {
    credentials: any
    setCredentials: any
    nextStep: any
}

export const Step1 = ({
    setCredentials,
    credentials,nextStep
}: Step1Props) => {

    const handleUpload = (files: FileList) => {
        const newCredentials = {
            ...credentials,
            file: files[0]
        }
        setCredentials(newCredentials);
        nextStep(newCredentials);
    }

    return ( 
        <div>
            <DropZonePrimary
                handleUpload={handleUpload}
                acceptedFileTypes=".zip,.rar,.7zip,.7z,.tar,.tar.gz,.gz"
                size="medium"
            />
        </div>
     );
}