import { useEffect, useState } from "react";
import { Step1 } from "./Step1";
import { Step2 } from "./Step2";
import { Alert, Button} from "@work-transfert/ui"
import { DelivrableService, ProjectService } from "@work-transfert/services"
import { useNavigate, useParams } from "react-router-dom";
import toast from 'react-hot-toast';
import { ErrorResponse } from '@work-transfert/types';

type credentialsStudentUploadDelivrable = {
    firstName: string;
    lastName: string;
    file: File;
}

type errorType = {
    message: string;
}

export const StepFormUploadDelivrable = () => {
    const [credentials, setCredentials] = useState({} as credentialsStudentUploadDelivrable);
    const [currentStep, setCurrentStep] = useState(1);
    const { id } = useParams<{ id: string }>();
    const [project, setProject] = useState({} as any);
    const navigate = useNavigate()
    const [onLoadingSubmit, setOnLoadingSubmit] = useState(false)
    const [error, setError] = useState<errorType | boolean>(false)

    useEffect(() => {
        if(id) fetchProject()
    }, [id])

    const checkIsValid = (newCredentials: credentialsStudentUploadDelivrable) => {
        switch (currentStep) {
            case 1:
                if(!newCredentials.file) {
                    toast.error("Veuillez sélectionner un fichier")
                    return false
                }
                break;
            case 2:
                if(!newCredentials.firstName || !newCredentials.lastName) {
                    toast.error("Veuillez remplir tous les champs")
                    return false
                }
                break;
        }
        return true
    }
    
    const nextStep = (newCredentials: credentialsStudentUploadDelivrable) => {
        if(currentStep >= steps.length) return;
        if(!checkIsValid(newCredentials)) return;
        setCurrentStep(currentStep + 1);
        setError(false)  
    }

    const prevStep = () => {
        if(currentStep <= 1) return;
        setCurrentStep(currentStep - 1);
    }

    const fetchProject = async () => {
        if(!id) return 

        try {
            const response = await ProjectService.findOne(id)
            setProject(response)
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    const createStepComponent = (Component: React.ComponentType<any>, props?: any) => (
          <Component
            {...props || {}}
            setCredentials={setCredentials}
            credentials={credentials}
            nextStep={nextStep}
          />
      );
      
      const steps = [
        {
          component: createStepComponent(Step1),
        },
        {
          component: createStepComponent(Step2),
        },
      ];

      const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if(!id) return;
        if(!checkIsValid(credentials)) return;
        setOnLoadingSubmit(true)

        try {
            const response = await DelivrableService.create(id, credentials)
            navigate(`/success/${response.id}`)
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
            setCurrentStep(1)
        } finally {
            setOnLoadingSubmit(false)
        }
      }

    return ( 
        <div>
            {error && typeof error === "object" && (
                <Alert
                    variant="error"
                    message={error?.message || "Une erreur est survenue"}
                />
            )}

            <form className="step-form" onSubmit={handleSubmit}>
                <div className="step-form--header mb-5">
                    <h1>
                        {project.name} 
                    </h1>
                    <p className="typography-caption typography-caption--large">
                        <span>
                            {project.subject?.name}
                        </span>

                        {project.studentClasses?.map((studentClass: any) => (
                            <span key={studentClass.id} className="ml-2">
                                {studentClass.name}
                            </span>
                        ))}
                    </p>
                    <p className="typography-caption typography-caption--large">
                        {project.teacherSchool?.teacher?.firstName} {project.teacherSchool?.teacher?.lastName}
                    </p>
                </div>

                <div className="step-form--container">
                    {steps[currentStep - 1].component}
                </div>

                <div className="step-form--navbar mt-5">
                    <Button
                        label="Précédent"
                        onClick={() => prevStep()}
                        disabled={currentStep === 1}
                        size="large"
                    />
                    <Button
                        primary={true}
                        type={currentStep === steps.length ? 'submit' : 'button'}
                        label={currentStep === steps.length ? 'Terminer' : 'Suivant'}
                        onClick={() => nextStep(credentials)}
                        onLoadingSubmit={onLoadingSubmit}
                        size="large"
                    />
                </div>
            </form>
        </div>
     );
}