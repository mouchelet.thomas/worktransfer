import React from 'react';
import { Input } from "@work-transfert/ui";

interface Step2Props {
    credentials: any
    setCredentials: any
    nextStep: any
}

export const Step2 = ({
    credentials,
    setCredentials,
    nextStep
}: Step2Props) => {
    return ( 
        <div>
            <div className="form-group mb-2">
                <Input 
                    placeholder="Prénom"
                    name="firstName"
                    type="text"
                    value={credentials.firstName || ''}
                    credentials={credentials}
                    setCredentials={setCredentials}
                />
            </div>
            <div className="form-group mb-2">
                <Input 
                    placeholder="Nom de famille"
                    name="lastName"
                    type="text"
                    value={credentials.lastName || ''}
                    credentials={credentials}
                    setCredentials={setCredentials}
                />
            </div>
        </div>
     );
}