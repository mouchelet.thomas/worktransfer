import { Navigate, Outlet } from 'react-router-dom'
import { useAuthenticated } from '@work-transfert/hooks'

export default function ProtectedRoute () {
  const isAuth = useAuthenticated()
  return isAuth ? <Outlet /> : <Navigate to="/auth/signin" />;
}