import { Routes, Route } from 'react-router-dom'
import HomePage from '../pages/HomePage'
import SigninPage from '../pages/auth/SigninPage'
import { ForgotPasswordPage } from '../pages/auth/ForgotPasswordPage'
import { ResetPasswordPage } from '../pages/auth/ResetPasswordPage'
import { SignupPage } from '../pages/auth/SignupPage'

export default function PublicRoutes() {
  return (
    <Routes>
      <Route path="/" element={<SigninPage />} />
      <Route path="/auth/signin" element={<SigninPage />} />
      <Route path="/auth/signup" element={<SignupPage />} />
      <Route path='/auth/forgot-password' element={<ForgotPasswordPage />} />
      <Route path='/auth/reset-password/:token' element={<ResetPasswordPage />} />
      <Route path="*" element={<SigninPage />} />
    </Routes>
  )
}