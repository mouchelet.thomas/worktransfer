import { useAuthenticated } from "@work-transfert/hooks";
import PrimaryLayout from "../layout/PrimaryLayout";
import ProtectedRoutes from "./ProtctedRoutes";
import PublicRoutes from "./PublicRoutes";

export default function MainRoutes() {
    const isAuthenticated = useAuthenticated();
    
    return (
       <>
            {!isAuthenticated ?
                <PublicRoutes />
            : 
                <PrimaryLayout>
                    <ProtectedRoutes />
                </PrimaryLayout>
            }
       </>
    )
}
