import { Routes, Route } from 'react-router-dom'
import ProtectedRoute from './ProtectedRoute'
import DashboardPage from '../pages/DashboardPage'
import { TeachersListPage } from '../pages/teacher/TeachersListPage'
import { TeacherFormPage } from '../pages/teacher/TeacherFormPage'
import SubjectsListPage from '../pages/subject/SubjectsListPage'
import ProjectsListPage from '../pages/project/ProjectsListPage'
import ProjectFormPage from '../pages/project/ProjectFormPage'
import CurriculumsListPage from '../pages/curriculum/CurriculumsListPage'
import { StudentClassListPage } from '../pages/student-class/StudentClassListPage'
import { UserAccount } from '../pages/user/UserAccount'
import { SchoolAcceptForm } from '../pages/school/SchoolAcceptForm'
import { SchoolFormPage } from '../pages/school/SchoolFormPage'

export default function ProtectedRoutes() {
  return (
    <Routes>
      <Route element={<ProtectedRoute />}>
        <Route path="/dashboard" element={<DashboardPage />} />
      </Route>

      <Route element={<ProtectedRoute />}>
        <Route path="/teachers" element={<TeachersListPage />} />
      </Route>

      <Route element={<ProtectedRoute />}>
        <Route path="/teachers/create" element={<TeacherFormPage />} />
      </Route>

      <Route element={<ProtectedRoute />}>
        <Route path="/teachers/:id" element={<TeacherFormPage />} />
      </Route>

      <Route element={<ProtectedRoute />}>
        <Route path="/subjects" element={<SubjectsListPage />} />
      </Route>

      <Route element={<ProtectedRoute />}>
        <Route path="/projects" element={<ProjectsListPage />} />
      </Route>

      <Route element={<ProtectedRoute />}>
        <Route path="/projects/create" element={<ProjectFormPage />} />
      </Route>
      
      <Route element={<ProtectedRoute />}>
        <Route path="/projects/:id" element={<ProjectFormPage />} />
      </Route>

      <Route element={<ProtectedRoute />}>
        <Route path="/curriculums" element={<CurriculumsListPage />} />
      </Route>

      <Route element={<ProtectedRoute />}>
        <Route path="/student-classes" element={<StudentClassListPage />} />
      </Route>

      <Route element={<ProtectedRoute />}>
        <Route path="/account" element={<UserAccount />} />
      </Route>

      <Route element={<ProtectedRoute />}>
        <Route path="/schools/accept" element={<SchoolAcceptForm />} />
      </Route>

      <Route element={<ProtectedRoute />}>
        <Route path="/schools/create" element={<SchoolFormPage />} />
      </Route>

      <Route path="*" element={<DashboardPage />} />
    </Routes>
  )
}