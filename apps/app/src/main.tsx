import ReactDOM from 'react-dom/client'
import App from './App'
import { SchoolContextProvider, UserContextProvider } from "@work-transfert/contexts"

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <UserContextProvider>
    <SchoolContextProvider>
      <App />
    </SchoolContextProvider>
  </UserContextProvider>
)
