import { UserContext } from "@work-transfert/contexts";
import { SubjectService } from "@work-transfert/services";
import { ListItemsCrud, ListItemsCrudSelect } from "@work-transfert/ui";
import { useContext } from "react";

export default function SubjectsListPage() {
    const { user } = useContext(UserContext);

    return (
        <div>
            <ListItemsCrudSelect 
                entityService={SubjectService}
                entity="subject"
                entityLabel="matière"
                allowActions={user.role !== "teacher"}
                inputType="text"
            />
        </div>
     )
}