import { UserContext } from '@work-transfert/contexts';
import { CurriculumService, StudentClassService } from '@work-transfert/services';
import { ListItemsCrud, ListItemsCrudSelect, ListItemsSelect } from '@work-transfert/ui';
import React, { useContext, useState } from 'react';

export const StudentClassListPage = () => {
    const { user } = useContext(UserContext);
    const [credentials, setCredentials] = useState({} as any)
    
    return ( 
        <div>
            <div className="row">
                <div className="col-md-6">
                    <ListItemsCrudSelect 
                        entityService={StudentClassService}
                        entity="studentClass"
                        entityLabel="promotion"
                        allowActions={user.role !== "teacher"}
                        setCredentials={setCredentials}
                        currentItem={credentials}
                        inputType="text"
                    />
                </div>
                {user.role !== "teacher" && 
                    <div className="col-md-6 pl-4">
                        {credentials && credentials.id && 
                            <ListItemsSelect 
                                listEntityService={CurriculumService}
                                updateEntityService={StudentClassService}
                                entityProperty="curriculums"
                                entityLabel="cursus"
                                updateItemId={credentials.id}
                                allowActions={user.role !== "teacher"}
                            />
                        }
                    </div>
                }
            </div>
        </div>
     );
}