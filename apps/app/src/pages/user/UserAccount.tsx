import { UserContext } from "@work-transfert/contexts";
import { AuthService, UserService } from "@work-transfert/services";
import TokenService from "@work-transfert/services/src/token.service";
import { ErrorResponse, UserType } from "@work-transfert/types";
import { Button, Input } from "@work-transfert/ui";
import { FormEvent, useContext, useEffect, useState } from "react";
import toast from 'react-hot-toast';

export const UserAccount = () => {
    const [credentials, setCredentials] = useState({} as UserType)
    const { user, setUser } = useContext(UserContext)
    const [onLoadingSubmit, setOnLoadingSubmit] = useState(false);

    useEffect(() => {
        setCredentials(user)
    }, [user])

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        setOnLoadingSubmit(true);
        e.preventDefault()
        try {
            await UserService.update(credentials)
            const refreshToken = await AuthService.regenerateToken()
            TokenService.setUser(refreshToken)
            const uerLocale = UserService.getCurrentUser()
            if(uerLocale) setUser(uerLocale)
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        } finally {
            setOnLoadingSubmit(false);
        }
    }

    return ( 
        <div>
            <h1>Account</h1>

            <form onSubmit={handleSubmit}>
                <div className="form-group mb-2">
                    <Input 
                        placeholder="email"
                        type="email" 
                        name="email"
                        disabled={true}
                        credentials={credentials}
                        setCredentials={setCredentials}
                    />
                </div>
                <div className="form-group mb-2">
                    <Input 
                        placeholder="password"
                        type="password" 
                        name="password"
                        credentials={credentials}
                        setCredentials={setCredentials}
                    />
                </div>
                
                {credentials.role == "teacher" && (
                    <>
                        <TeacherForm
                            credentials={credentials}
                            setCredentials={setCredentials}
                        />
                    </>
                )}

                <div className="form-group mb-2">
                    <Button 
                        primary={true}
                        type="submit"
                        label="Enregistrer"
                        onLoadingSubmit={onLoadingSubmit}
                    />
                </div>
            </form>
        </div>
     );
}


interface TeacherFormProps {
    credentials: UserType
    setCredentials: (credentials: UserType) => void
}

export const TeacherForm = ({
    credentials,
    setCredentials
}: TeacherFormProps) => {
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCredentials({ 
            ...credentials, 
            teacher: {
                ...credentials.teacher,
                [e.target.name]: e.target.value 
            }
        });
    };

    return (
        <div>
             <div className="form-group mb-2">
                <div className="input-container">
                    <input type="text"
                        placeholder="Prénom"
                        name="firstName"
                        value={credentials?.teacher?.firstName || ''}
                        onChange={handleChange}
                    />
                </div>
            </div>
            <div className="form-group mb-2">
                <div className="input-container">
                    <input type="text"
                        placeholder="Nom de famille"
                        name="lastName"
                        value={credentials?.teacher?.lastName || ''}
                        onChange={handleChange}
                    />
                </div>
            </div>
        </div>
    )
}