import { UserContext } from "@work-transfert/contexts";
import { CurriculumService, SubjectService } from "@work-transfert/services";
import { ListItemsCrud, ListItemsCrudSelect, ListItemsSelect } from "@work-transfert/ui";
import { useContext, useState } from "react";

export const CurriculumsListPage = () => {
    const { user } = useContext(UserContext);
    const [credentials, setCredentials] = useState({} as any)

    return ( 
        <div>
            <div className="row">
                <div className="col-md-6">
                    <ListItemsCrudSelect 
                        entityService={CurriculumService}
                        entity="curriculum"
                        entityLabel="promotion"
                        allowActions={user.role !== "teacher"}
                        setCredentials={setCredentials}
                        currentItem={credentials}
                        inputType="text"
                    />
                </div>
                <div className="col-md-6 pl-4">
                    {credentials && credentials.id && 
                        <ListItemsSelect 
                            listEntityService={SubjectService}
                            updateEntityService={CurriculumService}
                            entityProperty="subjects"
                            entityLabel="matières"
                            updateItemId={credentials.id}
                        />
                    }
                </div>
            </div>
        </div>
     );
}
 
export default CurriculumsListPage;