import { SchoolContext } from "@work-transfert/contexts";
import { SchoolService } from "@work-transfert/services";
import { SchoolType } from "@work-transfert/types";
import { Button, Input } from "@work-transfert/ui";
import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import toast from 'react-hot-toast';
import { ErrorResponse } from '@work-transfert/types';

export const SchoolFormPage = () => {
    const [credentials, setCredentials] = useState({} as SchoolType)
    const [onLoadingSubmit, setOnLoadingSubmit] = useState(false)
    const { fetchSchools } = useContext(SchoolContext);
    const navigate = useNavigate();

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        setOnLoadingSubmit(true)
        
        try {
            await SchoolService.create(credentials)
            fetchSchools()
            navigate("/")
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        } finally {
            setOnLoadingSubmit(false)
        }
    }

    return ( 
        <div>
            <h1 className="mb-4">Ajouter une école</h1>
            
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <Input 
                        placeholder="Nom de l'école"
                        name="name"
                        type="text"
                        credentials={credentials}
                        setCredentials={setCredentials}
                    />
                </div>
                <div className="form-group mt-2">
                    <Button
                        type="submit"
                        label="Ajouter une école"
                        primary={true}
                        className="w-100"
                        size="large"
                        onLoadingSubmit={onLoadingSubmit}
                    />
                </div>
            </form>
        </div>
     );
}