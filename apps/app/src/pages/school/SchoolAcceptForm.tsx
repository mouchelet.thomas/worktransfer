import { SchoolContext } from '@work-transfert/contexts';
import { TeacherSchoolService } from '@work-transfert/services';
import { ErrorResponse, JoinSchoolInvitationType } from '@work-transfert/types';
import { Button, Input } from '@work-transfert/ui';
import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import toast from 'react-hot-toast';

export const SchoolAcceptForm = () => {
    const [credentials, setCredentials] = useState({} as JoinSchoolInvitationType)
    const [onLoadingSubmit, setOnLoadingSubmit] = useState(false)
    const navigate = useNavigate();
    const { fetchSchools } = useContext(SchoolContext);

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        setOnLoadingSubmit(true)
        
        try {
            await TeacherSchoolService.acceptInvitation(credentials)
            fetchSchools()
            navigate("/")
        } catch (error) {
            toast.error((error as ErrorResponse)?.response?.data?.message)
        } finally {
            setOnLoadingSubmit(false);
        }
    }

    return ( 
        <div>
            <h1>Ajouter une école</h1>
            <p className="typography-caption typography-caption--large mb-2">
                Veuillez renseignez votre mail  école et le code de validation reçut sur cette adresse mail.
            </p>

            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <Input 
                        placeholder="Email de l'école"
                        name="teacher_school_email"
                        type="email"
                        credentials={credentials}
                        setCredentials={setCredentials}
                    />
                </div>
                <div className="form-group mt-2">
                    <Input 
                        placeholder="code de validation"
                        name="code"
                        type="text"
                        credentials={credentials}
                        setCredentials={setCredentials}
                    />
                </div>
                <div className="form-group mt-2">
                    <Button
                        type="submit"
                        label="Ajouter une école"
                        primary={true}
                        className="w-100"
                        size="large"
                        onLoadingSubmit={onLoadingSubmit}
                    />
                </div>
            </form>
        </div>
     );
}