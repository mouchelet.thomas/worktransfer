import { UserContext } from "@work-transfert/contexts";
import { useDelivrableUploadFile } from "@work-transfert/hooks";
import { ProjectService } from "@work-transfert/services";
import { Button, ListItemsCrud } from "@work-transfert/ui";
import { useContext, useEffect, useState } from "react";
import Moment from 'moment';

const ProjectsListPage = () => {
    const { user } = useContext(UserContext);
    const { handleDownloadAll } = useDelivrableUploadFile()
    
    const showLabels = (project: any) => {
        const total = project.delivrables.length

        const formatDate = (date: Date) => {
            return Moment(date).format('DD/MM/YYYY à HH:mm');
        }

        return (
            <>
                <p className="col-md-3">{project.name}</p>
                <p className="col-md-3">{formatDate(project.createdAt)}</p>
                <p className="col-md-3">
                    {total > 0 ? (
                        <Button
                            primary={true}
                            color="primary"
                            size="small"
                            label={`Tout télécharger en zip ${total > 0 ? `(${total})` : ''}`}
                            onClick={(event:React.MouseEvent<HTMLButtonElement>) => handleDownloadAll(event, project.id)}
                            icon={<i className="fa-solid fa-circle-down"></i>}
                        />
                    ) : (
                        <span className="text-muted">Aucun fichier</span>
                    )}
                </p>
            </>
        )
    }

    return ( 
        <div>
            <ListItemsCrud 
                entityService={ProjectService}
                showLabels={showLabels}
                entity="project"
                entityPathLink="projects"
                entityLabel="projet"
                allowActions={user.role === "teacher"}
            />
        </div>
     );
}
 
export default ProjectsListPage;