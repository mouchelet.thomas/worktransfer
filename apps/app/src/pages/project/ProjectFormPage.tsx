import { SchoolContext } from "@work-transfert/contexts";
import { ProjectService, StudentClassService, SubjectService } from "@work-transfert/services";
import { ErrorResponse, ProjectType } from "@work-transfert/types";
import { Button, DelivrableList, Input, ShareLinkProjectButton } from "@work-transfert/ui";
import { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import toast from 'react-hot-toast';

const ProjectFormPage = () => {
    const { id } = useParams();
    const [credentials, setCredentials] = useState({} as ProjectType)
    const navigate = useNavigate();
    const { school } = useContext(SchoolContext);
    const STUDENT_APP_URL = import.meta.env.VITE_STUDENT_APP_URL;
    const [onLoadingSubmit, setOnLoadingSubmit] = useState(false);
    const [studentClasses, setStudentClasses] = useState([] as any[]);
    const [subjects, setSubjects] = useState([] as any[]);

    useEffect(() => {
        if(school) fetchAllStudentClasses()
        if(id) fetchProject()
    }, [id, school]);

    useEffect(() => {
        if(credentials.studentClasses) {
            fetchSubjects()
        }
    }, [credentials.studentClasses])

    const fetchSubjects = async () => {
        try {
            const response = await SubjectService.findAddlSubjectsByStudentClassId(credentials.studentClasses);
            setSubjects(response.data)
            if(credentials.subject) return;
            setCredentials({
                ...credentials,
                subject: response.data[0]?.id
            })
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    const fetchProject = async () => {
        if(!id) return;
        try {
            const response = await ProjectService.findOne(id);
            setCredentials(response);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        setOnLoadingSubmit(true);
        event.preventDefault();
        try {
            const methode = id ? 'update' : 'create';
            const response = await ProjectService[methode](credentials);
            if(methode == "create") navigate(`/projects/${response.id}`);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        } finally {
            setOnLoadingSubmit(false);
        }
    }

    const fetchAllStudentClasses = async () => {
        try {
            const response = await StudentClassService.findAllBySchool(school.id);
            setStudentClasses([...response.data])
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    const handleChangeSubject = (event: any) => {
        let { name, value } = event.currentTarget;
        setCredentials({
            ...credentials,
            [name]: {id: value}
        });
    }

    const handleChangeStudentClasses = (event: any) => {
        let { name, value } = event.currentTarget;
        setCredentials({
            ...credentials,
            [name]: [
                {id: value}
            ]
        });
    }

    return ( 
        <div>
            <div className="row">
                <div className="col-md-3">
                    <h2>
                        {id ? 'Modifier le' : 'Ajouter un'} projet
                    </h2>

                    <div className="mb-2">
                        {id && <ShareLinkProjectButton id={id} url={STUDENT_APP_URL} size="small" className="w-100 d-flex justify-content-between" />}
                    </div>

                    <form onSubmit={handleSubmit}>
                        <div className="form-group mb-2">
                            <Input 
                                placeholder="Nom du projet"
                                name="name"
                                type="text"
                                value={credentials.name || ''}
                                credentials={credentials}
                                setCredentials={setCredentials}
                            />
                        </div>
                        <div className="form-group mb-2">
                            <div className="select-wrapper" >
                                <select
                                    onChange={handleChangeStudentClasses} 
                                    name="studentClasses"
                                    value={
                                        credentials.studentClasses && credentials.studentClasses[0]
                                        ? credentials.studentClasses[0].id
                                        : ""
                                    }
                                >
                                    <option value="">Selectionner une classe</option>
                                    {studentClasses.map((studentClass: any) => (
                                        <option 
                                            key={studentClass.id}
                                            value={studentClass.id}
                                        >
                                                {studentClass.name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </div>

                        <div className="form-group mb-2">
                            <div className="select-wrapper" >
                                <select
                                    name="subject"
                                    onChange={handleChangeSubject}
                                    value={credentials.subject?.id || ''}
                                >
                                    <option value="">Selectionner une matière</option>
                                    {subjects.map((subject: any) => (
                                        <option

                                            key={subject.id}
                                            value={subject.id}
                                        >
                                                {subject.name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </div>
                        <div className="form-group">
                            <Button
                                type="submit"
                                label="Enregistrer"
                                primary={!id && true}
                                className="mt-3 w-100"
                                size="large"
                                onLoadingSubmit={onLoadingSubmit}
                            />
                        </div>
                    </form>
                        
                </div>
                <div className="col-md-9">
                    {id && 
                    <>
                        {credentials.delivrables && credentials.delivrables.length > 0 ? 
                            <div>
                                <div className="col-md-12">
                                    <h2>
                                        Liste des livrables
                                    </h2>
                                </div>

                                <DelivrableList
                                    total={credentials.delivrables.length}
                                    delivrables={credentials.delivrables}
                                    fetchProject={fetchProject}
                                />
                            </div>
                        : 
                            <div className="share-link-block">
                                <p>Auncun livrable pour le moment</p>
                                <h2>Cliquer pour partager le lien aux étudiants</h2>
                                <div className="row">
                                    {id && <ShareLinkProjectButton id={id} url={STUDENT_APP_URL} size="large" />}
                                    <button 
                                        className="button button--large button--primary color--primary ml-3"
                                        onClick={() => fetchProject()}
                                    >
                                        Rafraichir
                                        <i className="fa-solid fa-rotate-right ml-2"></i>
                                    </button>
                                </div>
                            </div>
                        }
                    </>
                    }
                </div>
            </div>
        </div>
     );
}
 
export default ProjectFormPage;