import { Link } from 'react-router-dom'

export default function HomePage() {
    return (
        <div>
            <Link to="/school">École</Link>
            <br />
            <Link to="/teacher">Intervenant</Link>
            <br />
            <Link to="/dashboard">dashboard</Link>
        </div>
    )
}