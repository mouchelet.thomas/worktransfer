import { Button, Input } from "@work-transfert/ui";
import { AuthLayout } from "../../layout/AuthLayout";
import { FormEvent, useState } from "react";
import { AuthService } from "@work-transfert/services";
import { lottieSuccess } from "@work-transfert/assets";
import Lottie from 'react-lottie-player'
import toast from 'react-hot-toast';
import { ErrorResponse } from "@work-transfert/types";

type Credentials = {
    email: string;
}

export const ForgotPasswordPage = () => {
    const [credentials, setCredentials] = useState({} as Credentials);
    const [onLoadingSubmit, setOnLoadingSubmit] = useState(false);
    const [success, setSuccess] = useState(false);

    const handleForgotPassword = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        setOnLoadingSubmit(true);

        try {
            await AuthService.forgotPassword(credentials);
            setSuccess(true);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        } finally {
            setOnLoadingSubmit(false);
        }
    }

    return ( 
        <AuthLayout>
            <h1 className="mb-4 text-center">Entrez votre adresse email</h1>

            {success && (
                <div className="success-container">
                    <Lottie
                        animationData={lottieSuccess}
                        play
                        loop={false}
                        style={{ width: 250, height: 250 }}
                    />
                    <p className="typography-caption typography-caption--large">
                        Un email vous a été envoyé avec un lien de réinitialisation de mot de passe.
                    </p>
                    <Button
                        type="button"
                        label="Connexion"
                        to="/auth/signin"
                        className="mt-4 w-100"
                        size="large"
                    />
                </div>
            )}

            {!success && (
                <form onSubmit={handleForgotPassword}>
                    <Input 
                        placeholder="Adresse email"
                        name="email"
                        type="email"
                        value={credentials.email || ''}
                        credentials={credentials}
                        setCredentials={setCredentials}
                    />

                    <Button
                        type="submit"
                        label="Recevoir un lien de réinitialisation"
                        primary={true}
                        className="mt-4 w-100"
                        size="large"
                        onLoadingSubmit={onLoadingSubmit}
                    />
                    <Button
                        type="button"
                        label="Connexion"
                        to="/auth/signin"
                        className="mt-2 w-100"
                        size="large"
                    />
                </form>
            )}
        </AuthLayout>
     );
}