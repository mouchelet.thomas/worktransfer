import { AuthForm } from '@work-transfert/ui'
import { FormEvent, useContext, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { AuthService, UserService } from "@work-transfert/services"
import { UserContext } from '@work-transfert/contexts';
import { AuthLayout } from '../../layout/AuthLayout';
import toast from 'react-hot-toast';
import { ErrorResponse } from '@work-transfert/types';

export default function SigninPage() {
    const userContext = useContext(UserContext);
    const navigate = useNavigate();
    const [onLoadingSubmit, setOnLoadingSubmit] = useState(false);

    const [credentials, setCredentials] = useState({
        email: '',
        password: ''
    })

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        setOnLoadingSubmit(true)

        try {
            await AuthService.signin(credentials)
            const user = await UserService.getCurrentUser()
            if(userContext) userContext.setUser(user)
            navigate('/school/dashboard')
            toast('Bienvenue !',
                {
                    icon: '👏',
                    style: {
                        borderRadius: '10px',
                        background: '#333',
                        color: '#fff',
                    },
                }
            );
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        } finally {
            setOnLoadingSubmit(false)
        }
    }

    return (
        <AuthLayout>
            <div>
                <h1 className='mb-4 text-center'>Connectez-vous</h1>
                <AuthForm 
                    credentials={credentials}
                    setCredentials={setCredentials}
                    onSubmit={handleSubmit}
                    onLoadingSubmit={onLoadingSubmit}
                />
            </div>
        </AuthLayout>
    )
}