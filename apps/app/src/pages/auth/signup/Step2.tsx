import { Input } from "@work-transfert/ui";
import { ChangeEvent, useEffect, useState } from "react";

interface Step2Props {
    credentials: any
    setCredentials: any
    nextStep: any
}

interface localCredentials {
    firstName?: string
    lastName?: string
    name?: string
}

export const Step2 = ({
    setCredentials,
    credentials,
}: Step2Props) => {
    const [localCredentials, setLocalCredentials] = useState({} as localCredentials)

    useEffect(() => {
        handleChange()
    }, [localCredentials])

    const handleChange = () => {
        
        if(credentials.role === "teacher") {
            setCredentials({ 
                ...credentials, 
                teacher: {
                    firstName: localCredentials.firstName,
                    lastName: localCredentials.lastName
                }
            });
        } else {
            setCredentials({ 
                ...credentials, 
                schools: [{
                    name: localCredentials.name 
                }]
            });
        }
    }

    return ( 
        <div>
            {credentials.role === "teacher" && (
                <>
                    <div className="form-group mb-2">
                        <Input 
                            placeholder="Prénom"
                            name="firstName"
                            type="text"
                            credentials={localCredentials}
                            setCredentials={setLocalCredentials}
                        />
                    </div>
                    <div className="form-group">
                        <Input 
                            placeholder="Nom de famille"
                            name="lastName"
                            type="text"
                            credentials={localCredentials}
                            setCredentials={setLocalCredentials}
                        />
                    </div>
                </>
            )}
            {credentials.role === "school" && (
                <>
                    <div className="form-group">
                        <Input 
                            placeholder="Nom de l'établissement"
                            name="name"
                            type="text"
                            credentials={localCredentials}
                            setCredentials={setLocalCredentials}
                        />
                    </div>
                </>
            )}
        </div>
     );
}