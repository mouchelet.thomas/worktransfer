import { Input } from "@work-transfert/ui";

interface Step3Props {
    credentials: any
    setCredentials: any
}

export const Step3 = ({
    credentials,
    setCredentials
}: Step3Props) => {
    return ( 
        <div>
            <div className="form-group mb-2">
                <Input 
                    type="text" 
                    placeholder="Email" 
                    credentials={credentials}
                    setCredentials={setCredentials}
                    name="email"
                />
            </div>
            <div className="form-group mb-2">
                <Input 
                    type="password" 
                    placeholder=" Mot de passe" 
                    name="password"
                    credentials={credentials}
                    setCredentials={setCredentials}
                />
            </div>
            <div className="form-group">
                <Input 
                    type="password" 
                    placeholder="Confirmation du mot de passe" 
                    name="passwordConfirm"
                    credentials={credentials}
                    setCredentials={setCredentials}
                />
            </div>
        </div>
     );
}