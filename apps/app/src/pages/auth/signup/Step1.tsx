import { ChangeEvent } from "react";

interface Step1Props {
    credentials: any
    setCredentials: any
}

export const Step1 = ({
    setCredentials,
    credentials,
}: Step1Props) => {
    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setCredentials({ ...credentials, [name]: value });
    }

    return ( 
        <div className="form-group">
            <div className="select--radio" onChange={handleChange}>
                <div>
                    <input type="radio" id="school" name="role" value="school"  />
                    <label htmlFor="school">École</label>
                </div>
                <div>
                    <input type="radio" id="teacher" name="role" value="teacher" />
                    <label htmlFor="teacher">Intervenant</label>
                </div>
            </div>
        </div>
     );
}