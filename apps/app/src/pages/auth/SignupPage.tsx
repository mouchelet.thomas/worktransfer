import { useState } from "react";
import { AuthLayout } from "../../layout/AuthLayout";
import { AuthService } from "@work-transfert/services";
import { Alert, Button } from "@work-transfert/ui";
import { Step1 } from "./signup/Step1";
import { Step2 } from "./signup/Step2";
import { Step3 } from "./signup/Step3";
import { lottieSuccess } from "@work-transfert/assets"
import Lottie from 'react-lottie-player'
import toast from 'react-hot-toast';
import { ErrorResponse } from "@work-transfert/types";

type credentialsSignup = {
    email: string,
    password: string,
    passwordConfirm: string,
    role: string,
    teacher: {
        firstName?: string,
        lastName?: string,
    }
    schools: [{
        name?: string,
    }]
}

type errorType = {
    message: string;
}

export const SignupPage = () => {
    const [onLoadingSubmit, setOnLoadingSubmit] = useState(false);
    const [currentStep, setCurrentStep] = useState(1);
    const [credentials, setCredentials] = useState({} as credentialsSignup)
    const [success, setSuccess] = useState(false);

    const checkIsValid = (newCredentials: credentialsSignup) => {
        switch (currentStep) {
            case 1:
                if(!newCredentials.role) {
                    toast.error("Veuillez sélectionner un type d'utilisateur")
                    return false
                }
                break;
            case 2:
                if(newCredentials.role === 'school') {
                    if(!newCredentials.schools[0].name) {
                        toast.error("Veuillez sélectionner une école")
                        return false
                    }
                }  else {
                    if(!newCredentials.teacher.firstName || !newCredentials.teacher.lastName) {
                        toast.error("Veuillez remplir tous les champs")
                        return false
                    }
                }
                
                break;
            case 3:
                if(!newCredentials.email || !newCredentials.password || !newCredentials.passwordConfirm) {
                    toast.error("Veuillez remplir tous les champs")
                    return false
                }
                if(newCredentials.password !== newCredentials.passwordConfirm) {
                    toast.error("Les mots de passe ne correspondent pas")
                    return false
                }
                break;
        }
        return true
    }

    const nextStep = (newCredentials: credentialsSignup) => {
        if(!checkIsValid(newCredentials)) return;
        if(currentStep >= steps.length) {
            handleSubmit()
        } else {
            setCurrentStep(currentStep + 1);
        }
    }

    const createStepComponent = (Component: React.ComponentType<any>, props?: any) => (
        <Component
          {...props || {}}
          setCredentials={setCredentials}
          credentials={credentials}
          nextStep={nextStep}
        />
    );
    
    const steps = [
      {
        component: createStepComponent(Step1),
      },
      {
        component: createStepComponent(Step2),
      },
      {
        component: createStepComponent(Step3),
      },
    ];

    const handleSubmit = async () => {
        setOnLoadingSubmit(true)

        try {
            await AuthService.signup(credentials)
            setSuccess(true)
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        } finally {
            setOnLoadingSubmit(false)
        }
    }

    return ( 
        <AuthLayout>
            <div>
                <h1 className='mb-4 text-center'>Inscription</h1>

                <form onSubmit={(e) => e.preventDefault()}>
                    {success && (
                        <div className="success-container mb-4">
                            <Lottie
                                animationData={lottieSuccess}
                                play
                                loop={false}
                                style={{ width: 250, height: 250 }}
                            />
                            <p className="typography-caption typography-caption--large">
                                votre compte a bien été créé
                            </p>
                        </div>
                    )}
                    {!success && (
                        <>
                            <div>
                                {steps[currentStep - 1].component}
                            </div>
                            <div className="form-group mt-5">
                                <Button
                                    type="button"
                                    label={currentStep === steps.length ? "Terminer" : `Suivant (${currentStep}/${steps.length})`}
                                    primary={true}
                                    size="large"
                                    className="w-100"
                                    onLoadingSubmit={onLoadingSubmit}
                                    onClick={() => nextStep(credentials)}
                                />
                            </div>
                        </>
                    )}

                    <div className="form-group mt-2">
                        <Button
                            primary={success}
                            type="button"
                            label="Connexion"
                            size="large"
                            className="w-100"
                            to="/auth/signin"
                        />
                    </div>
                </form>
            </div>
        </AuthLayout>
     );
}