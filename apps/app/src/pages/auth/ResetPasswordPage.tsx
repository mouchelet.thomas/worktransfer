import { Button, Input } from "@work-transfert/ui";
import { AuthLayout } from "../../layout/AuthLayout";
import { FormEvent, useState } from "react";
import { AuthService } from "@work-transfert/services";
import { useNavigate, useParams } from "react-router-dom";
import { lottieSuccess } from "@work-transfert/assets";
import Lottie from 'react-lottie-player'
import toast from 'react-hot-toast';
import { ErrorResponse } from "@work-transfert/types";

type Credentials = {
    password: string;
}

export const ResetPasswordPage = () => {
    const [credentials, setCredentials] = useState({} as Credentials);
    const [onLoadingSubmit, setOnLoadingSubmit] = useState(false);
    const [success, setSuccess] = useState(false);
    const { token } = useParams()
    const navigate = useNavigate();

    const handleForgotPassword = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if(!token) return navigate('/auth/signin')
        
        setOnLoadingSubmit(true);
        try {
            await AuthService.resetPassword({
                token,
                password: credentials.password
            });
            setSuccess(true);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        } finally { 
            setOnLoadingSubmit(false);
        }
    }

    return ( 
        <AuthLayout>
            <h1 className="mb-4 text-center">
                Réinitialisation du mot de passe
            </h1>

            {success && (
                <div className="success-container">
                    <Lottie
                        animationData={lottieSuccess}
                        play
                        loop={false}
                        style={{ width: 250, height: 250 }}
                    />
                    <p className="typography-caption typography-caption--large">
                        Votre mot de passe a été réinitialisé avec succès
                    </p>
                    <Button
                        type="button"
                        label="Connexion"
                        to="/auth/signin"
                        className="mt-4 w-100"
                        primary={true}
                        size="large"
                    />
                </div>
            )}

            {!success && (  
            <form onSubmit={handleForgotPassword}>
                <Input 
                    placeholder="Mot de passe"
                    name="password"
                    type="password"
                    value={credentials.password || ''}
                    credentials={credentials}
                    setCredentials={setCredentials}
                />

                <Button
                    type="submit"
                    label="Réinitialisation"
                    primary={true}
                    className="mt-4 w-100"
                    size="large"
                    onLoadingSubmit={onLoadingSubmit}
                />
                <Button
                    type="button"
                    label="Connexion"
                    to="/auth/signin"
                    className="mt-2 w-100"
                    size="large"
                />
            </form>
            )}

        </AuthLayout>
     );
}