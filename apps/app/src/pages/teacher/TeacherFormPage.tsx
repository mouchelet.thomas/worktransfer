import { SchoolContext } from "@work-transfert/contexts";
import { TeacherSchoolService } from "@work-transfert/services";
import { ErrorResponse, TeacherSchoolType } from "@work-transfert/types";
import { Button, Input } from "@work-transfert/ui";
import { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import toast from 'react-hot-toast';

export const TeacherFormPage = () => {
    const { id } = useParams();
    const [credentials, setCredentials] = useState({} as TeacherSchoolType)
    const { school } = useContext(SchoolContext);
    const navigate = useNavigate();
    const [onLoadingSubmit, setOnLoadingSubmit] = useState(false);

    useEffect(() => {
        if(id) fetchTeacher()
    }, [id, school]);

    useEffect(() => {
        if(!id) {
            setCredentials({
                ...credentials,
                school
            })
        }
    }, [school])

    const fetchTeacher = async () => {
        if(!id) return;
        try {
            const response = await TeacherSchoolService.findOne(id);
            setCredentials(response);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        setOnLoadingSubmit(true);
        event.preventDefault();
        try {
            const methode = id ? 'update' : 'create';
            const response = await TeacherSchoolService[methode](credentials);
            if(methode == "create") navigate(`/teachers/${response.id}`);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        } finally {
            setOnLoadingSubmit(false);
        }
    }

    return (
        <div>
            <h1>
                {id ? 'Modifier l\'' : 'Ajouter un '}intervenant
            </h1>

            <form onSubmit={handleSubmit}>
                <div className="row">
                    <div className="col-md-4">
                        <h3>
                            Informations intervenant
                        </h3>
                        <div className="form-group mb-2">
                            <Input
                                placeholder="Adresse email"
                                type="email"
                                name="teacher_school_email"
                                credentials={credentials}
                                setCredentials={setCredentials}
                                value={credentials?.teacher_school_email || ''}
                            />
                        </div>
                        {/* <div className="form-group mb-2">
                            <Input
                                placeholder="Nom de famille"
                                type="text"
                                name="lastName"
                                credentials={credentials}
                                setCredentials={setCredentials}
                                value={credentials.lastName || ''}
                            />
                        </div>
                        <div className="form-group">
                            <Input
                                placeholder="Prénom"
                                type="text"
                                name="firstName"
                                credentials={credentials}
                                setCredentials={setCredentials}
                                value={credentials.firstName || ''}
                            />
                        </div> */}
                    </div>
                </div>
                
                <div className="form-group">
                    <Button
                        type="submit"
                        label="Enregistrer"
                        primary={true}
                        className="mt-3"
                        size="medium"
                        onLoadingSubmit={onLoadingSubmit}
                    />
                </div>
            </form>
        </div>
    )
}