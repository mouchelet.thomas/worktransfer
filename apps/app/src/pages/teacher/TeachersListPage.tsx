import { UserContext } from "@work-transfert/contexts";
import { TeacherSchoolService, TeacherService } from "@work-transfert/services";
import { ListItemsCrud, ListItemsCrudSelect } from "@work-transfert/ui";
import { useContext } from "react";

export const TeachersListPage = () => {
    const { user } = useContext(UserContext);

    const showLabels = (teacherSchool: any) => {
        return (
            <>
                {!teacherSchool.activated &&
                    <p className="col-md-2">
                        <span className="badge bg-warning">Non activé</span>
                    </p> 
                }
                {teacherSchool.activated && (
                    <>
                        <p className="col-md-2">{teacherSchool.teacher.firstName}</p>
                        <p className="col-md-2">{teacherSchool.teacher.lastName}</p>
                    </>
                )}
            </>
        )
    }

    return (
        <div>
            <ListItemsCrudSelect 
                entityService={TeacherSchoolService}
                showLabels={showLabels}
                entity="teacherSchool"
                entityLabel="intervenant"
                allowActions={user.role !== "teacher"}
                inputType="email"
                inputName="teacher_school_email"
            />
        </div>
    );
}