import { LogoPrimary } from "@work-transfert/ui";

type ChildrenProps = {
    children: React.ReactNode;
}

export const AuthLayout = ({children}: ChildrenProps) => {
    return ( 
        <div className='auth-page'>
            <LogoPrimary />
            <div className="auth-container">
                <div className='card'>
                    {children}
                </div>
            </div>
        </div>
     );
}