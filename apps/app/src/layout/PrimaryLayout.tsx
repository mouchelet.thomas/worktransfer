import { NavbarDrawer } from "@work-transfert/ui";

type ChildrenProps = {
    children: React.ReactNode;
}

const PrimaryLayout: React.FC<ChildrenProps> = ({children}) => {
    return ( 
        <div>

            <div className="row">
                <div className="col-md-2">
                    <NavbarDrawer />
                </div>
                <div className="col-md-10 pt-4">
                    <div className="container">
                        {children}  
                    </div>
                </div>
            </div>
        </div>
     );
}
 
export default PrimaryLayout;