import { BrowserRouter } from 'react-router-dom'
import MainRoutes from './routes/MainRoutes'
import "@work-transfert/styles/src/main.scss"
import { useContext, useEffect, useState } from 'react'
import { UserService } from '@work-transfert/services'
import { UserContext } from '@work-transfert/contexts'
import { Toaster } from 'react-hot-toast';

function App() {
  const { setUser } = useContext(UserContext)

  useEffect(() => {
    const uerLocale = UserService.getCurrentUser()
    if(uerLocale) setUser(uerLocale)
  }, [])

  return (
    <BrowserRouter>
      <MainRoutes />
      <div><Toaster/></div>
    </BrowserRouter>
  )
}

export default App
