import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { SchoolModule } from './school/school.module';
import { TeacherModule } from './teacher/teacher.module';
import { AuthModule } from './auth/auth.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { SubjectModule } from './subject/subject.module';
import { ProjectModule } from './project/project.module';
import { DelivrableModule } from './delivrable/delivrable.module';
import { CurriculumModule } from './curriculum/curriculum.module';
import { StudentClassModule } from './student-class/student-class.module';
import { TeacherSchoolModule } from './teacher-school/teacher-school.module';
import { JoinSchoolInvitationModule } from './join-school-invitation/join-school-invitation.module';
import { DatabaseModule } from './database/database.module';
import { MailerConfigModule } from './mailer-config/mailer-config.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MailerConfigModule,
    DatabaseModule,
    UserModule,
    SchoolModule,
    TeacherModule,
    AuthModule,
    SubjectModule,
    ProjectModule,
    DelivrableModule,
    CurriculumModule,
    StudentClassModule,
    TeacherSchoolModule,
    JoinSchoolInvitationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
