import { PartialType } from '@nestjs/mapped-types';
import { CreateDelivrableUploadFileDto } from './create-delivrable-upload-file.dto';

export class UpdateDelivrableUploadFileDto extends PartialType(CreateDelivrableUploadFileDto) {}
