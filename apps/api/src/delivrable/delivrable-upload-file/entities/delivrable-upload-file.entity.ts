import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("delivrableUploadFile")
export class DelivrableUploadFileEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    ETag: string
    
    @Column()
    Location: string
    
    @Column()
    Key: string
    
    @Column()
    size: number
    
    @Column()
    Bucket : string
}
