import { Controller, Get, Param, Res } from '@nestjs/common';
import { DelivrableUploadFileService } from './delivrable-upload-file.service';
@Controller('delivrable-upload-files')
export class DelivrableUploadFileController {
  constructor(private readonly delivrableUploadFileService: DelivrableUploadFileService) {}

  @Get('/download/:id')
    async downloadFile(
        @Param('id') id
  ){
      return await this.delivrableUploadFileService.getDownloadUrl(id);
  }

  @Get('/download/all/:id')
  async downloadAllFiles(
      @Param('id') id,
      @Res() res
  ){
      await this.delivrableUploadFileService.downloadAllFilesByProject(id, res);
  }
}
