import { Inject, Injectable } from '@nestjs/common';
import { CreateDelivrableUploadFileDto } from './dto/create-delivrable-upload-file.dto';
import { UpdateDelivrableUploadFileDto } from './dto/update-delivrable-upload-file.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DelivrableUploadFileEntity } from './entities/delivrable-upload-file.entity';
import { Repository } from 'typeorm';
import { ProjectService } from '../../project/project.service';
const S3 = require('aws-sdk/clients/s3')
const s3Zip = require('s3-zip')

@Injectable()
export class DelivrableUploadFileService {
  private s3
    private bucketName

    constructor(
        @InjectRepository(DelivrableUploadFileEntity)
        private delivrableUploadFileRepository: Repository<DelivrableUploadFileEntity>,
        @Inject(ProjectService)
        private projectService: ProjectService,
    ){
        this.s3 = new S3({
            region: process.env.APP_AWS_BUCKET_REGION,
            accessKeyId: process.env.APP_AWS_ACCESS_KEY,
            secretAccessKey: process.env.APP_AWS_SECRET_KEY
        })
        this.bucketName = process.env.APP_AWS_BUCKET_NAME
    }

    async uploadFileAws(delivrableUser, fileData, project){
        const firstName = this.formatText(delivrableUser.firstName)
        const lastName = this.formatText(delivrableUser.lastName)
        
        const fileName = `${lastName}-${firstName}-${Date.now()}.${fileData.originalname.split('.').pop()}`

        const uploadParams = {
            Bucket: this.bucketName,
            Body: fileData.buffer,
            Key: `${project.subject.school.id}/${project.subject.id}/${project.id}/${fileName}`,
        }
    
        return this.s3.upload(uploadParams).promise()
    }

    async getDownloadUrl(id){
        try {
            const {Key} = await this.delivrableUploadFileRepository.findOne({
                where: {
                    id
                }
            })
            const options = {
                Bucket: this.bucketName,
                Key,
                Expires: 3600, // one hour expires.
            };
            
            const url = this.s3.getSignedUrl('getObject', options);
            
            return url
        } catch (error) {
            throw new Error("Error while getting download url")
        }
    }

    async downloadAllFilesByProject(id, res){
        const dataProject = await this.projectService.findOne(id) 
        const delivrableUploadFiles = []
        
        dataProject.delivrables.map(delivrable => {
          if(delivrable.delivrableUploadFile){
              delivrableUploadFiles.push(delivrable.delivrableUploadFile)
          }
        })

        const filenames = []

        const params = {
            Bucket: this.bucketName,
            Key: `${dataProject.subject.school.id}/${dataProject.subject.id}/${dataProject.id}/`
        }

        delivrableUploadFiles.map(file => {
            filenames.push(file.Key.split('/').pop())
        })

        s3Zip
            .archive({ 
                s3: this.s3, 
                bucket: this.bucketName
            }, params.Key, [...filenames])
            .pipe(res)
    }

    formatText(text) {
        return text
          .trim()
          .toLowerCase()
          .normalize("NFD").replace(/[\u0300-\u036f]/g, "")
          .replace(/\s+/g, '-')
          .replace(/[^a-zA-Z0-9-]/g, '')
    }
}
