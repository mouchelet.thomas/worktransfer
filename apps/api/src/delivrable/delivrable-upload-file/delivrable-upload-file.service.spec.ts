import { Test, TestingModule } from '@nestjs/testing';
import { DelivrableUploadFileService } from './delivrable-upload-file.service';

describe('DelivrableUploadFileService', () => {
  let service: DelivrableUploadFileService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DelivrableUploadFileService],
    }).compile();

    service = module.get<DelivrableUploadFileService>(DelivrableUploadFileService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
