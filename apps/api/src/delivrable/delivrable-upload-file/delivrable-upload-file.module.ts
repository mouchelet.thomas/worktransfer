import { Module } from '@nestjs/common';
import { DelivrableUploadFileService } from './delivrable-upload-file.service';
import { DelivrableUploadFileController } from './delivrable-upload-file.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DelivrableUploadFileEntity } from './entities/delivrable-upload-file.entity';
import { ProjectModule } from '../../project/project.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([DelivrableUploadFileEntity]),
    ProjectModule
  ],
  controllers: [DelivrableUploadFileController],
  providers: [DelivrableUploadFileService],
  exports: [DelivrableUploadFileService]
})
export class DelivrableUploadFileModule {}
