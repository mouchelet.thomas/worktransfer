import { Test, TestingModule } from '@nestjs/testing';
import { DelivrableUploadFileController } from './delivrable-upload-file.controller';
import { DelivrableUploadFileService } from './delivrable-upload-file.service';

describe('DelivrableUploadFileController', () => {
  let controller: DelivrableUploadFileController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DelivrableUploadFileController],
      providers: [DelivrableUploadFileService],
    }).compile();

    controller = module.get<DelivrableUploadFileController>(DelivrableUploadFileController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
