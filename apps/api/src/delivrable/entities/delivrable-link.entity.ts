import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { DelivrableEntity } from "./delivrable.entity";

@Entity("delivrableLink")
export class DelivrableLinkEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    linkUrl: string;

    @ManyToOne(
        type => DelivrableEntity, 
        delivrable => delivrable.delivrableLinks,
    )
    delivrable: DelivrableEntity;
}
