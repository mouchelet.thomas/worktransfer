import { ProjectEntity } from "../../project/entities/project.entity";
import { Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { DelivrableUserEntity } from "./delivrable-user.entity";
import { DelivrableLinkEntity } from "./delivrable-link.entity";
import { DelivrableUploadFileEntity } from "../delivrable-upload-file/entities/delivrable-upload-file.entity";
import { TimestampEntites } from "../../Generic/timestamp.entities";

@Entity("delivrable")
export class DelivrableEntity extends TimestampEntites {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @ManyToOne(() => ProjectEntity, project => project.delivrables)
    project: ProjectEntity;

    @OneToOne(() => DelivrableUserEntity,{ 
        cascade: ["insert","remove"],
        eager: true,
        nullable: true 
    })
    @JoinColumn()
    delivrableUser: DelivrableUserEntity;

    @OneToOne(() => DelivrableUploadFileEntity, { 
        cascade: ["insert","remove"],
        eager: true,
        nullable: true 
    })
    @JoinColumn()
    delivrableUploadFile: DelivrableUploadFileEntity;

    @OneToMany(
        type => DelivrableLinkEntity,
        delivrableLink => delivrableLink.delivrable,
        {
            cascade: ["insert","remove"],
            eager: true,
            nullable: true
        }
    )
    delivrableLinks: DelivrableLinkEntity[]
}
