import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("delivrableUser")
export class DelivrableUserEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    firstName: string
    
    @Column()
    lastName: string
}
