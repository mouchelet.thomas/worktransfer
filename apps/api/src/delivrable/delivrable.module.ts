import { Module } from '@nestjs/common';
import { DelivrableService } from './delivrable.service';
import { DelivrableController } from './delivrable.controller';
import { DelivrableUploadFileModule } from './delivrable-upload-file/delivrable-upload-file.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DelivrableEntity } from './entities/delivrable.entity';
import { ProjectModule } from '../project/project.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([DelivrableEntity]),
    DelivrableUploadFileModule, 
    ProjectModule,
    DelivrableUploadFileModule
  ],
  controllers: [DelivrableController],
  providers: [DelivrableService],
})
export class DelivrableModule {}
