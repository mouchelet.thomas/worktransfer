import { Controller, Post, Body, UseInterceptors, UploadedFile, Delete, UseGuards, Query, Get, Param } from '@nestjs/common';
import { DelivrableService } from './delivrable.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from '../decorators/role.decorator';
import { User } from '../decorators/user.decorator';
import { UserType } from '@work-transfert/types';

@Controller('delivrables')
export class DelivrableController {
  constructor(private readonly delivrableService: DelivrableService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async addFile(
      @Body() bodyData,
      @UploadedFile() fileData
  ){
      return await this.delivrableService.create(bodyData, fileData);
  }

  @Delete()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school", "teacher")
  softDelete(
    @Query() queries: string,
    @User() user: UserType
  ) {
    return this.delivrableService.softDeleteByIds(queries, user);
  }

  @Get(":id")
  async findOne(
    @Param("id") id: string
  ){
    return await this.delivrableService.findOne(id);
  } 

}
