import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DelivrableEntity } from './entities/delivrable.entity';
import { Repository } from 'typeorm';
import { ProjectService } from '../project/project.service';
import { DelivrableUploadFileService } from './delivrable-upload-file/delivrable-upload-file.service';

@Injectable()
export class DelivrableService {
  constructor(
    @InjectRepository(DelivrableEntity)
    private delivrableRepository: Repository<DelivrableEntity>,
    @Inject(ProjectService)
    private projectService: ProjectService,
    @Inject(DelivrableUploadFileService)
    private delivrableUploadFileService: DelivrableUploadFileService
  ){}

  async create(bodyData, fileData) {
    const project = await this.projectService.findOne(bodyData.project)
    
    let newDelivrable = {
        delivrableUser: {
            firstName: bodyData.delivrableUser.firstName,
            lastName: bodyData.delivrableUser.lastName
        },
        project: project,
        delivrableUploadFile: null,
        delivrableLinks: null
    }
    
    if(fileData){
        const resultAws = await this.delivrableUploadFileService.uploadFileAws(
            bodyData.delivrableUser, 
            fileData,
            project,
            )
            
        newDelivrable.delivrableUploadFile = {
            ...resultAws,
            size: fileData.size,
        }
    }
    if(bodyData.delivrableLinks){
        const delivrableLinks = []
        Object.values(bodyData.delivrableLinks).map(linkUrl => delivrableLinks.push({linkUrl}))
        newDelivrable.delivrableLinks = delivrableLinks
    }

    try {
        const delivrable = await this.delivrableRepository.save(newDelivrable);
        
        return delivrable
    } catch (error) {
        throw new Error("Error while saving upload file")
    }
  }

  async softDeleteByIds(queries, user) {
    const ids = queries.ids.split(',')
    
    const delivrables = await this.delivrableRepository.createQueryBuilder('delivrable')
        .where('delivrable.id IN (:...ids)', { ids })
        .getMany()
    delivrables.map(delivrable => delivrable.deletedAt = new Date())
    return await this.delivrableRepository.save(delivrables)
  }

  async findOne(id: string) {
    return await this.delivrableRepository.createQueryBuilder("delivrable")
      .leftJoinAndSelect("delivrable.delivrableUser", "delivrableUser")
      .leftJoinAndSelect("delivrable.delivrableUploadFile", "delivrableUploadFile")
      .leftJoinAndSelect("delivrable.delivrableLinks", "delivrableLinks")
      .leftJoinAndSelect("delivrable.project", "project")
      .where("delivrable.id = :id", { id })
      .getOne();
  }
}
