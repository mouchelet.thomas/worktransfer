import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { MairlerConfigService } from './mailer-config.service';
import path from 'path';

@Module({
  imports: [
    MailerModule.forRootAsync({
      useFactory: () => {
        const isProduction = process.env.NODE_ENV === 'production';

        const transport =
         isProduction
          ? {
              host: process.env.MAIL_HOST,
              port: process.env.MAIL_PORT,
              auth: {
                user: process.env.MAIL_AUTH_USER,
                pass: process.env.MAIL_AUTH_PASSWORD,
              },
            }
          : 
          {
              host: 'localhost',
              port: 25,
              secure: false,
            };

        const templatePath = `${__dirname}/../../templates`

        return {
          transport,
          defaults: {
            from: `"WorkTransfer" <${process.env.MAIL_AUTH_USER}>`,
          },
          template: {
            dir: templatePath,
            adapter: new HandlebarsAdapter(),
            options: {
              strict: true,
            },
          },
        };
      },
      inject: [],
    }),
  ],
  providers: [MairlerConfigService],
  exports: [MairlerConfigService]
})
export class MailerConfigModule {}