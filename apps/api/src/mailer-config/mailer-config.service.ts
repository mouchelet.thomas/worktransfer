import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MairlerConfigService {
  constructor(
    private mailerService: MailerService,
  ) {}

  async sendMail() {
    this.mailerService.sendMail({
        to: "mouchelet.thomas@gmail.com",
        subject: 'test send mail',
        html: '<b>test send mail</b>',
    })
  }
}
