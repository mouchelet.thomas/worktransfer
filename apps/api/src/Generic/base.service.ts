import { HttpException, HttpStatus } from '@nestjs/common';
import { Repository } from 'typeorm';
import { UserType } from '@work-transfert/types';
import { ROLES_ENUM } from '../utils/roles.enum';

export abstract class BaseService<Entity> {
  constructor(protected readonly repository: Repository<Entity>) {}

  aLaProprieteDeletedAt(obj: any): obj is { deletedAt: Date | null } {
    return 'deletedAt' in obj;
  }

  getEntityName(): string {
    const metadata = this.repository.metadata;
    return metadata.targetName.replace("Entity", "").toLowerCase();
  }

  async softDelete(queries: any, user: UserType, relationName: string) {
    const ids = queries.ids.split(',')
    const entityName = this.getEntityName()

    const entities = await this.repository.createQueryBuilder('entity')
        .leftJoinAndSelect(`entity.${relationName}`, relationName)
        .where('entity.id IN (:...ids)', { ids })
        .getMany()

    if(!entities){
      throw new HttpException(
        `${entityName}s not found`,
        HttpStatus.NOT_FOUND,
      );
    }

    entities.map(entity => {
      if (user.role === ROLES_ENUM.SCHOOL) {
        if (Array.isArray(entity[relationName])) {
          // Si entity[relationName] est un tableau, vérifiez si au moins un élément correspond
          const hasMatchingSchool = entity[relationName].some(school => user.schools.some(userSchool => userSchool.id === school.id));
      
          if (!hasMatchingSchool) {
            throw new HttpException(
              'Vous ne pouvez pas effectuer cette action',
              HttpStatus.FORBIDDEN,
            );
          }
        } else {
          // Si entity[relationName] n'est pas un tableau, vérifiez directement la correspondance
          if (!user.schools.some((school) => school.id === entity[relationName].id)) {
            throw new HttpException(
              'Vous ne pouvez pas effectuer cette action',
              HttpStatus.FORBIDDEN,
            );
          }
        }
      }
    })

    try {
        entities.map(entity => {
            if (this.aLaProprieteDeletedAt(entity)) {
            entity.deletedAt = new Date();
            }
        });
      return await this.repository.save(entities)
    } catch (error) {
      throw new HttpException(
        error.message,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
