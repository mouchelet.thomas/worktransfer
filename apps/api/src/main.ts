import { RequestMethod } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api', { exclude: [
    { path: '/' , method: RequestMethod.GET}
  ]});
  app.enableCors({
    origin: "*",
    methods: ["GET", "POST","PATCH","DELETE","PUT"],
  });
  const port = process.env.PORT || 8000;
  await app.listen(port);
}
bootstrap();
