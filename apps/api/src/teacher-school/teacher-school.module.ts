import { Module } from '@nestjs/common';
import { TeacherSchoolService } from './teacher-school.service';
import { TeacherSchoolController } from './teacher-school.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TeacherSchoolEntity } from './entities/teacher-school.entity';
import { JoinSchoolInvitationModule } from '../join-school-invitation/join-school-invitation.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forFeature([TeacherSchoolEntity]),
    JoinSchoolInvitationModule
  ],
  controllers: [TeacherSchoolController],
  providers: [TeacherSchoolService],
  exports: [TeacherSchoolService]
})
export class TeacherSchoolModule {}
