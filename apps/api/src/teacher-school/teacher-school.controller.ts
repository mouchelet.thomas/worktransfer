import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Query } from '@nestjs/common';
import { TeacherSchoolService } from './teacher-school.service';
import { CreateTeacherSchoolDto } from './dto/create-teacher-school.dto';
import { UpdateTeacherSchoolDto } from './dto/update-teacher-school.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from '../decorators/role.decorator';
import { UserEntity } from '../user/entities/user.entity';
import { User } from '../decorators/user.decorator';
import { UserType } from '@work-transfert/types';

@Controller('teacher-schools')
export class TeacherSchoolController {
  constructor(private readonly teacherSchoolService: TeacherSchoolService) {}

  @Post()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  create(
    @Body() createTeacherSchoolDto: CreateTeacherSchoolDto,
  ) {
    return this.teacherSchoolService.create(createTeacherSchoolDto);
  }

  @Post("multiple")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  createMultipleTeacherSchools(
    @Body() createTeacherSchoolDto: CreateTeacherSchoolDto[],
  ) {
    return this.teacherSchoolService.createMultipleTeacherSchools(createTeacherSchoolDto);
  }

  @Post("accept")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "teacher")
  creacceptInvitationate(
    @Body() acceptedInvitation,
    @User() user: UserEntity
  ) {
    return this.teacherSchoolService.acceptInvitation(acceptedInvitation, user);
  }

  @Get("/schools/:id")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  findAllBySchoolId(
    @User() user: UserEntity,
    @Param('id') schoolId: string,
    @Query() queries,
  ) {
    return this.teacherSchoolService.findAllBySchoolId(schoolId, queries, user);
  }

  @Get(':id')
  findOneById(@Param('id') id: string) {
    return this.teacherSchoolService.findOneById(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTeacherSchoolDto: UpdateTeacherSchoolDto) {
    return this.teacherSchoolService.update(id, updateTeacherSchoolDto);
  }

  @Delete()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  softDelete(
    @User() user: UserType,
    @Query() queries,
  ) {
    return this.teacherSchoolService.softDelete(queries, user);
  }
}
