import { JoinSchoolInvitationEntity } from "../../join-school-invitation/entities/join-school-invitation.entity";
import { TimestampEntites } from "../../Generic/timestamp.entities";
import { SchoolEntity } from "../../school/entities/school.entity";
import { TeacherEntity } from "../../teacher/entities/teacher.entity";
import { Column, Entity, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn, ManyToOne } from "typeorm";
import { ProjectEntity } from "../../project/entities/project.entity";

@Entity("teacherSchool")
export class TeacherSchoolEntity extends TimestampEntites {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    teacher_school_email: string;

    @Column({ default: false })
    activated: boolean;

    @ManyToOne(() => SchoolEntity, (school) => school.teacherSchools)
    school: SchoolEntity;

    @ManyToOne(() => TeacherEntity, (teacher) => teacher.teacherSchools, {
        nullable: true,
    })
    teacher: TeacherEntity;

    @OneToOne(() => JoinSchoolInvitationEntity, (joinSchoolInvitation) => joinSchoolInvitation.teacherSchool, {
        cascade: ["remove"]
    })
    @JoinColumn()
    joinSchoolInvitation: JoinSchoolInvitationEntity

    @OneToMany(() => ProjectEntity, project => project.teacherSchool)
    projects: ProjectEntity[]
}
