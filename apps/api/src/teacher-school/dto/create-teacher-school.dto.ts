import { SchoolType, TeacherType } from "@work-transfert/types";

export class CreateTeacherSchoolDto {
    id: string;
    schoolId: string;
    teacherId: string | null;
    teacher_school_email: string;
    invitation_accepted: boolean;
    code: string | null;
    code_expiration: Date | null;
    school?: SchoolType;
    teacher?: TeacherType;
    activated: boolean;
}
