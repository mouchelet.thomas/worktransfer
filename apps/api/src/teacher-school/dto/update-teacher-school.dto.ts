import { PartialType } from '@nestjs/mapped-types';
import { CreateTeacherSchoolDto } from './create-teacher-school.dto';

export class UpdateTeacherSchoolDto extends PartialType(CreateTeacherSchoolDto) {}
