import { Test, TestingModule } from '@nestjs/testing';
import { TeacherSchoolController } from './teacher-school.controller';
import { TeacherSchoolService } from './teacher-school.service';

describe('TeacherSchoolController', () => {
  let controller: TeacherSchoolController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TeacherSchoolController],
      providers: [TeacherSchoolService],
    }).compile();

    controller = module.get<TeacherSchoolController>(TeacherSchoolController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
