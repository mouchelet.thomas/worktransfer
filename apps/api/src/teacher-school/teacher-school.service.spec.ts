import { Test, TestingModule } from '@nestjs/testing';
import { TeacherSchoolService } from './teacher-school.service';

describe('TeacherSchoolService', () => {
  let service: TeacherSchoolService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TeacherSchoolService],
    }).compile();

    service = module.get<TeacherSchoolService>(TeacherSchoolService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
