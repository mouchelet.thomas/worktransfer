import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateTeacherSchoolDto } from './dto/create-teacher-school.dto';
import { UpdateTeacherSchoolDto } from './dto/update-teacher-school.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { TeacherSchoolEntity } from './entities/teacher-school.entity';
import { Repository } from 'typeorm';
import { JoinSchoolInvitationService } from '../join-school-invitation/join-school-invitation.service';
import { UserType } from '@work-transfert/types';
import { MailerService } from '@nestjs-modules/mailer';
import { paginate } from '../utils/functions';
import { BaseService } from '../Generic/base.service';
import path from 'path';

@Injectable()
export class TeacherSchoolService extends BaseService<TeacherSchoolEntity>{
  constructor(
    @InjectRepository(TeacherSchoolEntity)
    private teacherSchoolRepository: Repository<TeacherSchoolEntity>,
    private joinSchoolInvitationService: JoinSchoolInvitationService,
    private mailerService: MailerService,
  ) {
    super(teacherSchoolRepository)
  }


  async create(createTeacherSchoolDto: CreateTeacherSchoolDto) {
    console.log("process.cwd() : ", process.cwd())
    const teacherSchoolCreate = await this.teacherSchoolRepository.save(createTeacherSchoolDto);
    const teacherSchool = await this.findOneById(teacherSchoolCreate.id)
    const joinSchoolInvitation = await this.joinSchoolInvitationService.create({teacherSchool})

    // const templatePath = path.join(__dirname, '..', '..', 'templates', 'teacher-school', 'activated.hbs');
    // console.log('Template path:', templatePath);
    const templatePath = `${__dirname}/../../templates/teacher-school/activated.hbs`

    try {
      await this.mailerService.sendMail({
        to: createTeacherSchoolDto.teacher_school_email,
        subject: 'Accepter l\'invitation',
        template: templatePath,
        context: {
            school_name: teacherSchool.school.name,
            code: joinSchoolInvitation.code.split(""),
            email: createTeacherSchoolDto.teacher_school_email,
            APP_FRONT_LINK: `${process.env.APP_FRONT_LINK}/schools/accept-invitation`,
        }
      })
    } catch (error) {
        console.log('Error sending email:', error);
    }

    return teacherSchool
  }

  async createMultipleTeacherSchools(createTeacherSchoolDtos: CreateTeacherSchoolDto[]) {
    const createdTeacherSchools = [];
  
    for (const createTeacherSchoolDto of createTeacherSchoolDtos) {
      const createdTeacherSchool = await this.create(createTeacherSchoolDto);
      createdTeacherSchools.push(createdTeacherSchool);
    }
  
    return createdTeacherSchools;
  }

  async acceptInvitation(acceptedInvitation, user: UserType) {
    const teacherSchool = await this.findByTeacherSchoolEmail(acceptedInvitation.teacher_school_email)
    
    if(!teacherSchool.joinSchoolInvitation) {
      throw new HttpException("Invitation not found", HttpStatus.NOT_FOUND)
    }
    if(teacherSchool.joinSchoolInvitation.code_expiration < new Date()) {
      throw new HttpException("Invitation expired", HttpStatus.BAD_REQUEST)
    }
    if(teacherSchool.joinSchoolInvitation.code !== acceptedInvitation.code) {
      throw new HttpException("Code not valid", HttpStatus.BAD_REQUEST)
    }
      
    const teacherSchoolUpdate = await this.update(teacherSchool.id, {
      teacher: user.teacher,
      activated: true
    })

    await this.joinSchoolInvitationService.softDelete(teacherSchool.joinSchoolInvitation.id)

    return teacherSchoolUpdate
  }

  async findByTeacherSchoolEmail(teacher_school_email: string) {
    const teacherSchool = await this.teacherSchoolRepository.createQueryBuilder("entity")
      .leftJoinAndSelect("entity.joinSchoolInvitation", "joinSchoolInvitation")
      .leftJoinAndSelect("entity.teacher", "teacher")
      .leftJoinAndSelect("entity.school", "school")
      .where("entity.teacher_school_email = :teacher_school_email", { teacher_school_email })
      .getOne();
    return teacherSchool
  }

  async findAllBySchoolId(id: string, queries: any, user : UserType) {
      const query = await this.teacherSchoolRepository.createQueryBuilder("entity")
      .leftJoinAndSelect("entity.joinSchoolInvitation", "joinSchoolInvitation")
      .leftJoinAndSelect("entity.teacher", "teacher")
      .leftJoinAndSelect("entity.school", "school")
      .where("school.id = :id", { id })

      return await paginate(query, queries);
  }

  async findOneById(id: string) {
    const teacherSchool = await this.teacherSchoolRepository.createQueryBuilder("entity")
      .leftJoinAndSelect("entity.joinSchoolInvitation", "joinSchoolInvitation")
      .leftJoinAndSelect("entity.teacher", "teacher")
      .leftJoinAndSelect("entity.school", "school")
      .where("entity.id = :id", { id })
      .getOne();
    return teacherSchool
  }

  async update(id: string, updateTeacherSchoolDto: UpdateTeacherSchoolDto) {
    const teacherSchool = await this.findOneById(id)
    if(!teacherSchool) {
      throw new HttpException("TeacherSchool not found", HttpStatus.NOT_FOUND)
    }
    const updatedTeacherSchool = await this.teacherSchoolRepository.save({...teacherSchool, ...updateTeacherSchoolDto})
    return updatedTeacherSchool
  }

  async softDelete(queries: any, user: UserType) {
    return super.softDelete(queries, user, "school");
  }
}
