import { ConflictException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ){}

  async create(createUserDto: CreateUserDto) {
    if(!createUserDto.password){
      throw new ConflictException(`Mot de passe manquant`);
    }

    if(!createUserDto.email){
        throw new ConflictException(`Email obligatoire`);
    }
    
    const findUser = await this.userRepository.findOneBy({ email: createUserDto.email }); 

    if (findUser) {
        throw new ConflictException(`Email déjà utilisé`);
    }
    
    createUserDto.password = await bcrypt.hash(createUserDto.password, 10);

    try {
      const user = await this.userRepository.create(createUserDto);
      await this.userRepository.save(user);
      return user;
    } catch (error) {
      throw new HttpException(
        error.message,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  findAll() {
    return `This action returns all user`;
  }

  async update(id: string, updateUserDto: UpdateUserDto) {

    if(updateUserDto.password){
      updateUserDto.password = await bcrypt.hash(updateUserDto.password, 10);
    }

    try {
      const user = await this.findOne(id);
      const userUpdated = {
        ...user,
        ...updateUserDto,
        teacher: {
          ...user.teacher,
          ...updateUserDto.teacher
        }
      }
      return await this.userRepository.save(userUpdated)
    } catch (error) {
      throw new HttpException(
        error.message,
        HttpStatus.INTERNAL_SERVER_ERROR,
      )
    }
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }

  async findOneByEmail(email: string) {
    return await this.createQueryBuilderWithRelations()
      .where("user.email = :email", { email })
      .getOne();
  }

  async findOneByEmailWithPassword(email: string) {
    return await this.createQueryBuilderWithRelations(true)
      .where("user.email = :email", { email })
      .getOne();
  }

  async findOne(id: string) {
    return await this.createQueryBuilderWithRelations()
      .where("user.id = :id", { id })
      .getOne();
  }

  private createQueryBuilderWithRelations(withPassword: boolean = false) {
    const queryBuilder = this.userRepository.createQueryBuilder("user")
      .leftJoinAndSelect("user.schools", "schools")
      .leftJoinAndSelect("user.teacher", "teacher")
      .leftJoinAndSelect("teacher.teacherSchools", "teacherSchools")
      .leftJoinAndSelect("teacherSchools.school", "school")

    if (withPassword) {
      queryBuilder.addSelect("user.password");
    }

    return queryBuilder;
  } 
}
