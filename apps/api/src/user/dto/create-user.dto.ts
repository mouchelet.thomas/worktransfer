import { SchoolType, TeacherType } from "@work-transfert/types";
import { CreateSchoolDto } from "../../school/dto/create-school.dto";

export class CreateUserDto {
    email: string;
    password: string;
    school?: SchoolType
    teacher?: TeacherType
}
