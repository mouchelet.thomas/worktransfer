import { TimestampEntites } from "../../Generic/timestamp.entities";
import { SchoolEntity } from "../../school/entities/school.entity";
import { TeacherEntity } from "../../teacher/entities/teacher.entity";
import { Column, Entity, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn } from "typeorm";
import { ROLES_ENUM } from "../../utils/roles.enum";

@Entity("user")
export class UserEntity extends TimestampEntites {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({
        unique: true
    })
    email: string;

    @Column({ 
        nullable: true,
        select: false
    })
    password?: string;

    @Column({
        type: "enum",
        enum: ROLES_ENUM,
    })
    role: ROLES_ENUM

    @OneToMany(() => SchoolEntity, (school) => school.user, {
        cascade: true
    })
    schools: SchoolEntity[]

    @OneToOne(() => TeacherEntity, (teacher) => teacher.user, {
        cascade: true
    })
    @JoinColumn()
    teacher: TeacherEntity
}
