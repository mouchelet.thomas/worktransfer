import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateSubjectDto } from './dto/create-subject.dto';
import { UpdateSubjectDto } from './dto/update-subject.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SubjectEntity } from './entities/subject.entity';
import { LIMIT_PER_PAGE } from '../utils/constants';
import { UserType } from '@work-transfert/types';
import { ROLES_ENUM } from '../utils/roles.enum';
import { paginate } from '../utils/functions';
import { BaseService } from '../Generic/base.service';

@Injectable()
export class SubjectService extends BaseService<SubjectEntity>{
  constructor(
    @InjectRepository(SubjectEntity)
    private subjectRepository: Repository<SubjectEntity>,
  ) {
    super(subjectRepository)
  }

  async create(createSubjectDto: CreateSubjectDto) {
    return await this.subjectRepository.save(createSubjectDto);
  }

  async findAllBySchoolId(user: UserType, queries, id) {
    const query= await this.subjectRepository.createQueryBuilder("entity")
      .leftJoinAndSelect("entity.school", "school")
      .leftJoinAndSelect("entity.curriculums", "curriculums")
      .where("school.id = :id", { id })

      if(queries.orderBy){
        await query.orderBy("subject.createdAt", queries.orderBy)
      }

    return await paginate(query, queries);
  }

  async findOne(id: string) {
    return await this.subjectRepository.createQueryBuilder("subject")
      .leftJoinAndSelect("subject.school", "school")
      .leftJoinAndSelect("subject.curriculums", "curriculums")
      .where("subject.id = :id", { id })
      .getOne();
  }

  async update(id: string, updateSubjectDto: UpdateSubjectDto, user: UserType) {
    const subject = await this.findOne(id);

    this.checkIfUserIsAllowed(user, subject);

    try {
      return await this.subjectRepository.save(updateSubjectDto)
    } catch (error) {
      throw new HttpException(
        error.message,
        HttpStatus.INTERNAL_SERVER_ERROR,
      )
    }
  }

  findAddlSubjectsByStudentClassId = async (user, queries) => {
    const { ids } = queries;
    const idsSplit = ids.split(",");
    const result = await this.subjectRepository.createQueryBuilder("subject")
      .leftJoinAndSelect("subject.curriculums", "curriculums")
      .leftJoinAndSelect("curriculums.studentClasses", "studentClasses")
      .where("studentClasses.id IN (:...ids)", { ids: idsSplit })
      .getMany();

    return result;
  }

  checkIfUserIsAllowed(user: UserType, subject: SubjectEntity) { 
    // Check if the user have the school in his schools
    const schoolExist = user.schools.find(school => school.id === subject.school.id);
    // Check if the user is a school and if the school is the owner of the subject
    if(user.role === ROLES_ENUM.SCHOOL && !schoolExist){
      throw new HttpException(
        'You are not allowed to delete this school',
        HttpStatus.UNAUTHORIZED,
      );
    }
  }

  async softDelete(queries: any, user: UserType) {
    return super.softDelete(queries, user, "school");
  }
}
