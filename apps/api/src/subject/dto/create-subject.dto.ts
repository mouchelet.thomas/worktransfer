import { SchoolType } from "@work-transfert/types";

export class CreateSubjectDto {
    name: string;
    school: SchoolType;
}
