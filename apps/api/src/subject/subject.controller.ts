import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Query } from '@nestjs/common';
import { SubjectService } from './subject.service';
import { CreateSubjectDto } from './dto/create-subject.dto';
import { UpdateSubjectDto } from './dto/update-subject.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from '../decorators/role.decorator';
import { User } from '../decorators/user.decorator';
import { UserType } from '@work-transfert/types';

@Controller('subjects')
export class SubjectController {
  constructor(private readonly subjectService: SubjectService) {}

  @Post()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school", "teacher")
  create(@Body() createSubjectDto: CreateSubjectDto) {
    return this.subjectService.create(createSubjectDto);
  }

  // TODO: Fix this
  @Post("multiple")
  createMultiple(@Body() createSubjectDto: CreateSubjectDto) {
    return this.subjectService.create(createSubjectDto);
  }

  @Get("/schools/:id")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school", "teacher")
  findAll(
    @User() user: UserType,
    @Query() queries,
    @Param('id') id: string
  ) {
    return this.subjectService.findAllBySchoolId(user, queries, id);
  }

  @Get("student-classes")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school", "teacher")
  findAddlSubjectsByStudentClassId(
    @User() user: UserType,
    @Query() queries,
  ) {
    return this.subjectService.findAddlSubjectsByStudentClassId(user, queries);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.subjectService.findOne(id);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  update(
    @Param('id') id: string, 
    @Body() updateSubjectDto: UpdateSubjectDto,
    @User() user: UserType
  ) {
    return this.subjectService.update(id, updateSubjectDto, user);
  }

  @Delete()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  softDelete(
    @User() user: UserType,
    @Query() queries,
  ) {
    return this.subjectService.softDelete(queries, user);
  }
}
