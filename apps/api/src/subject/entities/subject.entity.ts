import { TeacherEntity } from "../../teacher/entities/teacher.entity";
import { TimestampEntites } from "../../Generic/timestamp.entities";
import { ProjectEntity } from "../../project/entities/project.entity";
import { SchoolEntity } from "../../school/entities/school.entity";
import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { CurriculumEntity } from "../../curriculum/entities/curriculum.entity";

@Entity("subject")
export class SubjectEntity extends TimestampEntites {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @ManyToOne(() => SchoolEntity, school => school.subjects)
    school: SchoolEntity;

    @OneToMany(() => ProjectEntity, project => project.subject)
    projects: ProjectEntity[];

    @ManyToMany(() => CurriculumEntity, curriculum => curriculum.subjects)
    @JoinTable()
    curriculums: CurriculumEntity[]
}