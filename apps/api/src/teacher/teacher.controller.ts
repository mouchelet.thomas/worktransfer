import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Query } from '@nestjs/common';
import { TeacherService } from './teacher.service';
import { CreateTeacherDto } from './dto/create-teacher.dto';
import { UpdateTeacherDto } from './dto/update-teacher.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from '../decorators/role.decorator';
import { UserType } from '@work-transfert/types';
import { User } from '../decorators/user.decorator';

@Controller('teachers')
export class TeacherController {
  constructor(private readonly teacherService: TeacherService) {}

  @Get("/schools/:id")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  findAllBySchoolId(
    @Query() queries,
    @Param('id') schoolId: string,
  ) {
    return this.teacherService.findAllBySchoolId(queries, schoolId);
  }

  @Post()
  create(@Body() createTeacherDto: CreateTeacherDto) {
    return this.teacherService.create(createTeacherDto);
  }

  @Get()
  findAll() {
    return this.teacherService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.teacherService.findOne(id);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  update(
    @Param('id') id: string, 
    @Body() updateTeacherDto: UpdateTeacherDto,
    @User() user: UserType,
  ) {
    return this.teacherService.update(id, updateTeacherDto, user);
  }

  @Delete()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  softDelete(
    @User() user: UserType,
    @Query() queries,
  ) {
    return this.teacherService.softDelete(queries, user);
  }
}
