import { SubjectEntity } from "../../subject/entities/subject.entity";
import { TimestampEntites } from "../../Generic/timestamp.entities";
import { ProjectEntity } from "../../project/entities/project.entity";
import { SchoolEntity } from "../../school/entities/school.entity";
import { UserEntity } from "../../user/entities/user.entity";
import { BeforeInsert, Column, Entity, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn, ManyToMany, JoinTable } from "typeorm";
import { TeacherSchoolEntity } from "../../teacher-school/entities/teacher-school.entity";

@Entity("teacher")
export class TeacherEntity extends TimestampEntites {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @OneToOne(() => UserEntity, (user) => user.teacher)
    user: UserEntity

    @OneToMany(() => TeacherSchoolEntity, (teacherSchool) => teacherSchool.teacher, {
        cascade: true
    })
    teacherSchools: TeacherSchoolEntity[]
}
