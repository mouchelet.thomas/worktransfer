import { SchoolType, SubjectType, TeacherSchoolType, UserType } from "@work-transfert/types";

export class CreateTeacherDto {
    firstName?: string;
    lastName?: string;
    user: UserType;
    subjects: SubjectType[];
    schools: TeacherSchoolType[];
}
