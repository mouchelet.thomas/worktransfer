import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateTeacherDto } from './dto/create-teacher.dto';
import { UpdateTeacherDto } from './dto/update-teacher.dto';
import { UserService } from '../user/user.service';
import { LIMIT_PER_PAGE } from "../utils/constants"
import { InjectRepository } from '@nestjs/typeorm';
import { TeacherEntity } from './entities/teacher.entity';
import { Repository } from 'typeorm';
import { paginate } from '../utils/functions';
import { BaseService } from '../Generic/base.service';
import { UserType } from '@work-transfert/types';
import { ROLES_ENUM } from '../utils/roles.enum';
import { JoinSchoolInvitationService } from '../join-school-invitation/join-school-invitation.service';

@Injectable()
export class TeacherService extends BaseService<TeacherEntity> {
  constructor(
    @InjectRepository(TeacherEntity)
    private teacherRepository: Repository<TeacherEntity>,
    private userService: UserService,
    private joinSchoolInvitationService: JoinSchoolInvitationService
  ) {
    super(teacherRepository)
  }

  async create(createTeacherDto: CreateTeacherDto) {
    
  }

  async findAllBySchoolId(queries, schoolId: string) {
    const query = await this.teacherRepository.createQueryBuilder("entity")
      .leftJoinAndSelect("entity.teacherSchools", "teacherSchools")
      .leftJoinAndSelect("entity.user", "user")
      .where("teacherSchools.id = :id", { id: schoolId })

    return await paginate(query, queries);
  }

  findAll() {
    return `This action returns all teacher`;
  }

  async findOne(id: string) {
    return await this.teacherRepository.createQueryBuilder("teacher")
      .leftJoinAndSelect("teacher.teacherSchools", "teacherSchools")
      .leftJoinAndSelect("teacher.user", "user")
      .where("teacher.id = :id", { id })
      .getOne();
  }

  async softDelete(queries: any, user: UserType) {
    return super.softDelete(queries, user, "teacherSchools");
  }

  async update(id: string, updateTeacherDto: UpdateTeacherDto, user: UserType) {
    const teacher = await this.teacherRepository.createQueryBuilder("teacher")
      .where("teacher.id = :id", { id })
      .getOne();

    if (!teacher) {
      throw new HttpException(
        "Teacher not found",
        HttpStatus.NOT_FOUND,
      )
    }

    if (user.role === ROLES_ENUM.TEACHER) {
      if (user.teacher.id !== teacher.id) {
        throw new HttpException(
          "You are not allowed to update this teacher",
          HttpStatus.UNAUTHORIZED,
        )
      }
    }

    return await this.teacherRepository.save(updateTeacherDto)
  }

  remove(id: number) {
    return `This action removes a #${id} teacher`;
  }
}
