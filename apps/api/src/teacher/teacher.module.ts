import { Module } from '@nestjs/common';
import { TeacherService } from './teacher.service';
import { TeacherController } from './teacher.controller';
import { UserModule } from '../user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TeacherEntity } from './entities/teacher.entity';
import { JoinSchoolInvitationModule } from '../join-school-invitation/join-school-invitation.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([TeacherEntity]),
    UserModule,
    JoinSchoolInvitationModule
  ],
  controllers: [TeacherController],
  providers: [TeacherService]
})
export class TeacherModule {}
