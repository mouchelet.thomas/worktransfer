import { Test, TestingModule } from '@nestjs/testing';
import { JoinSchoolInvitationService } from './join-school-invitation.service';

describe('JoinSchoolInvitationService', () => {
  let service: JoinSchoolInvitationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [JoinSchoolInvitationService],
    }).compile();

    service = module.get<JoinSchoolInvitationService>(JoinSchoolInvitationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
