import { TeacherSchoolType } from "@work-transfert/types";

export class CreateJoinSchoolInvitationDto {
    code?: string;
    code_expiration?: Date;
    teacherSchool?: TeacherSchoolType;
    teacher_school_email?: string;
}
