import { PartialType } from '@nestjs/mapped-types';
import { CreateJoinSchoolInvitationDto } from './create-join-school-invitation.dto';

export class UpdateJoinSchoolInvitationDto extends PartialType(CreateJoinSchoolInvitationDto) {}
