import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { JoinSchoolInvitationService } from './join-school-invitation.service';
import { CreateJoinSchoolInvitationDto } from './dto/create-join-school-invitation.dto';
import { UpdateJoinSchoolInvitationDto } from './dto/update-join-school-invitation.dto';

@Controller('join-school-invitations')
export class JoinSchoolInvitationController {
  constructor(private readonly joinSchoolInvitationService: JoinSchoolInvitationService) {}

  @Post()
  create(@Body() createJoinSchoolInvitationDto: CreateJoinSchoolInvitationDto) {
    return this.joinSchoolInvitationService.create(createJoinSchoolInvitationDto);
  }

  @Get()
  findAll() {
    return this.joinSchoolInvitationService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.joinSchoolInvitationService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateJoinSchoolInvitationDto: UpdateJoinSchoolInvitationDto) {
    return this.joinSchoolInvitationService.update(+id, updateJoinSchoolInvitationDto);
  }

  @Delete(':id')
  softDelete(@Param('id') id: string) {
    return this.joinSchoolInvitationService.softDelete(id);
  }
}
