import { Test, TestingModule } from '@nestjs/testing';
import { JoinSchoolInvitationController } from './join-school-invitation.controller';
import { JoinSchoolInvitationService } from './join-school-invitation.service';

describe('JoinSchoolInvitationController', () => {
  let controller: JoinSchoolInvitationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [JoinSchoolInvitationController],
      providers: [JoinSchoolInvitationService],
    }).compile();

    controller = module.get<JoinSchoolInvitationController>(JoinSchoolInvitationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
