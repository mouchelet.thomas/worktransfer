import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateJoinSchoolInvitationDto } from './dto/create-join-school-invitation.dto';
import { UpdateJoinSchoolInvitationDto } from './dto/update-join-school-invitation.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { JoinSchoolInvitationEntity } from './entities/join-school-invitation.entity';
import { Repository } from 'typeorm';
import { UserType } from '@work-transfert/types';
import { TeacherSchoolService } from '../teacher-school/teacher-school.service';

@Injectable()
export class JoinSchoolInvitationService {
  constructor(
    @InjectRepository(JoinSchoolInvitationEntity)
    private joinSchoolInvitationRepository: Repository<JoinSchoolInvitationEntity>,
  ) {}

  async create(createJoinSchoolInvitationDto: CreateJoinSchoolInvitationDto) {
    const now = new Date();
    const expirationDate = new Date(now);
    expirationDate.setDate(now.getDate() + 7);
    createJoinSchoolInvitationDto.code_expiration = expirationDate;
    createJoinSchoolInvitationDto.code = this.generateRandomString();
    
    return await this.joinSchoolInvitationRepository.save(createJoinSchoolInvitationDto)
  }

  generateRandomString() {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const length = 4;
    let result = '';
  
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
  
    return result;
  }  

  findAll() {
    return `This action returns all joinSchoolInvitation`;
  }

  findOne(id: number) {
    return `This action returns a #${id} joinSchoolInvitation`;
  }

  update(id: number, updateJoinSchoolInvitationDto: UpdateJoinSchoolInvitationDto) {
    return `This action updates a #${id} joinSchoolInvitation`;
  }

  async softDelete(id: string) {
    return await this.joinSchoolInvitationRepository.softDelete(id)
  }
}
