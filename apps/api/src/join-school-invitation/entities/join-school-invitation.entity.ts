import { TeacherSchoolEntity } from "../../teacher-school/entities/teacher-school.entity";
import { TimestampEntites } from "../../Generic/timestamp.entities";
import { Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity("joinSchoolInvitation")
export class JoinSchoolInvitationEntity extends TimestampEntites {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ nullable: true })
    code: string;

    @Column({ nullable: true })
    code_expiration: Date;

    @OneToOne(() => TeacherSchoolEntity, (teacherSchool) => teacherSchool.joinSchoolInvitation)
    teacherSchool: TeacherSchoolEntity;
}
