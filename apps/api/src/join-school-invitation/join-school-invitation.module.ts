import { Module } from '@nestjs/common';
import { JoinSchoolInvitationService } from './join-school-invitation.service';
import { JoinSchoolInvitationController } from './join-school-invitation.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JoinSchoolInvitationEntity } from './entities/join-school-invitation.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([JoinSchoolInvitationEntity])
  ],
  controllers: [JoinSchoolInvitationController],
  providers: [JoinSchoolInvitationService],
  exports: [JoinSchoolInvitationService]
})
export class JoinSchoolInvitationModule {}
