import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Query } from '@nestjs/common';
import { SchoolService } from './school.service';
import { CreateSchoolDto } from './dto/create-school.dto';
import { UpdateSchoolDto } from './dto/update-school.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from '../decorators/role.decorator';
import { User } from '../decorators/user.decorator';
import { UserType } from '@work-transfert/types';

@Controller('schools')
export class SchoolController {
  constructor(private readonly schoolService: SchoolService) {}

  @Post()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  create(
    @Body() createSchoolDto: CreateSchoolDto,
    @User() user: UserType
  ) {
    return this.schoolService.create(createSchoolDto, user);
  }

  @Get()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school", "teacher")
  findAll(
    @User() user: UserType,
    @Query() queries,
  ) {
    return this.schoolService.findAll(user, queries);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.schoolService.findOne(id);
  }

  @Patch(":id")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  update(
    @Param('id') id: string, 
    @Body() updateSchoolDto: UpdateSchoolDto,
    @User() user: UserType
  ) {
    return this.schoolService.update(id, updateSchoolDto, user);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  softDelete(
    @Param('id') id: string,
    @User() user: UserType
  ) {
    return this.schoolService.softDelete(id, user);
  }
}
