import { CurriculumEntity } from "../../curriculum/entities/curriculum.entity";
import { TimestampEntites } from "../../Generic/timestamp.entities";
import { SubjectEntity } from "../../subject/entities/subject.entity";
import { UserEntity } from "../../user/entities/user.entity";
import { BeforeInsert, Column, Entity, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn, ManyToMany, JoinTable, ManyToOne } from "typeorm";
import { StudentClassEntity } from "../../student-class/entities/student-class.entity";
import { TeacherSchoolEntity } from "../../teacher-school/entities/teacher-school.entity";

@Entity("school")
export class SchoolEntity extends TimestampEntites {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @ManyToOne(() => UserEntity, (user) => user.schools)
    user: UserEntity

    @OneToMany(() => SubjectEntity, (subject) => subject.school)
    subjects: SubjectEntity[];
    
    @OneToMany(() => CurriculumEntity, curriculum => curriculum.school)
    curriculums: CurriculumEntity[]
    
    @OneToMany(() => StudentClassEntity, studentClass => studentClass.school)
    studentClasses: StudentClassEntity[]

    @OneToMany(() => TeacherSchoolEntity, (teacherSchool) => teacherSchool.school)
    teacherSchools: TeacherSchoolEntity[]
}
