import { UserType } from "@work-transfert/types";

export class CreateSchoolDto {
    name: string;
    user?: UserType;
}
