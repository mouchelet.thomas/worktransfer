import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserType } from '@work-transfert/types';
import { Repository } from 'typeorm';
import { CreateSchoolDto } from './dto/create-school.dto';
import { UpdateSchoolDto } from './dto/update-school.dto';
import { SchoolEntity } from './entities/school.entity';
import { ROLES_ENUM } from '../utils/roles.enum';
import { LIMIT_PER_PAGE } from "../utils/constants"
import { paginate } from '../utils/functions';

@Injectable()
export class SchoolService {
  constructor(
    @InjectRepository(SchoolEntity)
    private schoolRepository: Repository<SchoolEntity>,
  ) {}

  async create(createSchoolDto: CreateSchoolDto, user: UserType) {
    try {
      return await this.schoolRepository.save({
        ...createSchoolDto,
        user: user.role === ROLES_ENUM.ADMIN ? createSchoolDto.user : user
      });  
    } catch (error) {
      throw new HttpException(
        error.message,
        HttpStatus.INTERNAL_SERVER_ERROR,
      )
    }
  }

  async findAll(user: UserType, queries) {
    const query= await this.schoolRepository.createQueryBuilder("entity")
      .leftJoinAndSelect("entity.user", "user")
      .leftJoinAndSelect("entity.teacherSchools", "teacherSchools")

    if(user.role === ROLES_ENUM.SCHOOL) {
      query
        .where("user.id = :id", { id: user.id })
    }

    if(user.role === ROLES_ENUM.TEACHER) {
      query
        .leftJoinAndSelect("teacherSchools.teacher", "teacher")
        .where("teacher.id = :id", { id: user.teacher.id })
    }

    return await paginate(query, queries);
  }

  async findOne(id: string) {
    return await this.schoolRepository.createQueryBuilder("school")
      .leftJoinAndSelect("school.user", "user")
      .leftJoinAndSelect("school.teacherSchools", "teacherSchools")
      .where("school.id = :id", { id })
      .getOne();
  }

  async checkIfSchoolExist(id: string) {
    const school = await this.findOne(id);

    if(!school) {
      throw new HttpException(
        'School not found',
        HttpStatus.NOT_FOUND,
      );
    }

    return school;
  }

  checkIfUserIsAllowed(user: UserType, school: SchoolEntity) {
    if(user.role === ROLES_ENUM.SCHOOL && user.id !== school.user.id){
      throw new HttpException(
        'You are not allowed to delete this school',
        HttpStatus.UNAUTHORIZED,
      );
    }
  }

  async update(id: string, updateSchoolDto: UpdateSchoolDto, user: UserType) {
    const school = await this.checkIfSchoolExist(id);
    // Si l'utilisateur est une école et le propriétaire de l'école
    this.checkIfUserIsAllowed(user, school);

    try {
      return await this.schoolRepository.save(updateSchoolDto)
    } catch (error) {
      throw new HttpException(
        error.message,
        HttpStatus.INTERNAL_SERVER_ERROR,
      )
    }
  }

  async softDelete(id: string, user: UserType) {
    const school = await this.checkIfSchoolExist(id);
    // Si l'utilisateur est une école et le propriétaire de l'école
    this.checkIfUserIsAllowed(user, school);

    try {
      return await this.schoolRepository.softDelete({id});
    }
    catch (error) {
      throw new HttpException(
        error.message,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
