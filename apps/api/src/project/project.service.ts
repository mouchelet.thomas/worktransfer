import { HttpException, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ProjectEntity } from './entities/project.entity';
import { Repository } from 'typeorm';
import { UserType } from '@work-transfert/types';
import { LIMIT_PER_PAGE } from '../utils/constants';
import { paginate } from '../utils/functions';
import { ROLES_ENUM } from '../utils/roles.enum';
import { SubjectService } from '../subject/subject.service';
import { StudentClassService } from '../student-class/student-class.service';

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(ProjectEntity)
    private projectRepository: Repository<ProjectEntity>,
    private subjectService: SubjectService,
    private StudentClassService: StudentClassService,
  ) {}

  async create(createProjectDto: CreateProjectDto, user) {
    const subject = await this.subjectService.findOne(createProjectDto.subject.id);

    // Récupérer les entités StudentClass en utilisant les identifiants reçus
    const studentClasses = await this.StudentClassService.findByIds(
      createProjectDto.studentClasses.map((sc) => sc.id)
    );

    const school = subject.school

    // Récupérer le teacherSchool dans le user en fonction de l'école
    const teacherSchool = user.teacher.teacherSchools.find((ts) => ts.school.id === school.id);

    const project = new ProjectEntity();
    project.name = createProjectDto.name;
    project.subject = subject;
    project.studentClasses = studentClasses;
    project.teacherSchool = teacherSchool;

    return await this.projectRepository.save(project);
  }

  async findAllBySchoolId(user: UserType, queries, id: string) {
    const query = await this.projectRepository.createQueryBuilder("entity")
      .leftJoinAndSelect("entity.delivrables", "delivrables")
      .leftJoinAndSelect("entity.teacherSchool", "teacherSchool")
      .leftJoinAndSelect("teacherSchool.teacher", "teacher")
      .leftJoinAndSelect("entity.subject", "subject")
      .leftJoinAndSelect("subject.school", "school")
      .where("school.id = :schoolId", { schoolId: id })
      
      if(user.role === ROLES_ENUM.TEACHER) {
        await query.andWhere("teacher.id = :teacherId", { teacherId: user.teacher.id })
      }
    
    try {
        return await paginate(query, queries);
      } catch (error) {
        throw new HttpException(
          'Project not found',
          HttpStatus.NOT_FOUND,
        );
      }

  }

  async findOne(id: string) {
    try {
      return await this.projectRepository.createQueryBuilder("project")
        .leftJoinAndSelect("project.teacherSchool", "teacherSchool")
        .leftJoinAndSelect("teacherSchool.teacher", "teacher")
        .leftJoinAndSelect("project.subject", "subject")
        .leftJoinAndSelect("subject.school", "school")
        .leftJoinAndSelect("project.studentClasses", "studentClasses")
        .leftJoinAndSelect("project.delivrables", "delivrables")
        .leftJoinAndSelect("delivrables.delivrableUser", "delivrableUser")
        .leftJoinAndSelect("delivrables.delivrableUploadFile", "delivrableUploadFile")
        .leftJoinAndSelect("delivrables.delivrableLinks", "delivrableLinks")
        .where("project.id = :id", { id })
        .getOne();
    } catch (error) {
      throw new HttpException(
        'Project not found',
        HttpStatus.NOT_FOUND,
      );
    }
  }

  async update(id: string, updateProjectDto: UpdateProjectDto, user: UserType) {
    if(user.role === ROLES_ENUM.TEACHER && user.teacher.id !== updateProjectDto.teacherSchool.teacher.id) {
      throw new UnauthorizedException("You can't update this project");
    }
    
    return await this.projectRepository.save(updateProjectDto);
  }

  remove(id: string) {
    return `This action removes a #${id} project`;
  }
}
