import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Query } from '@nestjs/common';
import { ProjectService } from './project.service';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from '../decorators/role.decorator';
import { User } from '../decorators/user.decorator';
import { UserType } from '@work-transfert/types';

@Controller('projects')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @Post()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles("teacher")
  create(
    @Body() createProjectDto: CreateProjectDto,
    @User() user: UserType,
  ) {
    return this.projectService.create(createProjectDto, user);
  }

  @Get("/schools/:id")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school", "teacher")
  findAll(
    @User() user: UserType,
    @Query() queries,
    @Param('id') id: string
  ) {
    return this.projectService.findAllBySchoolId(user, queries, id);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.projectService.findOne(id);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school", "teacher")
  update(
    @Param('id') id: string, 
    @Body() updateProjectDto: UpdateProjectDto,
    @User() user: UserType,
  ) {
    return this.projectService.update(id, updateProjectDto, user);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.projectService.remove(id);
  }
}
