import { TeacherSchoolType, TeacherType } from "@work-transfert/types";

export class CreateProjectDto {
    name: string;
    teacherSchool?: TeacherSchoolType;
    subject: {id: string};
    studentClasses: {id: string}[];
}
