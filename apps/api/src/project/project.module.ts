import { Module } from '@nestjs/common';
import { ProjectService } from './project.service';
import { ProjectController } from './project.controller';
import { ProjectEntity } from './entities/project.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SubjectModule } from '../subject/subject.module';
import { StudentClassModule } from '../student-class/student-class.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ProjectEntity]),
    SubjectModule,
    StudentClassModule
  ],
  controllers: [ProjectController],
  providers: [ProjectService],
  exports: [ProjectService]
})
export class ProjectModule {}
