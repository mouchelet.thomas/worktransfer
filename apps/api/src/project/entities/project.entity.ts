import { StudentClassEntity } from "../../student-class/entities/student-class.entity";
import { TimestampEntites } from "../../Generic/timestamp.entities";
import { DelivrableEntity } from "../../delivrable/entities/delivrable.entity";
import { SubjectEntity } from "../../subject/entities/subject.entity";
import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { TeacherSchoolEntity } from "../../teacher-school/entities/teacher-school.entity";

@Entity("project")
export class ProjectEntity extends TimestampEntites {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @ManyToOne(() => SubjectEntity, subject => subject.projects)
    subject: SubjectEntity;

    @OneToMany(() => DelivrableEntity, delivrable => delivrable.project, {
        nullable: true, 
    })
    delivrables: DelivrableEntity[];

    @ManyToMany(() => StudentClassEntity, studentClass => studentClass.projects)
    @JoinTable()
    studentClasses: StudentClassEntity[];

    @ManyToOne(() => TeacherSchoolEntity, teacherSchool => teacherSchool.projects)
    teacherSchool: TeacherSchoolEntity;
}
