import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { CreateStudentClassDto } from './dto/create-student-class.dto';
import { UpdateStudentClassDto } from './dto/update-student-class.dto';
import { BaseService } from '../Generic/base.service';
import { StudentClassEntity } from './entities/student-class.entity';
import { LIMIT_PER_PAGE } from '../utils/constants';
import { paginate } from '../utils/functions';
import { UserType } from '@work-transfert/types';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { SchoolService } from '../school/school.service';

@Injectable()
export class StudentClassService extends BaseService<StudentClassEntity> {
  constructor(
    @InjectRepository(StudentClassEntity)
    private studentClassRepository: Repository<StudentClassEntity>,
    @Inject(SchoolService)
    private readonly schoolService: SchoolService,
  ) {
    super(studentClassRepository)
  }

  async create(createStudentClassDto: CreateStudentClassDto) {
    try {
      return await this.studentClassRepository.save(createStudentClassDto);
    } catch (error) {
      throw new HttpException(
        error.message,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async findAllBySchoolId(queries, schoolId: string) {
    const query = await this.studentClassRepository.createQueryBuilder("entity")
      .leftJoinAndSelect("entity.school", "school")
      .leftJoinAndSelect("entity.curriculums", "curriculums")
      .where("school.id = :id", { id: schoolId })

    return await paginate(query, queries);
  }

  async findByIds(ids: string[]) {
    return await this.studentClassRepository.createQueryBuilder("studentClass")
      .where("studentClass.id IN (:...ids)", { ids })
      .getMany();
  }

  async findOne(id: string) {
    return await this.studentClassRepository.createQueryBuilder("studentClass")
      .leftJoinAndSelect("studentClass.school", "school")
      .leftJoinAndSelect("studentClass.curriculums", "curriculums")
      .where("studentClass.id = :id", { id })
      .getOne();
  }

  async update(id: string, updateStudentClassDto: UpdateStudentClassDto) {
      const studentClass = await this.findOne(id);

      if(!studentClass){
        throw new HttpException(
          'StudentClass not found',
          HttpStatus.NOT_FOUND,
        );
      }

      try {
        return await this.studentClassRepository.save({
          ...studentClass,
          ...updateStudentClassDto,
        });
      }
      catch (error) {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
  }

  async softDelete(queries: any, user: UserType) {
    return super.softDelete(queries, user, "school");
  }
}
