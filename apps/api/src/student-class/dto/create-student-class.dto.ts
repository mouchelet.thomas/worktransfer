import { SchoolType } from "@work-transfert/types";

export class CreateStudentClassDto {
    school: SchoolType
}
