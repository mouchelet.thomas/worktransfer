import { TimestampEntites } from "../../Generic/timestamp.entities";
import { CurriculumEntity } from "../../curriculum/entities/curriculum.entity";
import { ProjectEntity } from "../../project/entities/project.entity";
import { SchoolEntity } from "../../school/entities/school.entity";
import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity("studentClass")
export class StudentClassEntity extends TimestampEntites {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @ManyToMany(() => CurriculumEntity, curriculum => curriculum.studentClasses)
    @JoinTable()
    curriculums: CurriculumEntity[];

    @ManyToMany(() => ProjectEntity, project => project.studentClasses)
    projects: ProjectEntity[];

    @ManyToOne(() => SchoolEntity, school => school.studentClasses)
    school: SchoolEntity;
}
