import { Module } from '@nestjs/common';
import { StudentClassService } from './student-class.service';
import { StudentClassController } from './student-class.controller';
import { StudentClassEntity } from './entities/student-class.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SchoolModule } from '../school/school.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([StudentClassEntity]),
    SchoolModule
  ],
  controllers: [StudentClassController],
  providers: [StudentClassService],
  exports: [StudentClassService]
})
export class StudentClassModule {}
