import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Query } from '@nestjs/common';
import { StudentClassService } from './student-class.service';
import { CreateStudentClassDto } from './dto/create-student-class.dto';
import { UpdateStudentClassDto } from './dto/update-student-class.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from '../decorators/role.decorator';
import { UserType } from '@work-transfert/types';
import { User } from '../decorators/user.decorator';

@Controller('student-classes')
export class StudentClassController {
  constructor(private readonly studentClassService: StudentClassService) {}

  @Post()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  create(@Body() createStudentClassDto: CreateStudentClassDto) {
    return this.studentClassService.create(createStudentClassDto);
  }

  // TODO: Fix this
  @Post("multiple")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  createMultiple(@Body() createStudentClassDto: CreateStudentClassDto) {
    return this.studentClassService.create(createStudentClassDto);
  }

  @Get("/schools/:id")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school", "teacher")
  findAllBySchoolId(
    @Query() queries,
    @Param('id') schoolId: string,
  ) {
    return this.studentClassService.findAllBySchoolId(queries, schoolId);
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school","teacher")
  findOne(
    @Param('id') id: string,
    @User() user: UserType,
  ) {
    return this.studentClassService.findOne(id);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  update(
    @Param('id') id: string, 
    @Body() updateStudentClassDto: UpdateStudentClassDto,
    @User() user: UserType,
  ) {
    return this.studentClassService.update(id, updateStudentClassDto);
  }

  @Delete()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  softDelete(
    @User() user: UserType,
    @Query() queries,
  ) {
    return this.studentClassService.softDelete(queries, user);
  }
}
