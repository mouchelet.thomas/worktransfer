import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    
    if (!roles) {
        // Si aucune métadonnée de rôle n'est définie, l'accès est autorisé.
        return true;
    }
    
    const request = context.switchToHttp().getRequest();
    const user = request.user;

    if (!user.role || !roles.includes(user.role)) {
      // Si l'utilisateur n'est pas connecté ou s'il n'a pas de rôle, l'accès est refusé.
      throw new UnauthorizedException();
    }

    return true
  }
}