import { ConflictException, Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UserService } from "../user/user.service"
import { UserEntity } from '../user/entities/user.entity';
import { TokenPasswordEntity } from './entities/tokenPassword.entity';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(UserEntity)
        private userRepository: Repository<UserEntity>,
        @InjectRepository(TokenPasswordEntity)
        private tokenPasswordRepository: Repository<TokenPasswordEntity>,
        private jwtService: JwtService,
        private userService: UserService,
        private mailerService: MailerService,
    ) { }

    async signup(userData) {
        const user = await this.userService.create(userData);

        return this.generateToken(user);
    }

    async signin(credentials) {
        const user = await this.userService.findOneByEmailWithPassword(credentials.email);

        if (!user) {
            throw new ConflictException("Email not found");
        }

        user.deletedAt = null;
        
        const isValid = await bcrypt.compare(credentials.password, user.password);

        if (!isValid) {
            throw new ConflictException("Password is not valid");
        }
        
        await this.userRepository.save(user);

        delete user.password;

        return await this.generateToken(user);
    }

    async regenerateToken(user) {
        const payload = {...user}
        
        try {
            const jwt = await this.jwtService.sign(payload);
            return {accessToken: jwt};
        } catch (error) {
            throw new ConflictException("Erreur lors de la génération du token");
        }
    }

    async generateToken(user) {
        const payload = {...user}

        try {
            const jwt = await this.jwtService.sign(payload);

            return { accessToken: jwt };
        } catch (error) {
            throw new ConflictException("Erreur lors de la génération du token");
        }
    }

    verifyJwt(jwt: string): Promise<any> {
        return this.jwtService.verifyAsync(jwt);
    }

    async resetPassword(data) {
        const {token, password} = data

        const tokenPassword = await this.tokenPasswordRepository.findOne({ where: { token } });

        if (!tokenPassword) {
            throw new ConflictException("Token not found");
        }

        const user = await this.userRepository.findOne({ where: { email: tokenPassword.email } });

        if (!user) {
            throw new ConflictException("Email not found");
        }

        user.password = await bcrypt.hash(password, 10);
        await this.userRepository.save(user);

        await this.tokenPasswordRepository.delete({id: tokenPassword.id});
    }

    async forgotPassword({ email }) {
        const user = await this.userService.findOneByEmail(email);

        if (!user) {
            throw new ConflictException("Adresse mail introuvable");
        }

        const token = {
            email: email,
            token: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
        }

        await this.tokenPasswordRepository.save(token)

        this.mailerService.sendMail({
            to: email,
            subject: 'Réinitialisation de votre mot de passe',
            template: process.cwd() + '/templates/auth/forgot-password',
            context: {
                email,
                APP_FRONT_LINK: `${process.env.APP_FRONT_LINK}/auth/reset-password/${token.token}`,
            }
        })

    }

    async signinAdmin(credentials) {
        if(credentials.username !== process.env.ADMIN_USERNAME || credentials.password !== process.env.ADMIN_PASSWORD) {
            throw new ConflictException("Email ou mot de passe incorrect");
        }

        const payload = {
            username: process.env.ADMIN_USERNAME,
            roles: ['admin']
        }

        try {
            const jwt = await this.jwtService.sign(payload);

            return {accessToken: jwt};
        } catch (error) {
            throw new ConflictException("Erreur lors de la génération du token");
        }
    }
}
