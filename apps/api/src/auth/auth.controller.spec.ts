import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

describe('AuthController', () => {
  let controller: AuthController;

  const mockAuthService = {
    signup: jest.fn((userData) => {
        return {
            accessToken: "token"
        }
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [AuthService],
    }).overrideProvider(AuthService).useValue(mockAuthService).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be signup user school', () => {
    expect(controller.signup({
        email: "admin@esd.fr",
        password: "password",
        role: "school",
        schools: [
            {
                name: "ESD"
            }
        ]
    }))
    .resolves.toEqual({
        accessToken: expect.any(String),
    })
  })
});
