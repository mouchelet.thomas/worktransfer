import { TimestampEntites } from "../../Generic/timestamp.entities";
import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity('token_password')
export class TokenPasswordEntity extends TimestampEntites {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ nullable: true })
    token: string;

    @Column({ nullable: true })
    email: string;
}
