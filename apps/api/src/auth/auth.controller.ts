import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { User } from '../decorators/user.decorator';

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService,
    ) {}

    @Post('signup')
    async signup(
        @Body() userData
    ) {
        return await this.authService.signup(userData);
    }

    @Post('signin')
    async signin(
        @Body() credentials
    ) {
        return await this.authService.signin(credentials);
    }

    @Get('regenerate-token')
    @UseGuards(JwtAuthGuard)
    async regenerateToken(
        @User() user
    ) {
        return await this.authService.regenerateToken(user);
    }

    @Post('forgot-password')
    async forgotPassword(
        @Body() data
    ) {
        return await this.authService.forgotPassword(data);
    }

    @Post('reset-password')
    async resetPassword(
        @Body() data
    ) {
        return await this.authService.resetPassword(data);
    }

    @Post('signin-admin')
    async signinAdmin(
        @Body() credentials
    ) {
        return await this.authService.signinAdmin(credentials);
    }
}
