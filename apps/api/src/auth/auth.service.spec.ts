import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UserEntity } from '../user/entities/user.entity';
import { TokenPasswordEntity } from './entities/tokenPassword.entity';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';
import { MailerService } from '@nestjs-modules/mailer';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('AuthService', () => {
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: getRepositoryToken(UserEntity),
          useValue: {},
        },
        {
          provide: getRepositoryToken(TokenPasswordEntity),
          useValue: {},
        },
        {
            provide: JwtService,
            useValue: {
              sign: jest.fn().mockImplementation(() => 'fake_jwt_token'),
            },
        },
        {
            provide: UserService,
            useValue: {
              create: jest.fn().mockImplementation(() => {
                return {
                  // Add expected user object properties
                };
              }),
            },
        },
        {
          provide: MailerService,
          useValue: {},
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  Example:
  it('should call the signup method and return the token', async () => {
    // Arrange
    const userData = {
        email: "admin2@esd.fr",
        password: "password",
        role: "school",
        schools: [
            {
                name: "ESD"
            }
        ]
    };
    const expectedToken = { 
        accessToken: expect.any(String),
     };
  
    // Act
    const result = await authService.signup(userData);
  
    // Assert
    expect(result).toEqual(expectedToken);
  });
});
