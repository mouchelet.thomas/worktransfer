import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UserModule } from '../user/user.module';
import { JwtStrategy } from './strategy/passport-jwt.strategy';
import { UserEntity } from '../user/entities/user.entity';
import { TokenPasswordEntity } from './entities/tokenPassword.entity';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forFeature([
      UserEntity,
      TokenPasswordEntity
    ]),
    PassportModule.register(
      { defaultStrategy: 'jwt' }
    ),
    JwtModule.register({
        secret: process.env.JWT_SECRET,
        signOptions: { expiresIn: '360000s' },
        // signOptions: { expiresIn: '3600s' },
    }),
    UserModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
