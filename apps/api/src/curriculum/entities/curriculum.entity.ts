import { StudentClassEntity } from "../../student-class/entities/student-class.entity";
import { SchoolEntity } from "../../school/entities/school.entity";
import { SubjectEntity } from "../../subject/entities/subject.entity";
import { Column, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { TimestampEntites } from "../../Generic/timestamp.entities";

@Entity('curriculum')
export class CurriculumEntity extends TimestampEntites {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @ManyToOne(() => SchoolEntity, school => school.curriculums)
    school: SchoolEntity;

    @ManyToMany(() => SubjectEntity, subject => subject.curriculums, {
        cascade: true
    })
    subjects: SubjectEntity[];

    @ManyToMany(() => StudentClassEntity, studentClass => studentClass.curriculums)
    studentClasses: StudentClassEntity[];
}
