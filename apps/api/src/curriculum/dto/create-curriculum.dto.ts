import { SchoolType, SubjectType } from "@work-transfert/types";

export class CreateCurriculumDto {
    name: string;
    school: SchoolType
    subjects: SubjectType[]
}
