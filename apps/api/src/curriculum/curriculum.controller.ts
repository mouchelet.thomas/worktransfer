import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Query } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from '../decorators/role.decorator';
import { UserType } from '@work-transfert/types';
import { User } from '../decorators/user.decorator';
import { CurriculumService } from './curriculum.service';
import { CreateCurriculumDto } from './dto/create-curriculum.dto';
import { UpdateCurriculumDto } from './dto/update-curriculum.dto';

@Controller('curriculums')
export class CurriculumController {
  constructor(private readonly curriculumService: CurriculumService) {}

  @Post()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  create(@Body() createCurriculumDto: CreateCurriculumDto) {
    return this.curriculumService.create(createCurriculumDto);
  }
  // TODO: Fix this
  @Post("multiple")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  createMultiple(@Body() createCurriculumDto: CreateCurriculumDto) {
    return this.curriculumService.create(createCurriculumDto);
  }

  @Get("/schools/:id")
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school", "teacher")
  findAllBySchoolId(
    @Query() queries,
    @Param('id') schoolId: string,
  ) {
    return this.curriculumService.findAllBySchoolId(queries, schoolId);
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school","teacher")
  findOne(
    @Param('id') id: string,
    @User() user: UserType,
  ) {
    return this.curriculumService.findOne(id);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  update(
    @Param('id') id: string, 
    @Body() updateCurriculumDto: UpdateCurriculumDto,
    @User() user: UserType,
  ) {
    return this.curriculumService.update(id, updateCurriculumDto);
  }

  @Delete()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', "school")
  softDelete(
    @User() user: UserType,
    @Query() queries,
  ) {
    return this.curriculumService.softDelete(queries, user);
  }
}
