import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { BaseService } from '../Generic/base.service';
import { LIMIT_PER_PAGE } from '../utils/constants';
import { paginate } from '../utils/functions';
import { UserType } from '@work-transfert/types';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { SchoolService } from '../school/school.service';
import { CurriculumEntity } from './entities/curriculum.entity';
import { CreateCurriculumDto } from './dto/create-curriculum.dto';
import { UpdateCurriculumDto } from './dto/update-curriculum.dto';

@Injectable()
export class CurriculumService extends BaseService<CurriculumEntity> {
  constructor(
    @InjectRepository(CurriculumEntity)
    private curriculumRepository: Repository<CurriculumEntity>,
    @Inject(SchoolService)
    private readonly schoolService: SchoolService,
  ) {
    super(curriculumRepository)
  }

  async create(createCurriculumDto: CreateCurriculumDto) {
    try {
      return await this.curriculumRepository.save(createCurriculumDto);
    } catch (error) {
      throw new HttpException(
        error.message,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async findAllBySchoolId(queries, schoolId: string) {
    const query = await this.curriculumRepository.createQueryBuilder("entity")
      .leftJoinAndSelect("entity.school", "school")
      .leftJoinAndSelect("entity.subjects", "subjects")
      .where("school.id = :id", { id: schoolId })

    return await paginate(query, queries);
  }

  async findOne(id: string) {
    return await this.curriculumRepository.createQueryBuilder("curriculum")
      .leftJoinAndSelect("curriculum.school", "school")
      .leftJoinAndSelect("curriculum.subjects", "subjects")
      .where("curriculum.id = :id", { id })
      .getOne();
  }

  async update(id: string, updateCurriculumDto: UpdateCurriculumDto) {
      const curriculum = await this.findOne(id);

      if(!curriculum){
        throw new HttpException(
          'Curriculum not found',
          HttpStatus.NOT_FOUND,
        );
      }

      try {
        return await this.curriculumRepository.save({
          ...curriculum,
          ...updateCurriculumDto,
        });
      }
      catch (error) {
        throw new HttpException(
          error.message,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
  }

  async softDelete(queries: any, user: UserType) {
    return super.softDelete(queries, user, "school");
  }
}
