import { Module } from '@nestjs/common';
import { CurriculumService } from './curriculum.service';
import { CurriculumController } from './curriculum.controller';
import { CurriculumEntity } from './entities/curriculum.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SchoolModule } from '../school/school.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([CurriculumEntity]),
    SchoolModule
  ],
  controllers: [CurriculumController],
  providers: [CurriculumService]
})
export class CurriculumModule {}
