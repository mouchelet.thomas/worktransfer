export enum ROLES_ENUM {
    ADMIN = 'admin',
    SCHOOL = 'school',
    TEACHER = 'teacher'
}