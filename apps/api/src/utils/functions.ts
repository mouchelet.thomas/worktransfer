interface PaginatedResult<T> {
    data: T[];
    total: number;
    totalPages: number;
  }
  
export async function paginate<T>(query: any, queries: any) {
    let limit = parseInt(queries.limit) || 7
    let offset = parseInt(queries.offset) || 0
    let page = parseInt(queries.page) || 1
    let order = queries.order || "DESC"

    if(page > 1) offset = (page - 1) * limit
    
    const [data, count] = await query
      .orderBy("entity.createdAt", order)
      .offset(offset)
      .limit(limit)
      .getManyAndCount();
    
    return {
      data,
      totalCount: count,
      totalPages: Math.ceil(count / limit),
      currentPage: Math.ceil(offset / limit) + 1,
    };
}