import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { SubjectEntity } from "../subject/entities/subject.entity";
import { UserEntity } from "../user/entities/user.entity";
import { SchoolEntity } from "../school/entities/school.entity";
import { DatabaseService } from "./database.service";

@Module({
  imports: [
    TypeOrmModule.forFeature([SubjectEntity, UserEntity, SchoolEntity]),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.POSTGRESQL_ADDON_HOST,
      port: parseInt(process.env.POSTGRESQL_ADDON_PORT),
      username: process.env.POSTGRESQL_ADDON_USER,
      password: process.env.POSTGRESQL_ADDON_PASSWORD,
      database: process.env.POSTGRESQL_ADDON_DB,
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      synchronize: true,
      extra: {
        min: 2,
        max: 5,
      },
    }),
  ],
  providers: [DatabaseService],
  exports: [DatabaseService]
})
export class DatabaseModule {}
