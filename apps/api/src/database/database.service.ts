import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';

// Import your entities here
import { UserEntity } from '../user/entities/user.entity';
import { SchoolEntity } from '../school/entities/school.entity';
import { SubjectEntity } from '../subject/entities/subject.entity';

@Injectable()
export class DatabaseService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(SchoolEntity)
    private readonly schoolRepository: Repository<SchoolEntity>,
    @InjectRepository(SubjectEntity)
    private readonly subjectRepository: Repository<SubjectEntity>,
  ) {}

  async resetDatabase(): Promise<void> {
    // Clear all tables
    const connection = getConnection();

    // Check if the connection is not connected
    if (!connection.isConnected) {
      // If it's not connected, try connecting
      await connection.connect();
    }
    await this.userRepository.clear();
    await this.schoolRepository.clear();
    await this.subjectRepository.clear();
  }

  // Optionally, you can also add methods for specific entities
  async deleteAllUsers(): Promise<void> {
    await this.userRepository.clear();
  }

  async deleteAllSubjects(): Promise<void> {
    await this.subjectRepository.clear();
  }

  async deleteAllSchools(): Promise<void> {
    await this.schoolRepository.clear();
  }
}
