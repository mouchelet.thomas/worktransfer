import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { SchoolType } from '@work-transfert/types';
import { checkDate, checkProperties } from './utils/generic.expect';
// import { DatabaseService } from '../src/database/database.service';

describe('App', () => {
  let app: INestApplication;
  let accessToken: string;
  let school: SchoolType
  // let databaseService: DatabaseService;

  const userData = {
    email: `test${Date.now()}@example.com`,
    password: 'test1234',
    role: "school",
    schools: [
      {
          name: "test school",
      }
    ]
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    // databaseService = app.get(DatabaseService);
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });

  describe('Authentification', () => {
    it('POST /auth/signup - should signup a user with valid credentials', async () => {
      // await authService.signup(userData);
      return request(app.getHttpServer())
        .post('/auth/signup')
        .send(userData)
        .expect(201)
        .then((response) => {
          // TODO : verify token
          expect(response.body).toHaveProperty('accessToken');
        });
    });
    
    it('POST /auth/signin - should signin a user with valid credentials', async () => {
      const authResponse = await request(app.getHttpServer())
        .post('/auth/signin')
        .send({
          email: userData.email,
          password: userData.password,
        })
        .expect(201)

        await expect(authResponse.body).toHaveProperty('accessToken');

        accessToken = authResponse.body.accessToken;
    });

    it('POST /auth/forgot-password - should send an email to reset password', async () => {
      await request(app.getHttpServer())
        .post('/auth/forgot-password')
        .send({
          email: userData.email,
        })
        .expect(201)
    });
  })

  describe('Schools', () => {
    const createOneSchoolData = {
          name: `test school ${Date.now()}`,
    }

    it("POST /schools - should create ONE school", async () => {
      const schoolResponse = await request(app.getHttpServer())
        .post('/schools')
        .send(createOneSchoolData)
        .set('Authorization', `Bearer ${accessToken}`)
        .expect(201)

        return expect(schoolResponse.body).toMatchObject({
          id: expect.any(String),
          name: createOneSchoolData.name,
          createdAt: checkDate(),
          updatedAt: checkDate(),
          deletedAt: null,
          user: {
            id: expect.any(String),
            email: userData.email,
            role: userData.role,
            createdAt: checkDate(),
            updatedAt: checkDate(),
            deletedAt: null,
          }
      });
    })

    it("GET /schools - should get all schools", async () => {
      const schoolResponse = await request(app.getHttpServer())
        .get('/schools')
        .set('Authorization', `Bearer ${accessToken}`)
        // set default school selected
        school = schoolResponse.body.data[0]

        return expect(schoolResponse.body).toMatchObject({
          data: expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(String),
              name: createOneSchoolData.name,
              createdAt: checkDate(),
              updatedAt: checkDate(),
              deletedAt: null,
              user: {
                id: expect.any(String),
                email: userData.email,
                role: userData.role,
                createdAt: checkDate(),
                updatedAt: checkDate(),
                deletedAt: null,
              }
            }),
          ]),
          totalCount: expect.any(Number),
          totalPages: expect.any(Number),
          currentPage: expect.any(Number),
        });
    })
  })

  describe('Subjects', () => {
    it("POST /subjests - should create ONE subject", async () => {
      const subjectData = {
        name: `test subject ${Date.now()}`,
        school
      }
  
      const subjectResponse = await request(app.getHttpServer())
        .post('/subjects')
        .set('Authorization', `Bearer ${accessToken}`)
        .send(subjectData)
        .expect(201)

        return checkProperties(subjectResponse.body, subjectData)
    });
  })

  describe('Curriculums', () => {
    it("POST /curriculums - should create ONE curriculum", async () => {
      const curriculumData = {
        name: `test curriculums ${Date.now()}`,
        school
      }
  
      const curriculumResponse = await request(app.getHttpServer())
        .post('/curriculums')
        .set('Authorization', `Bearer ${accessToken}`)
        .send(curriculumData)
        .expect(201)

        return checkProperties(curriculumResponse.body, curriculumData)
    });
  })

  describe('StudentClass', () => {
    it("POST /student-classes - should create ONE studentClass", async () => {
      const studentClassData = {
        name: `test Student Class ${Date.now()}`,
        school
      }
  
      const studentClassResponse = await request(app.getHttpServer())
        .post('/student-classes')
        .set('Authorization', `Bearer ${accessToken}`)
        .send(studentClassData)
        .expect(201)

        return checkProperties(studentClassResponse.body, studentClassData)
    });
  })

  afterAll(async () => {
    // Reset or clean up the database
  });
  afterEach(async () => {
    // await databaseService.resetDatabase()
  //   await app.close();
  });
});
