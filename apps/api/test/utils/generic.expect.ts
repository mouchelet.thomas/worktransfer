import { SchoolType, SubjectType } from "@work-transfert/types";

export const checkProperties = (
        body: Partial<SubjectType>, 
        subjectData: Partial<SubjectType>, 
    ): void => {
        return expect(body).toMatchObject({
            id: expect.any(String),
            name: subjectData.name,
            createdAt: checkDate(),
            updatedAt: checkDate(),
            deletedAt: null,
            school: subjectData.school
        });
  };

export const checkDate = () => {
    return expect.stringMatching(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/)
}
