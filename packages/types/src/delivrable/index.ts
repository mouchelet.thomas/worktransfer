import { ProjectType, SubjectType, TeacherType } from '../index'

export type DelivrableUserType = {
    id: string
    createdAt: Date
    updatedAt: Date
    deletedAt: Date | null,
    firstName: string,
    lastName: string
}

export type DelivrableUploadFileType = {
    id: string,
    ETag: string,
    Location: string,
    Key: string,
    size: number,
    Bucket : string
}

export type DelivrableLinkType = {
    id: string,
    linkUrl: string,
    delivrable: DelivrableType
}

export type DelivrableType = {
    id: string
    createdAt: Date
    updatedAt: Date
    deletedAt: Date | null,
    project: ProjectType,
    delivrableUser: DelivrableUserType,
    delivrableUploadFile?: DelivrableUploadFileType,
    delivrableLinks?: DelivrableLinkType[]
  }