import { SchoolType } from '../index'

export type SubjectType = {
    id: string
    createdAt: Date
    updatedAt: Date
    deletedAt: Date | null,
    name: string,
    school: SchoolType,
  }