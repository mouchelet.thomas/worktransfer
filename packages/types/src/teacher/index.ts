import { UserType, SchoolType, TeacherSchoolType } from '../index'

export type TeacherType = {
    id?: string
    createdAt?: Date
    updatedAt?: Date
    deletedAt?: Date | null
    user?: UserType
    schools?: TeacherSchoolType[]
    firstName?: string
    lastName?: string
  }