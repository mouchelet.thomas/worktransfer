import { SchoolType, TeacherType } from '../index';

export type TeacherSchoolType = {
    id?: string;
    teacher_school_email?: string;
    invitation_accepted?: boolean;
    code?: string | null;
    code_expiration?: Date | null;
    school?: SchoolType;
    teacher?: TeacherType;
    createdAt?: Date
    updatedAt?: Date
    deletedAt?: Date | null,
  };
  