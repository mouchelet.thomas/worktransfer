import { UserType, TeacherSchoolType } from '../index'

export type SchoolType = {
    id: string
    createdAt?: Date
    updatedAt?: Date
    deletedAt?: Date | null,
    name?: string,
    user?: UserType
    teachers?: TeacherSchoolType[]
  }