import { DelivrableType, StudentClassType, SubjectType, TeacherType } from '../index'

export type ProjectType = {
    id: string
    createdAt: Date
    updatedAt: Date
    deletedAt: Date | null,
    name: string,
    teacher?: TeacherType
    subject?: SubjectType,
    studentClasses: StudentClassType[],
    delivrables?: DelivrableType[]
}