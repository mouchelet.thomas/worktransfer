import { CurriculumType, SchoolType } from '../index'

export type StudentClassType = {
    id: string
    createdAt: Date
    updatedAt: Date
    deletedAt: Date | null,
    name: string,
    school: SchoolType,
    curriculums: CurriculumType[],
  }