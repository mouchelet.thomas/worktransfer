import { TeacherType, SchoolType } from '../index'

export enum ROLES_ENUM {
  ADMIN = 'admin',
  SCHOOL = 'school',
  TEACHER = 'teacher'
}

export type UserType = {
    id?: string
    email: string
    password?: string
    createdAt?: Date
    updatedAt?: Date
    role?: ROLES_ENUM
    deletedAt?: Date | null
    schools?: SchoolType[]
    teacher?: TeacherType
}