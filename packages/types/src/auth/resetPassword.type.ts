export type ResetPasswordType = {
    password: string;
    token?: string;
}