import { SchoolType, SubjectType } from '../index'

export type CurriculumType = {
    id: string
    createdAt: Date
    updatedAt: Date
    deletedAt: Date | null,
    name: string,
    subjects?: SubjectType[]
    school: SchoolType
  }