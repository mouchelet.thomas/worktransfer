import { SchoolType } from "../school"

export type CredentialsType = {
    name?: string
    school: SchoolType
    teacher_school_email?: string
    [key: string]: any;
  }