export type JoinSchoolInvitationType = {
    id?: string
    teacher_school_email: string
    code: string
}