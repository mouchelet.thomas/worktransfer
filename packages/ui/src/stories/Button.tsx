import '@work-transfert/styles/src/components/_button.scss';
import { useNavigate } from 'react-router-dom'

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement>  {
  /**
   * Is this the principal call to action on the page?
   */
  primary?: boolean;
  /**
   * What background color to use
   */
  backgroundColor?: string;
  /**
   * How large should the button be?
   */
  size?: 'small' | 'medium' | 'large';
  /**
   * Button contents
   */
  label: string;
  /**
   * Optional link to redirect to
   */
  to?: string;
  /**
   * Optional className
   */
  className?: string;
  /**
   * Optional type
   */
  type?: "button" | "submit" | "reset" | undefined;
  /**
   * Optional color
   */
  color?: "primary" | "secondary" | "danger" | "success" | "warning" | "info" | "light" | "dark" | undefined;
  /**
   * Optional click handler
   */
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void | Promise<void>;
  /**
   * Icon object
   */
  icon?: React.ReactNode;
  /**
   * onLoadingSubmit
   */
  onLoadingSubmit?: boolean
}

/**
 * Primary UI component for user interaction
 */
export const Button = ({
  primary = false,
  color = "primary",
  size = 'medium',
  to, 
  label,
  type = "button",
  className,
  onClick,
  icon,
  onLoadingSubmit = false,
  ...props
}: ButtonProps) => {
  const mode = primary ? 'button--primary' : 'button--secondary';
  const navigate = useNavigate()

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    if(to) {
      event.preventDefault();
      navigate(to);
    } else if (onClick) {
      onClick(event);
    }
  }

  return (
    <button
      type={type}
      className={[
        'button', 
        `button--${size}`, 
        mode, 
        className, 
        `color--${color}`,
        icon ? 'button--icon' : ''
      ].join(' ')}
      {...props}
      onClick={handleClick}
      disabled={onLoadingSubmit || props.disabled}
    >

        
      {onLoadingSubmit ? 
        <>
          
          <span className='spinner'></span>
          
          <span>
            En cours...
          </span>
        </>
        : 
        label
      }
      {icon}
    </button>
  );
};
