import "@work-transfert/styles/src/components/_input.scss"
import "@work-transfert/styles/src/layout/_grid.scss"
import { SingninType, UnknownObject } from "@work-transfert/types"
import { Input } from "../molecules/Input";
import { Button } from "../stories/Button";
import { Link } from "react-router-dom";

interface AuthFormProps { 
  /**
   * Auth credentials
   */
  credentials: SingninType;
  /**
   * Auth setter credentials
   */
  setCredentials: React.Dispatch<React.SetStateAction<SingninType>>;
  /**
   * Optional submit handler
   */
  onSubmit?: (event: React.FormEvent<HTMLFormElement>) => void;
  /**
   * onLoadingSubmit
   */
  onLoadingSubmit?: boolean;
}

/**
 * Primary UI component for user interaction
 */
export const AuthForm = ({
  credentials,
  setCredentials,
  onLoadingSubmit,
  ...props
}: AuthFormProps) => {
  return (
    <form {...props}>
        <div className="form-group mb-2">
          <Input 
            type="text" 
            placeholder="Email" 
            credentials={credentials}
            setCredentials={setCredentials}
            name="email"
          />
        </div>
        <div className="form-group">
          <Input 
            type="password" 
            placeholder="Password" 
            name="password"
            credentials={credentials}
            setCredentials={setCredentials}
          />
        </div>
        <div className="form-group mt-2">
          <div className="typography-caption typography-caption--large text-right">
            <Link to="/auth/forgot-password">Mot de passe oublié ?</Link>
          </div>
        </div>
        <div className="form-group mt-5">
          <Button
              type="submit"
              label="Connexion"
              primary={true}
              size="large"
              className="w-100"
              onLoadingSubmit={onLoadingSubmit}
          />
        </div>
        <div className="form-group mt-2">
          <Button
              type="submit"
              label="Inscription"
              size="large"
              className="w-100"
              to="/auth/signup"
          />
        </div>
    </form>
  );
};
