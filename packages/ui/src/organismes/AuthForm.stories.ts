import type { Meta, StoryObj } from '@storybook/react';

import { AuthForm } from './AuthForm';

const meta = {
  title: 'organismes/AuthForm',
  component: AuthForm,
  tags: ['autodocs'],
  argTypes: {
    credentials: { credentials: 'object' },
  },
} satisfies Meta<typeof AuthForm>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Signin: Story = {
  args: {
    credentials: {
        email: "test",
        password: "test"
    }
  },
};