import { CredentialsType, ErrorResponse } from "@work-transfert/types";
import { useContext, useEffect, useState } from "react";
import { Input } from "../../molecules/Input";
import { SchoolContext } from "@work-transfert/contexts";
import { Button } from "../../stories/Button";
import toast from 'react-hot-toast';

interface ItemFormAddProps {
    entityService: any;
    fetchAllItems: () => void;
    item?: CredentialsType;
    entityLabel?: string;
    showButton?: boolean;
    inputType?: string;
    inputName?: string;
}

export const ItemFormAdd = (
    {
        entityService,
        fetchAllItems,
        item,
        entityLabel,
        showButton = false,
        inputType = "text",
        inputName = "name"
    }: ItemFormAddProps) => {
        const { school } = useContext(SchoolContext);
        const [credentials, setCredentials] = useState({} as CredentialsType)
        const [onLoadingSubmit, setOnLoadingSubmit] = useState(false);

        useEffect(() => {
            setCredentials({
                ...credentials,
                school, 
            })
            if(item) setCredentials(item)
        }, [school]);

        const handleSubmit = async (event: React.FormEvent<HTMLFormElement> | React.MouseEvent<HTMLElement>) => { 
            event.preventDefault();
            setOnLoadingSubmit(true);
            
            try {
                const method = item ? 'update' : 'create';
                await entityService[method](credentials);
                !item && fetchAllItems()
                !item && setCredentials({school} as CredentialsType)
            } catch (error) {
                toast.error((error as ErrorResponse).response.data.message)
            } finally {
                setOnLoadingSubmit(false);
            }
        }

        return (
            <form
                onSubmit={handleSubmit}
                className="form-add-item"
            >
                <Input
                    placeholder={`Ajouter ${entityLabel || 'un élément'}`}
                    name={inputName}
                    type={inputType}
                    value={credentials[inputName] || ''}
                    credentials={credentials}
                    setCredentials={setCredentials}
                    haveItem={!!item}
                    handleSubmit={handleSubmit}
                />
                {showButton && 
                    <Button
                        type="submit"
                        label="Ajouter"
                        primary={true}
                        size="medium"
                        onLoadingSubmit={onLoadingSubmit}
                    />
                }
            </form>
        );
}