import React from "react";

interface ListRowProps {
    item: any;
    children?: React.ReactNode;
    allowActions?: boolean;
    handleSelect: any
    selected: boolean;
}

export const ListRow = ({
  selected,
  handleSelect,
  item,
  children,
  allowActions = false
}: ListRowProps) => {

  return (
    <>
        {allowActions && 
            <div className="col-md-2" onClick={(e) => e.stopPropagation()}>
                <label
                    htmlFor={item.id}
                    className={selected ? "checked" : ""}   
                >
                  <span>
                      {selected && (
                          <i className="fa-solid fa-check"></i>
                      )}
                  </span>
                </label>
                <input
                    id={item.id}
                    type="checkbox"
                    value={item.id}
                    checked={selected}
                    onChange={(e) => handleSelect(e, item)}
                />
            </div>
        }
        <div className="col-md-10">
          <div className="d-flex align-items-center justify-content-between">
            {children}
          </div>
        </div>
    </>
  );
};

export default ListRow;
