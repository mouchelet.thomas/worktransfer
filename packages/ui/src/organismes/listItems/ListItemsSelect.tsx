import { SchoolContext } from "@work-transfert/contexts";
import { useContext, useEffect, useState } from "react";
import ListRow from "./ListRow";
import SkeletonList from "./SkeletonList";
import toast from 'react-hot-toast';
import { ErrorResponse } from "@work-transfert/types";

interface ListItemsSelectProps {
    listEntityService: any
    updateEntityService: any
    entityProperty: string
    updateItemId: string
    entityLabel?: string
    allowActions?: boolean
}

export const ListItemsSelect = ({
    listEntityService,
    updateEntityService,
    entityProperty,
    updateItemId,
    entityLabel,
    allowActions = true
}: ListItemsSelectProps) => {
    const { school } = useContext(SchoolContext);
    const [entities, setEntities] = useState([] as any[])
    const [onLoading, setOnLoading] = useState(false);
    const [credentials, setCredentials] = useState({} as any)

    useEffect(() => {
        if(school) fetchEntities()
        fetchItem()
    }, [school, updateItemId])

    const fetchItem = async () => {
        if(!updateItemId) return;
        try {
            const response = await updateEntityService.findOne(updateItemId);
            setCredentials(response);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    const fetchEntities = async () => {
        const timer = setTimeout(() => {
            setOnLoading(true);
        }, 100);
            
        try {
            const response = await listEntityService.findAllBySchool(school.id);
            setEntities(response.data);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        } finally {
            clearTimeout(timer);
            setOnLoading(false);
        }
    }    

    const isItemInArrayById = (array: any[], item: any) => {
        // if(!array || array.length === 0 || !item.id) return false
        return array.some((entity: any) => entity?.id === item?.id);
    };

    const handleSelect = (event: any, item: any ) => {
        event.stopPropagation()
        event.preventDefault()
        let newSelected = !credentials[entityProperty] ? [] : [...credentials[entityProperty]]
        
        if (isItemInArrayById(newSelected, item)) {
            newSelected = newSelected.filter((entity: any) => entity.id !== item.id)
        } else {
            newSelected.push(item)
        }

        const newCredentials = {
            ...credentials,
            [entityProperty]: newSelected
        }

        updateEntityService.update(newCredentials)
        
        setCredentials({
            ...credentials,
            [entityProperty]: newSelected
        })
    }

    const isSelected = (item: any) => {
        if(!credentials[entityProperty]) return false
        return isItemInArrayById(credentials[entityProperty], item)
    }

    return ( 
        <div className="select--list">
            <h2>
                Selectionner parmi les {entityLabel}
            </h2>
            {onLoading && <SkeletonList />}

            {!onLoading && entities.map((item, index) => (
                <div 
                    className="list-row pb-2"
                    onClick={(e) => handleSelect(e, item)}
                    key={item.id}
                >
                    <ListRow
                        selected={isSelected(item)}
                        handleSelect={handleSelect}
                        item={item}
                        allowActions={allowActions}
                    >
                        {item.name}
                    </ListRow>
                </div>
            ))}
        </div>
     );
}
 
export default ListItemsSelect;