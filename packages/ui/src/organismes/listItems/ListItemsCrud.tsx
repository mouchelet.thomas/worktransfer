import { SchoolContext } from "@work-transfert/contexts";
import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "../../stories/Button";
import ListRow from "./ListRow";
import { SkeletonList } from "../../index";
import { NavbarActionListItems } from "./NavbarActionListItems";
import toast from 'react-hot-toast';
import { ErrorResponse } from "@work-transfert/types";

interface ListItemsProps {
    entityService: any;
    showLabels: any;
    entity: string;
    entityName?: string;
    allowActions?: boolean;
    entityPathLink?: string;
    entityLabel?: string;
}

export const ListItemsCrud = ({
    entityService,
    showLabels,
    entity,
    allowActions = false,
    entityPathLink = entity,
    entityLabel = entity,
}: ListItemsProps) => {
    const [items, setItems] = useState([])
    const { school } = useContext(SchoolContext);
    const navigate = useNavigate();
    const [selected, setSelected] = useState([] as any[])
    const [onLoading, setOnLoading] = useState(false);

    useEffect(() => {
        if(school) fetchAllItems();
    }, [school])

    const fetchAllItems = async () => {
        setOnLoading(true);
        try {
            const response = await entityService.findAllBySchool(school.id);
            setItems(response.data);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        } finally {
            setOnLoading(false);
        }
    }

    const isItemInArrayById = (array: any[], item: any) => {
        return array.find((entity: any) => entity.id === item.id);
    };

    const handleSelect = (event: any, item: any ) => {
        event.stopPropagation()

        if (isItemInArrayById(selected, item)) {
            const newSelected = selected.filter((entity: any) => entity.id !== item.id)
            setSelected([...newSelected]) 
        } else {
            setSelected([item, ...selected]) 
        }
    }

    return (
        <div className="select--list">
            <div className="row align-items-center mb-2 justify-content-between">
                <div className="col-md-12 d-flex justify-content-between align-items-center">
                    <h1>Liste des {entityLabel}s</h1>
                    {allowActions && 
                        <Button
                            primary={true}
                            size="small"
                            to={`/${entityPathLink}/create`}
                            label={`Ajouter ${entityLabel}`}
                            className="ml-4"
                        />
                    }
                </div>
            </div>

            <NavbarActionListItems
                selected={selected}
                setSelected={setSelected}
                fetchAllItems={fetchAllItems}
                entityService={entityService}
                entityLabel={entityLabel}
                items={items}
                allowActions={allowActions}
                entity={entity}
            />

            {onLoading && <SkeletonList />}

            {items.length === 0 && !onLoading && (
                <div className="mt-5 d-flex flex-column align-items-center text-center">
                    <h2 className="text-center mb-3">Aucun {entityLabel} pour le moment</h2>
                    {allowActions && 
                        <Button
                            primary={true}
                            size="large"
                            to={`/${entityPathLink}/create`}
                            label={`Ajouter ${entityLabel}`}
                            className="ml-4"
                        />
                    }
                </div>
            )}

            <div>
                {items.map((item: any) => (
                    <div 
                        key={item.id}
                        className="list-row"
                        onClick={() => navigate(`/${entityPathLink}/${item.id}`)}
                    >
                            <ListRow
                                selected={selected.includes(item)}
                                handleSelect={handleSelect}
                                item={item}
                                allowActions={allowActions}
                            >
                                {showLabels(item)}
                            </ListRow>
                    </div>
                ))}
            </div>
        </div>
    )
}