import { useContext, useEffect, useState } from "react"
import { SchoolContext } from "@work-transfert/contexts"
import ListRow from "./ListRow"
import { ErrorResponse } from "@work-transfert/types"
import toast from 'react-hot-toast';

interface ListItemsAccordionProps {
    credentials: any
    setCredentials: any
    entityService: any
    entityProperty: string
}

export const ListItemsAccordion = ({
    credentials, 
    setCredentials,
    entityService,
    entityProperty
}: ListItemsAccordionProps)  => {
    const { school } = useContext(SchoolContext);
    const [entities, setEntities] = useState([] as any[])
    const [onLoading, setOnLoading] = useState(false);

    useEffect(() => {
        if(school) fetchEntities()
    }, [school])

    const fetchEntities = async () => {
        setOnLoading(true);
        try {
            const response = await entityService.findAllBySchool(school.id);
            setEntities(response.data);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        } finally {
            setOnLoading(false);
        }
    }  

    const isItemInArrayById = (array: any[], item: any) => {
        // if(!array || array.length === 0 || !item.id) return false
        return array.some((entity: any) => entity?.id === item?.id);
    };

    const handleSelect = (event: any, item: any ) => {
        event.stopPropagation()
        event.preventDefault()
        let newSelected = !credentials[entityProperty] ? [] : [...credentials[entityProperty]]
        
        if (isItemInArrayById(newSelected, item)) {
            newSelected = newSelected.filter((entity: any) => entity.id !== item.id)
        } else {
            newSelected.push(item)
        }
        
        setCredentials({
            ...credentials,
            [entityProperty]: newSelected
        })
    }

    const isSelected = (item: any) => {
        if(!credentials[entityProperty]) return false
        return isItemInArrayById(credentials[entityProperty], item)
    }
    
    return ( 
        <div>
            {entities.map((item, index) => (
                <div>
                    <h3>{item.name}</h3>

                    {item.subjects.map((child: any) => (
                        <div 
                            className="list-row"
                            onClick={(e) => handleSelect(e, child)}
                            key={`${item.id}-${child.id}`}
                        >
                            <ListRow
                                selected={isSelected(child)}
                                handleSelect={handleSelect}
                                item={child}
                                allowActions={true}
                            >
                                {child.name}
                            </ListRow>
                        </div>
                    ))}
                </div>
            ))}
        </div>
     );
}
 
export default ListItemsAccordion;