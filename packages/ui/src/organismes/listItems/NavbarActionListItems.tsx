import { useState } from "react";
import { PrimaryModal } from "../../molecules/modal/PrimaryModal";
import { Button } from "../../stories/Button";
import { ImportDataModal } from "../../molecules/modal/ImportDataModal";
import { ErrorResponse } from "@work-transfert/types";
import toast from 'react-hot-toast';

interface NavbarActionListItemsProps {
    entityService: any;
    selected: any[];
    setSelected: (selected: any[]) => void;
    fetchAllItems: () => void;
    entityLabel: string;
    items: any[];
    allowActions?: boolean;
    entity: string;
}

export const NavbarActionListItems = ({
    selected,
    setSelected,
    fetchAllItems,
    entityService,
    entityLabel,
    items,
    allowActions = false,
    entity
}: NavbarActionListItemsProps) => {
    const [openModal, setOpenModal] = useState(false);

    const handleSelectAll = () => {
        if (selected.length === items.length) {
            setSelected([])
        } else {
            setSelected(items)
        }
    }

    const handleDelete = async () => {
        setOpenModal(false)
        try {
            await entityService.deleteAllByIds(selected.map((item: any) => item.id));
            setSelected([])
            fetchAllItems();
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    return ( 
        <div className="d-flex align-items-center mb-3">
            {items.length > 0 && allowActions && 
                <Button
                    color="secondary"
                    size="small"
                    label={selected.length === items.length ? "Désélectionner tout" : "Sélectionner tout"}
                    className=""
                    onClick={handleSelectAll}
                />
            }
            {allowActions && entity !== "project" && 
                <ImportDataModal
                    entityService={entityService}
                    fetchAllItems={fetchAllItems}
                    entity={entity}
                />
            }
            {selected.length > 0 && allowActions && (
                <Button
                    color="danger"
                    size="small"
                    className="ml-2"
                    label={`Supprimer ${selected.length>1 ? "les" : ""} ${selected.length > 1 ? selected.length : "" } ${entityLabel}${selected.length > 1 ? 's' : ''}`}
                    onClick={() => setOpenModal(true)}
                />
            )}
            {allowActions && 
                <PrimaryModal
                    selected={selected} 
                    handleDelete={handleDelete}
                    entityLabel={entityLabel}
                    setOpenModal={setOpenModal}
                    openModal={openModal}
                />
            }
        </div>
     );
}