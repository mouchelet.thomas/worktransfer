import { useContext, useEffect, useState } from "react";
import { ItemFormAdd } from "./ItemFormAdd";
import { SchoolContext } from "@work-transfert/contexts";
import SkeletonList from "./SkeletonList";
import ListRow from "./ListRow";
import { NavbarActionListItems } from "./NavbarActionListItems";
import { Pagination } from "../../molecules/pagination/Pagination";
import { ErrorResponse } from "@work-transfert/types";
import toast from 'react-hot-toast';

interface ListItemsCrudSelectProps {
    entityService: any;
    showLabels?: any;
    entity: string;
    entityName?: string;
    allowActions?: boolean;
    entityLabel?: string;
    setCredentials?: any;
    currentItem?: any;
    inputType?: string;
    inputName?: string;
}

type FetchAllItemsFunction = (queries?: any) => Promise<void>;

export const ListItemsCrudSelect = ({
    entityService,
    showLabels,
    entity,
    allowActions = false,
    entityLabel = entity,
    setCredentials,
    currentItem,
    inputType = "text",
    inputName = "name"
    } : ListItemsCrudSelectProps) => {
        const [items, setItems] = useState([])
        const { school } = useContext(SchoolContext);
        const [selected, setSelected] = useState([] as any[])
        const [onLoading, setOnLoading] = useState(false);
        const [totalCount, setTotalCount] = useState(0);
        const [currentPage, setCurrentPage] = useState(1);
        const [totalPages, setTotalPages] = useState(0);

        useEffect(() => {
            if(school) fetchAllItems();
        }, [school])

        const fetchAllItems: FetchAllItemsFunction = async (queries = {}) => {
            const timer = setTimeout(() => {
                setOnLoading(true);
            }, 100);

            try {
                const response = await entityService.findAllBySchool(school.id, queries);
                setCredentials && setCredentials(response.data[0])
                setTotalCount(response.totalCount);
                setTotalPages(response.totalPages);
                setCurrentPage(response.currentPage);
                setItems(response.data);
                clearTimeout(timer);
            } catch (error) {
                toast.error((error as ErrorResponse).response.data.message)
            } finally {
                setOnLoading(false);
            }
        }

        const isItemInArrayById = (array: any[], item: any) => {
            return array.find((entity: any) => entity.id === item.id);
        };
    
        const handleSelect = (event: any, item: any ) => {
            event.stopPropagation()
    
            if (isItemInArrayById(selected, item)) {
                const newSelected = selected.filter((entity: any) => entity.id !== item.id)
                setSelected([...newSelected]) 
            } else {
                setSelected([item, ...selected]) 
            }
        }

        return ( 
            <div className="select--list">
                {allowActions &&
                    <div className="mb-3">
                        <ItemFormAdd 
                            entityService={entityService}
                            fetchAllItems={fetchAllItems}
                            entityLabel={entityLabel}
                            showButton={true}
                            inputType={inputType}
                            inputName={inputName}
                        />
                    </div>
                }

                <NavbarActionListItems
                    selected={selected}
                    setSelected={setSelected}
                    fetchAllItems={fetchAllItems}
                    entityService={entityService}
                    entityLabel={entityLabel}
                    items={items}
                    allowActions={allowActions}
                    entity={entity}
                />

                {onLoading && <SkeletonList />}

                {items.length === 0 && !onLoading && (
                    <div className="mt-7 text-center typography-xl">
                        Aucune donnée disponible
                    </div>  
                )}

                <div>
                    {items.map((item: any) => (
                        <div 
                            key={`list-row-${item.id}`}
                            className={`list-row row ${currentItem && currentItem.id === item.id ? 'active' : ''}`}
                            onClick={() => setCredentials && setCredentials(item)}
                        >
                            <ListRow
                                selected={selected.includes(item)}
                                handleSelect={handleSelect}
                                item={item}
                                allowActions={allowActions}
                            >
                                <ItemFormAdd 
                                    entityService={entityService}
                                    fetchAllItems={fetchAllItems}
                                    item={item}
                                    inputName={inputName}
                                    inputType={inputType}
                                />
                                {showLabels && showLabels(item)}
                            </ListRow>
                        </div>
                    ))}
                </div>

                <Pagination 
                    totalPages={totalPages}
                    currentPage={currentPage}
                    fetchAllItems={fetchAllItems}
                    totalCount={totalCount}
                />
            </div>
        );
}