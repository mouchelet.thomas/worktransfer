import { SchoolContext } from "@work-transfert/contexts";
import { ErrorResponse } from "@work-transfert/types";
import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import toast from 'react-hot-toast';

interface ListItemsProps {
    /**
     * Entity service
     */
    entityService?: any;
    /**
     * Name of property in credentials
     */
    property: string;
    /**
     *  Credentials
     */
    credentials: any;
    /**
     * Auth setter credentials
     */
    setCredentials: React.Dispatch<React.SetStateAction<any>>;
    /**
     * Options array
     */
    options?: any[];
}

export const SelectItem = ({
    entityService, 
    credentials, 
    setCredentials,
    property,
    options
}: ListItemsProps ) => {
    const { school } = useContext(SchoolContext);
    const [listItems, setListItems] = useState([] as any);
    const { id } = useParams();

    useEffect(() => {
        if(school && !options) fetchAllItems();
        if(options) setListItems(options)
    }, [school, options]);
    
    useEffect(() => {
        if(!id) setDefaultCredentials()
    }, [listItems, school])

    const fetchAllItems = async () => {
        try {
            const response = await entityService.findAllBySchool(school.id);
            setListItems(response.data);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    const handleChange = (event: any) => {
        let { name, value } = event.currentTarget;
        
        setCredentials({
            ...credentials,
            [name]: value
        });
    }

    const setDefaultCredentials = () => {
        let value

        if(property == "classrooms") {
            const arrayClassrooms = credentials[property as keyof typeof credentials] as any[];
            if (arrayClassrooms instanceof Array && arrayClassrooms.length > 0) return
            
            value = [{id: listItems[0]?.id}]
        } else {
            value = listItems[0]?.id
        }

        setCredentials({
            ...credentials,
            [property]: value
        })
    }

    const showDefaultCredentials = () => {
        if(!credentials[property as keyof typeof credentials]) return;
        return credentials[property as keyof typeof credentials];
    }

    return ( 
        <div className="select-wrapper">
            <select 
                onChange={handleChange} 
                name={property}
                value={showDefaultCredentials() || ""}
            >
                {listItems.map((item: any) => (
                    <option key={item.id} value={item.id}>{item.name}</option>
                ))}
            </select>
        </div>
    );
}
 
export default SelectItem;