import { DelivrableType, ErrorResponse } from "@work-transfert/types";
import { useState } from "react";
import { Button } from "../../../stories/Button";
import Moment from 'moment';
import { DelivrableService } from "@work-transfert/services";
import { useParams } from "react-router-dom";
import { useDelivrableUploadFile } from "@work-transfert/hooks";
import toast from 'react-hot-toast';

interface DelivrableListProps {
    /**
     * List of delivrables
     */
    delivrables: DelivrableType[];
    /**
     * Total number of delivrables
     */
    total: number;
    /**
     * Fetch projects
     */
    fetchProject: () => void;
}

export const DelivrableList = ({
    delivrables,
    total,
    fetchProject
}: DelivrableListProps) => {
    const [selected, setSelected] = useState([] as any[])
    const { id } = useParams();
    const { handleDownloadAll, handleDownload, showFileSize } = useDelivrableUploadFile()

    const handleSelect = (event: any, item: any ) => {
        event.stopPropagation()
        if (selected.includes(item)) {
            setSelected(selected.filter((i: any) => i !== item))
            return
        }
        setSelected([...selected, item])
    }

    const formatDate = (date: Date) => {
        return Moment(date).format('DD/MM/YYYY à HH:mm');
    }

    const handleDelete = async (e: React.MouseEvent) => {
        try {
            const ids = selected.map((item: any) => item.id)
            await DelivrableService.deleteDelivrables(ids)
            setSelected([])
            fetchProject()
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    return ( 
        <div className="select--list">
                <div className="mb-2">
                    <div className="d-flex align-items-center">
                        <button 
                            className="button button--small button--secondary mr-3"
                            onClick={() => fetchProject()}
                        >
                            Rafraichir
                            <i className="fa-solid fa-rotate-right ml-2"></i>
                        </button>
                        {id && 
                            <Button
                                primary={true}
                                color="primary"
                                size="small"
                                label={`Tout télécharger en zip ${total > 0 ? `(${total})` : ''}`}
                                onClick={(e) => handleDownloadAll(e , id)}
                                icon={<i className="fa-solid fa-circle-down"></i>}
                            />
                        }
                        {selected.length > 0 && (
                            <Button
                                color="danger"
                                size="small"
                                label={`Supprimer selection`}
                                className="ml-2"
                                onClick={handleDelete}
                            />
                        )}
                    </div>
                </div>
            {delivrables.map(delivrable => (
                <div 
                    key={delivrable.id}
                    className="list-row"
                >
                    <div className="col-md-1">
                        <label 
                            htmlFor={delivrable.id}
                            className={selected.includes(delivrable) ? "checked" : ""}
                            onClick={(event) => event.stopPropagation()}
                            >
                                <span>
                                    {selected.includes(delivrable) &&
                                        <i className="fa-solid fa-check"></i>
                                    }
                                </span>
                        </label>
                        <input 
                            id={delivrable.id}
                            type="checkbox"
                            name={delivrable.id}
                            value={delivrable.id}
                            checked={selected.includes(delivrable)}
                            onClick={(event) => event.stopPropagation()}
                            onChange={(event) => handleSelect(event, delivrable)}
                        />
                    </div>
                    <p className="col-md-2">
                        {delivrable.delivrableUser.firstName} 
                    </p>
                    <p className="col-md-2">
                        {delivrable.delivrableUser.lastName}
                    </p>
                    <p className="col-md-2">
                        {formatDate(delivrable.createdAt)}
                    </p>
                    <p className="col-md-2">
                        {delivrable.delivrableUploadFile &&
                            showFileSize(delivrable?.delivrableUploadFile?.size)
                        } 
                    </p>
                    <p className="col-md-2">
                        <Button
                            color="primary"
                            size="medium"
                            label={`Télécharger`}
                            onClick={(e) => handleDownload(e, delivrable?.delivrableUploadFile?.id ?? "")}
                            icon={<i className="fa-solid fa-circle-down"></i>}
                        />
                    </p>
                </div>
            ))}
        </div>
     );
}
 
export default DelivrableList;