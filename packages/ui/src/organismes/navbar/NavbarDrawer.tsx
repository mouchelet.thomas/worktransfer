import "@work-transfert/styles/src/components/_navbar.scss"
import { AuthService } from "@work-transfert/services";
import { Link, useNavigate } from "react-router-dom";
import { useContext } from "react";
import { UserContext } from "@work-transfert/contexts";
import SelectSchool from "../../molecules/select-school/SeclectSchool";

const NAVBAR_ITEMS = [
    {
        label: 'Dashboard',
        path: '/',
        icon: 'dashboard',
        allow: ['admin', 'school', 'teacher']
    },
    {
        label: 'Matières',
        path: '/subjects',
        icon: 'book',
        allow: ['admin', 'school']
    },
    {
        label: 'Cursus',
        path: '/curriculums',
        icon: 'building-columns',
        allow: ['admin', 'school']
    },
    {
        label: 'Promotions',
        path: '/student-classes',
        icon: 'graduation-cap',
        allow: ['admin', 'school', 'teacher']
    },
    {
        label: 'Intervenants',
        path: '/teachers',
        icon: 'chalkboard-user',
        allow: ['admin', 'school']
    },
    {
        label: 'Projets',
        path: '/projects',
        icon: 'laptop-file',
        allow: ['admin', 'school', 'teacher']
    },
]

export const NavbarDrawer = () => {
    const navigate = useNavigate()
    const { setUser, user } = useContext(UserContext)

    const handleLogout = (e: React.MouseEvent) => {
        e.preventDefault()
        handleSingout()
    }

    const handleSingout = () => {
        AuthService.signout()
        setUser(null)
        navigate('/')
    }

    const checkIsActive = (path: string) => {
        const currentPath = window.location.pathname
        const splitCurrentPath = currentPath.split('/')[1]
        const splitPath = path.split('/')[1]

        return splitCurrentPath === splitPath
    }

    return ( 
        <div className="navbar-drawer pt-4">
            <div className="navbar-drawer--header">
                <SelectSchool />
            </div>

            <div className="navbar-drawer--body">
                <ul>
                    {NAVBAR_ITEMS.map((item, index) => { 
                        if (!item.allow.includes(user?.role)) return null
                        return (
                            <li 
                                className={`navbar-drawer--item ${checkIsActive(item.path) ? 'active' : ''}`}
                                onClick={() => navigate(item.path)}
                                key={index} 
                            >
                                <i className={`fa-solid fa-${item.icon}`}></i>
                                <p>{item.label}</p>
                            </li>
                        )
                    })}
                </ul>
            </div>

            <div className="navbar-drawer--footer">
                <ul>
                    <li
                        className={`navbar-drawer--item ${checkIsActive("/account") ? 'active' : ''}`}
                        onClick={() => navigate("/account")}
                    >
                        <i className={`fa-solid fa-gear`}></i>
                        <p>Mon compte</p>
                    </li>
                    <li
                        className={`navbar-drawer--item`}
                        onClick={handleLogout}
                    >
                        <i className={`fa-solid fa-right-from-bracket`}></i>
                        <p>Se déconnecter</p>
                    </li>
                </ul>
            </div>
        </div>
     );
}