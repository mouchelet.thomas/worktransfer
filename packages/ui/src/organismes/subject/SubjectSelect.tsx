import { SchoolContext } from "@work-transfert/contexts";
import { SubjectService } from "@work-transfert/services";
import { ErrorResponse } from "@work-transfert/types";
import { useContext, useEffect, useState } from "react";
import toast from 'react-hot-toast';

export const SubjectSelect = () => {
    const { school } = useContext(SchoolContext);
    const [listItems, setListItems] = useState([] as any);

    useEffect(() => {
        if(school) fetchAllItems();
    }, [school]);

    const fetchAllItems = async () => {
        try {
            const response = await SubjectService.findAllBySchool(school.id);
            setListItems(response.data);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    return ( 
        <div className="select-wrapper">
            <select>
                {listItems.map((item: any) => (
                    <option key={item.id} value={item.id}>{item.name}</option>
                ))}
            </select>
        </div>
     );
}
 
export default SubjectSelect;