import React, { useEffect, useState } from 'react';

interface ShareLinkProjectButtonProps {
    /**
     * Project id
     */
    id: string;
    /**
     * Student app url
     */
    url: string
    /**
     * Size of the button
     */
    size?: 'small' | 'medium' | 'large';
    /**
     * className of the button
     */
    className?: string;
}

export const ShareLinkProjectButton = ({ 
    id, 
    url,
    size = 'medium',
    className = ''
 }: ShareLinkProjectButtonProps) => {
    const [copied, setCopied] = useState(false);
    const [projectLink] = useState(`${url}/projects/${id}`);

    const handleCopy = (e: React.MouseEvent) => {
        e.stopPropagation();
        
        navigator.clipboard.writeText(projectLink)
        setCopied(true);
        setTimeout(() => {
            setCopied(false);
        }, 3000)
    }

    return ( 
        <React.Fragment>
            <button
                className={`button button--secondary button--${size} ${className}`}
                onClick={handleCopy}
            >
                <span>
                    {copied ? 'Lien copié' : `${projectLink.substring(0,10)}...${projectLink.substring(projectLink.length-5)}`}
                </span>
                {copied ? <i className="fa-solid fa-check"></i> : <i className="fa-solid fa-copy"></i>}
            </button>
        </React.Fragment>
     );
}