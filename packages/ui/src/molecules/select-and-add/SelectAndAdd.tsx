import { SchoolContext } from "@work-transfert/contexts"
import { ErrorResponse } from "@work-transfert/types"
import { useContext, useEffect, useState } from "react"
import toast from 'react-hot-toast';

interface SelectAndAddProps {
    credentials: any
    setCredentials: any
    entityService: any
    entityProperty: string
}

export const SelectAndAdd = ({
    credentials, 
    setCredentials,
    entityService,
    entityProperty
}: SelectAndAddProps) => {
    const { school } = useContext(SchoolContext);
    const [entities, setEntities] = useState([] as any[])
    const [selectedEntity, setSelectedEntity] = useState([] as any[])

    useEffect(() => {
        if(school) fetchEntities()
    }, [school])

    const fetchEntities = async () => {
        try {
            const response = await entityService.findAllBySchool(school.id);
            setEntities(response.data);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
        }
    }

    const handleAddAutocomplete = async () => {
        if (entities.length > 0) {
            // Trouvez la première entité qui n'est pas déjà dans selectedEntity
            const firstNotSelectedEntity = entities.find(
                entity => !selectedEntity.some(e => e.id === entity.id)
            );
    
            if (firstNotSelectedEntity) {
                setSelectedEntity([...selectedEntity, firstNotSelectedEntity]);
            } else {
                // Aucune entité n'est disponible pour être ajoutée, vous pouvez gérer ce cas comme vous le souhaitez
                console.log("Toutes les entités ont déjà été sélectionnées.");
            }
        }
    }

    const handleChange = (event: any, index: number) => {
        const newSelectedEntity = selectedEntity.map((entity, i) => {
            if (i === index) {
                return entities.find(e => e.id === event.target.value) || entity;
            }
            return entity;
        });

        setSelectedEntity(newSelectedEntity);
    }

    const getFilteredEntities = (currentIndex: number) => {
        if (selectedEntity.length === 1) {
            return entities;
        }
        return entities.filter(
            (entity, index) =>
                !selectedEntity.some(
                    (e, selectedIndex) => e.id === entity.id && selectedIndex !== currentIndex
                )
        );
    }

    function generateUniqueId() {
        return 'id-' + Date.now().toString(36) + '-' + Math.random().toString(36).substr(2, 9);
    }

    return ( 
        <div>
            <button 
                className="button button--large button--secondary"
                onClick={handleAddAutocomplete}
                type="button"
            >
                Ajouter {entityProperty}
            </button>

            {selectedEntity.map((entity, index) => (
                <div className="select-wrapper" key={index}>
                    <select 
                        name={entityProperty}
                        value={entity.id}
                        onChange={(e) => handleChange(e, index)}
                    >
                        {getFilteredEntities(index).map(entityOption => (    
                            <option value={entityOption.id} key={entityOption.id}>
                                {entityOption.name}
                            </option>
                        ))}
                    </select>
                </div>
            ))}
        </div>
     );
}
 
export default SelectAndAdd;