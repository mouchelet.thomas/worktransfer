import { SchoolContext } from "@work-transfert/contexts";
import { SchoolType } from "@work-transfert/types";
import { useContext } from "react";
import { DropZonePrimary } from "../drop-zone/DropZonePrimary";

interface ImportDataProps {
    setImportDatas: (importDatas: any[]) => void;
    importDatas: any[];
    setHeader: (header: any[]) => void;
}

export const ImportData = ({
    setImportDatas,
    importDatas,
    setHeader
}: ImportDataProps) => {
    const { school } = useContext(SchoolContext);

    function readCSVFile(file: File): Promise<Array<{ [key: string]: string }>> {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = (event: any) => {
          const data = event.target.result;
          const dataArray: Array<Array<string>> = data
            .split('\n')
            .map((row: string) => row.split(';').map(cell => cell.trim()));
  
          const header = dataArray.shift();
          if(header) setHeader(header);
          const formattedData = dataArray
            .map((row: Array<string>) => {
              if (header && row.length === header.length) {
                const obj: { [key: string]: string } = {};
                header.forEach((columnName: string, index: number) => {
                  obj[columnName] = row[index]
                });
                obj.school = school
                return obj;
              }
              return undefined;
            })
            .filter((obj): obj is { [key: string]: string } => obj !== undefined);
    
          resolve(formattedData);
        };
        reader.onerror = (error) => reject(error);
        reader.readAsText(file);
      });
    }

    const handleUpload = (acceptedFiles: any) => {
      const file = acceptedFiles[0];
      readCSVFile(file)
          .then((dataArray) => {
              setImportDatas(dataArray);
          })
          .catch((error) => {
              console.error('Error reading file:', error);
          });
    }

    return ( 
        <div>
            <DropZonePrimary
              handleUpload={handleUpload}
              acceptedFileTypes=".csv"
              size="small"
            />
        </div>
     );
}
