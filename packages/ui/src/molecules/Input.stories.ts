import type { Meta, StoryObj } from '@storybook/react';

import { Input } from './Input';

// More on how to set up stories at: https://storybook.js.org/docs/7.0/react/writing-stories/introduction
const meta = {
  title: 'molecules/Input',
  component: Input,
  tags: ['autodocs'],
  argTypes: {
    placeholder: { placeholder: 'string' },
    type: { type: 'string'},
    name: { name: 'string' },
  },
} satisfies Meta<typeof Input>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/7.0/react/writing-stories/args
export const Text: Story = {
  args: {
    placeholder: "Text input",
    type: "text",
    name: "email",
  },
};