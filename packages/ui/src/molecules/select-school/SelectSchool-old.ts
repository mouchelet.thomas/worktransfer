// import { SchoolType } from "@work-transfert/types";
// import { useContext, useEffect, useState } from "react";
// import { SchoolService } from "@work-transfert/services";
// import { SchoolContext, UserContext } from "@work-transfert/contexts";

// export const SelectSchool = () => {
//     const { user } = useContext(UserContext);
//     const { school, schools, setSchool } = useContext(SchoolContext);

//     const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
//         const { value } = e.target;
//         const school = schools.find((school) => school.id === value);
//         setSchool(school);
//     }

//     return ( 
//         <div className="select-wrapper">
//             <select 
//                 name="school"
//                 onChange={handleChange}
//                 value={school?.id}
//             >
//                 {user?.role === 'teacher' && user.teacher.teacherSchools.length === 0 && (
//                     <option value="">Aucune école</option>
//                 )}
//                 {schools.map((school) => (
//                     <option key={school.id} value={school.id}>{school.name}</option>
//                 ))}
//                 <option>
//                     <button className="btn btn-primary">
//                         Ajouter une école
//                     </button>
//                 </option>
//             </select>
//         </div>
//      );
// }
 
// export default SelectSchool;