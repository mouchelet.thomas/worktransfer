import { SchoolType } from "@work-transfert/types";
import { MutableRefObject, useContext, useEffect, useRef, useState } from "react";
import { SchoolService } from "@work-transfert/services";
import { SchoolContext, UserContext } from "@work-transfert/contexts";
import { Button } from "../../stories/Button";
import { useNavigate } from "react-router-dom";

export const SelectSchool = () => {
    const { user } = useContext(UserContext);
    const { school, schools, setSchool } = useContext(SchoolContext);
    const [isOpened, setIsOpened] = useState(false);
    const wrapperRef = useRef<HTMLDivElement>(null);
    const navigate = useNavigate();

    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
            if (wrapperRef.current && !wrapperRef.current.contains(event.target as Node)) {
                setIsOpened(false);
            }
        };

        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [wrapperRef]);

    const handleChangeSchool = (school: SchoolType) => {
        setSchool(school);
        setIsOpened(false);
        navigate(`/`);
    }

    const filteredSchools = schools.filter((s) => s.id !== school.id);

    return ( 
        <div 
            className="select-wrapper school mb-2" 
            onClick={()=> setIsOpened(!isOpened)}
            ref={wrapperRef}
        >
            {user?.role === 'teacher' && schools.length === 0 && (
                <div className="select-item">Aucune école</div>
            )}
            <div className="select-item">{school?.name}</div>

            <div className={`select-list-items ${isOpened ? "open" : "close"}`}>
                {filteredSchools.map((school) => (
                    <div 
                        key={school.id} 
                        className="select-item"
                        onClick={() => handleChangeSchool(school)}
                    >
                        {school.name}
                    </div>
                ))}
                
                <div className="select-list-items--footer">
                    <Button
                        type="button"
                        label="Ajouter une école"
                        primary={true}
                        className="w-100"
                        size="small"
                        to={user?.role === 'teacher' ? "schools/accept" : "schools/create"}
                        icon={<i className="fa-sharp fa-solid fa-plus"></i>}
                    />
                </div>
            </div>
        </div>
     );
}
 
export default SelectSchool;