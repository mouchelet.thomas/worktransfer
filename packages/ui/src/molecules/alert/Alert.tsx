import { useState } from "react";

interface AlertProps {
    message: string;
    variant?: 'success' | 'warning' | 'error';
    className?: string;
}

export const Alert = ({
    message,
    variant = 'success',
    className = ''
}: AlertProps) => {
    const [open, setOpen] = useState(true);

    return ( 
        <div className={`alert alert--${variant} ${className} ${open ? "open" : "close"}`}>
            <span className="message">
                {message}
            </span>

            <i className="fa-solid fa-xmark" onClick={()=> setOpen(!open)}></i>
        </div>
     );
}