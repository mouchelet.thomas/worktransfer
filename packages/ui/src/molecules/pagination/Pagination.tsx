interface PaginationProps {
    totalPages: number;
    currentPage: number;
    fetchAllItems: (queries?: any) => void;
    totalCount?: number;
}

export const Pagination = ({
    totalPages,
    currentPage,
    fetchAllItems,
    totalCount
}: PaginationProps) => {
    const renderPaginationItems = () => {
        const paginationItems = [];
        const maxPagesToShow = 6;

        for (let i = 1; i <= totalPages; i++) {
            if (i === 1 || i === totalPages || (i >= currentPage - 2 && i <= currentPage + 2)) {
                paginationItems.push(
                    <li
                        key={i}
                        className={`page-item ${currentPage === i ? "current" : ""}`}
                        onClick={() => fetchAllItems({ page: i })}
                    >
                        <span className="page-link">
                            {i}
                        </span>
                    </li>
                );
            } else if (i === currentPage - 3 || i === currentPage + 3) {
                paginationItems.push(
                    <li key={i} className="page-item disabled">
                        <span className="page-link">...</span>
                    </li>
                );
            }
        }

        return paginationItems;
    };


    return ( 
        <div className="mt-5">
            {totalPages > 1 && (
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        <li className="page-item" onClick={() => fetchAllItems({ page: currentPage-1})}>
                            <i className="fa-solid fa-chevron-left"></i>
                        </li>
                        {renderPaginationItems()}
                        <li className="page-item" onClick={() => fetchAllItems({ page: currentPage+1})}>
                            <i className="fa-solid fa-chevron-right"></i>
                        </li>
                    </ul>
                    <p className="typography-caption">
                        {totalCount} éléments
                    </p>
                </nav>
            )}
        </div>
     );
}