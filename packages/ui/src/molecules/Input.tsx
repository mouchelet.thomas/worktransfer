import "@work-transfert/styles/src/components/_input.scss"
import { useState } from "react";

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> { 
  /**
   * Input type
   */
  type: string;
  /**
   * Input name
   */
  name: string;
  /**
   * Input placeholder contents
   */
  placeholder: string;
  /**
   * Value
   */
  value?: any;
  /**
   * Auth credentials
   */
  credentials?: any;
  /**
   * Auth setter credentials
   */
  setCredentials?: React.Dispatch<React.SetStateAction<any>>;
  /**
   * Optional change handler
   */
  onChange?: () => void;
  /**
   * Optional update mode
   */
  haveItem?: boolean;
  /**
   * Optional submit handler
   */
  handleSubmit?: (e: React.MouseEvent<HTMLElement>) => Promise<void> ;

}

/**
 * Primary UI component for user interaction
 */
export const Input = ({
  placeholder = "Placeholder",
  type = "text",
  credentials,
  setCredentials,
  name,
  haveItem = false,
  handleSubmit,
  ...props
}: InputProps ) => {
  const [editMode, setEditMode] = useState(false);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCredentials && setCredentials({ ...credentials, [e.target.name]: e.target.value });
  };

  const handleFocus = (event: any) => {
      haveItem && setEditMode(true);
  }

  const handleBlur = (event: any) => {
    setTimeout(() => {
      setEditMode(false);
    }, 100);
  }

  return (
    <div
        className={`input-container ${haveItem ? 'input-update' : 'input-create'} ${editMode ? 'focus' : ''}`}
    >
        <input
            placeholder={placeholder}
            type={type}
            name={name}
            onChange={handleChange}
            {...props}
            onFocus={handleFocus}
            onBlur={handleBlur}
            value={credentials[name] || ''}
        />
        {editMode && 
            <i className="fa-solid fa-check" onClick={handleSubmit}></i>
          }
    </div>
  );
};
