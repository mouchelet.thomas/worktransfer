import { Button } from "../../stories/Button";

interface PrimaryModalProps {
    selected: any[];
    handleDelete: () => void;
    entityLabel: string;
    setOpenModal: (openModal: boolean) => void;
    openModal: boolean;
}

export const PrimaryModal = ({
    selected,
    handleDelete,
    entityLabel,
    setOpenModal,
    openModal
}: PrimaryModalProps) => {

    return ( 
        <div className={`modal ${openModal ? "open" : ""}`}>
            <div className="modal--body">
                <h2 className="text-center">
                    {`Êtes-vous sûr de vouloir supprimer les élements suivants ?`}
                </h2>
                <div className="text-center">
                    {selected.map((item: any, index) => (
                        <p key={`modal-${index}`}>{item.name}</p>
                    ))}
                </div>
            </div>
            <div className="modal--footer mt-4">
                <div className="row justify-content-between">
                    <Button
                        color="secondary"
                        size="large"
                        label={`Annuler`}
                        onClick={() => setOpenModal(false)}
                        className="mb-2 w-100 text-center"
                    />
                    <Button
                        color="danger"
                        size="large"
                        label={`Supprimer ${selected.length>1 ? "les" : ""} ${selected.length > 1 ? selected.length : "" } ${entityLabel}${selected.length > 1 ? 's' : ''}`}
                        onClick={handleDelete}
                        className="w-100 text-center"
                    />
                </div>
            </div>
        </div>
     );
}