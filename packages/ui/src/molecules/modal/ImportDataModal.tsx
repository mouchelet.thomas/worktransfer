import { useState } from "react";
import { Button } from "../../stories/Button";
import { ImportData } from "../input-file/ImportData";
import toast from 'react-hot-toast';
import { ErrorResponse } from "@work-transfert/types";

interface PrimaryModalProps {
    entityService: any;
    fetchAllItems: () => void;
    entity: string;
}

export const ImportDataModal = ({    
    entityService,
    fetchAllItems,
    entity
}: PrimaryModalProps) => {
    const [openModal, setOpenModal] = useState(false);
    const [importDatas, setImportDatas] = useState<any[]>([]);
    const [header, setHeader] = useState<any[]>([]);
    const [loading, setLoading] = useState(false);

    const handleConfirmImport = async () => {
        setLoading(true);
        try {
            await entityService.create(importDatas);
            setImportDatas([]);
            fetchAllItems()
            setOpenModal(false)
            setLoading(false);
        } catch (error) {
            toast.error((error as ErrorResponse).response.data.message)
            setLoading(false);
        }
    }

    const showObject = (obj: any) => {
        const propertyItems = [];
        for (const key of Object.keys(obj)) {
            if(key !== "school"){
                propertyItems.push(
                  <div key={key} className="ml-2 item">
                    {obj[key]}
                  </div>
                );
            }
        }
        return propertyItems;
    }

    const handleCloseModal = () => {
        setOpenModal(false);
        setImportDatas([]);
    }

    const handleDeleteItem = (e: React.MouseEvent<HTMLElement>, importDataRow: {}, index: number) => {
        importDatas.splice(index, 1);
        setImportDatas([...importDatas]);
    }

    return ( 
        <>
            <Button
                type="button"
                label="importer en csv"
                className="ml-2"
                size="small"
                onClick={() => setOpenModal(true)}
            />
            <div className={`modal ${openModal ? "open" : ""} large`}>
                <div className="modal--body">
                    <h2 className="text-center">
                        Importer des données
                    </h2>
                    <div>
                        <p className="typography-caption typography-caption--small mt-4 mb-4">
                            Pour importer des données, vous devez télécharger le modèle ci-dessous et le remplir. Vous pouvez ensuite importer le fichier dans l'application.
                        </p>
                        <div className="text-center mb-4">
                            <a href={`/downloads/${entity}.csv`} download className="download--link">
                                Téllécharger le model
                            </a>
                        </div>
                        
                    </div>
                    <div>
                        <ImportData 
                            setImportDatas={setImportDatas}
                            importDatas={importDatas}
                            setHeader={setHeader}
                        />
                    </div>
                    {importDatas.length > 0 && (
                        <div>
                            <p className="typography-caption typography-caption--small mt-4 mb-4">
                                Ci-dessous la liste des données à importer. Vous pouvez supprimer une donnée en cliquant sur la croix. L'importation sera effectuée une fois que vous aurez cliqué sur le bouton "Importer". 
                            </p>
                            <div className="list-data-import">
                                <div className="header">
                                    {header.map((headerRow, index) => (
                                        <div key={`header-${index}`} className="header-item">
                                            {headerRow}
                                        </div>
                                    ))}
                                </div>
                                {importDatas.map((importDataRow, index) => (
                                    <div 
                                        key={`importData-${index}`} 
                                        className="row"
                                        onClick={(e:React.MouseEvent<HTMLElement>) => handleDeleteItem(e,importDataRow, index)}
                                    >
                                        {showObject(importDataRow)}
                                        
                                        <i 
                                            className="fa-solid fa-xmark"
                                        ></i>
                                    </div>
                                ))}
                            </div>
                        </div>
                    )}
                </div>
                <div className="modal--footer mt-4">
                    <div className="row justify-content-between">
                        <Button
                            primary={true}
                            size="large"
                            label={`Importer`}
                            className="w-100 text-center mb-2"
                            onClick={handleConfirmImport}
                            onLoadingSubmit={loading}
                        />
                        <Button
                            color="secondary"
                            size="large"
                            label={`Annuler`}
                            onClick={handleCloseModal}
                            className="w-100 text-center"
                        />
                    </div>
                </div>
            </div>
        </>
    );
}