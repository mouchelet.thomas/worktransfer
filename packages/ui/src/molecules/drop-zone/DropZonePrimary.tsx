import React, { useEffect } from 'react';

// const ACCEPTED_FILE_TYPES =
//   '.zip,.rar,.7zip,.7z,.tar,.tar.gz,.gz';
// const ACCEPTED_FILE_TYPES =
//   '.csv';
const MAX_FILE_SIZE = 500 * 1024 * 1024; // 500 Mo

interface UploadFileStudentProps {
    handleUpload: any
    acceptedFileTypes: string
    size?: 'small' | 'medium' | 'large'
}


export const DropZonePrimary = ({
    handleUpload,
    acceptedFileTypes,
    size = 'medium',
}: UploadFileStudentProps) => {
    const dropzoneRef = React.useRef<HTMLDivElement>(null);
    const fileInputRef = React.useRef<HTMLInputElement>(null);
    const [error, setError] = React.useState<string | null>(null);

    const onDropzoneClick = () => {
        fileInputRef.current?.click();
    };

    const onDragOver = (e: React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
        if (dropzoneRef.current) {
        dropzoneRef.current.classList.add("dragover");
        }
    };

    const onDragLeave = () => {
        if (dropzoneRef.current) {
            dropzoneRef.current.classList.remove("dragover");
        }
    };

    const onDrop = (e: React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
        if (dropzoneRef.current) {
            dropzoneRef.current.classList.remove("dragover");
        }
        const files = e.dataTransfer.files;
        handleFiles(files);
    };

    const resetInput = () => {
        if (fileInputRef.current) {
          fileInputRef.current.value = '';
        }
    };

    const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const files = e.target.files;
        if (files) {
            handleFiles(files);

        }
    };

    const handleFiles = (files: FileList) => {
        const acceptedFiles: File[] = [];

        for (let i = 0; i < files.length; i++) {
            const file = files.item(i);
            if (file && isAcceptedFileType(file) && isFileSizeValid(file)) {
                acceptedFiles.push(file);
            }
        }

        if (acceptedFiles.length > 0) {
            // Traitez ici les fichiers téléchargés
            handleUpload(acceptedFiles);
        } else {
            if (dropzoneRef.current) dropzoneRef.current.classList.add("error");
            setError('Fichier non accepté');
        }
        resetInput();
    };

    const isAcceptedFileType = (file: File) => {
        const fileExtension = file.name.split('.').pop()?.toLowerCase();
        const acceptedExtensions = acceptedFileTypes.split(',');
        return (
            fileExtension &&
            acceptedExtensions.includes(`.${fileExtension}`)
        );
    };

    const isFileSizeValid = (file: File) => {
        return file.size <= MAX_FILE_SIZE;
    };
    
    return ( 
        <div
            id="dropzone"
            ref={dropzoneRef}
            className={`dropzone ${size}`}
            onClick={onDropzoneClick}
            onDragOver={onDragOver}
            onDragLeave={onDragLeave}
            onDrop={onDrop}
            >
            <p>Cliquez ou glissez déposez votre fichier ici</p>
            {error && <p className="error--message">{error}</p>}
            <input
                type="file"
                id="fileInput"
                ref={fileInputRef}
                multiple
                accept={acceptedFileTypes}
                style={{ display: 'none' }}
                onChange={onInputChange}
            />
        </div>
     );
}