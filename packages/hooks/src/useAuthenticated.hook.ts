import { UserContext } from '@work-transfert/contexts'
import { useContext } from 'react';

export function useAuthenticated() {
    const userContext = useContext(UserContext);

    if(!userContext) {
        return false;
    }

    const { user } = userContext;

    if(!user) {
        return false;
    }
    
    return Boolean(user);
}