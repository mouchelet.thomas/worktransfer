import { saveAs } from 'file-saver';
import { DelivrableService } from '@work-transfert/services';

const VITE_API_APP_URL = import.meta.env.VITE_API_APP_URL;

export function useDelivrableUploadFile() {


    const handleDownloadAll = async (event: React.MouseEvent<HTMLButtonElement>, id: string) => {
        if (event !== null && event !== undefined) {
            event.stopPropagation();
        }
        const linkPath = `${VITE_API_APP_URL}/api/delivrable-upload-files/download/all/${id}`
        const link = document.createElement("a");
        link.href = linkPath;
        link.target = "_blank";
        link.setAttribute("download", "download.zip");
        document.body.appendChild(link);
        link.click();
        link.remove();
    }

    const handleDownload = async (event: React.MouseEvent<HTMLButtonElement>, id: string) => {
        try {
            const downloadUrl =  await DelivrableService.downloadOneDelivrableUploadFile(id)
            saveAs(downloadUrl)
        } catch (error) {
            console.log(error)
        }
    }

    const showFileSize = (bytes: number) => {
        const sizes = ['Bytes', 'Kb', 'Mb', 'Gb', 'Tb']
        if (bytes === 0) return 'n/a'
        const i = Math.floor(Math.log(bytes) / Math.log(1024))
        if (i === 0) return `${bytes} ${sizes[i]}`
        return `${(bytes / (1024 ** i)).toFixed(1)} ${sizes[i]}`
    }

    return {
        handleDownloadAll,
        handleDownload,
        showFileSize
    }
}