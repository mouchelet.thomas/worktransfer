import api from "./api.service";
import createGenericService from "./generic.service";

const endPoint = "/teachers";

const TeacherService = {
    ...createGenericService(endPoint),
};
  
export default TeacherService;