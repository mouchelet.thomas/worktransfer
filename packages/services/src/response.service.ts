import api from "./api.service";

const create = (data: any) => {
    return api.post("/responses", data)
        .then(res => res.data)
    };
    
const getResponseStatisticalForOneProject = (projectId: string) => {
    return api.get(`/responses/${projectId}/statistical`)
        .then(res => res.data)
}

const ResponseService = {
    create,
    getResponseStatisticalForOneProject
};
  
export default ResponseService;