import api from "./api.service";

const getAll = () => {
    return api.get("/questions")
        .then(res => res.data)
};

const QuestionService = {
    getAll,
};
  
export default QuestionService;