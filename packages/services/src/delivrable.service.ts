import api from "./api.service";
const fileReader = new FileReader();

const create = (id: string, credentials: any) => {
    const formData = new FormData();
    formData.append("delivrableUser[firstName]", credentials.firstName)
    formData.append("delivrableUser[lastName]", credentials.lastName)
    formData.append("project", id)  
    if(credentials.file){
        formData.append("file", credentials.file)
    }
    if( credentials.delivrableLinks ) {
        Object.keys(credentials.delivrableLinks).map(key => {
            formData.append(key, credentials.delivrableLinks[key])
        })
    }

    return api.post('/delivrables', 
        formData, 
        { headers: { 'Content-Type': 'multipart/form-data' } })
        .then(res => res.data)
}

const downloadOneDelivrableUploadFile = (id: string) => {
    return api.get(`/delivrable-upload-files/download/${id}`)
        .then(res => res.data)
}

const downloadAllDelivrableUploadFilesByProjectId = (id: string) => {
    return api.get(`/delivrable-upload-files/download/all/${id}`)
        .then(res => fileReader.readAsDataURL(new Blob([res.data])))
}

const deleteDelivrables = (ids: string[]) => {
    return api.delete(`/delivrables?ids=${ids}`)
        .then(res => res.data)
}

const findOne = (id: string) => {
    return api.get(`/delivrables/${id}`)
        .then(res => res.data)
}

const DelivrableService = {
    findOne,
    create,
    downloadOneDelivrableUploadFile,
    downloadAllDelivrableUploadFilesByProjectId,
    deleteDelivrables
};
  
export default DelivrableService;