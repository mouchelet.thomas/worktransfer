import AuthService from './auth.service';
import UserService from './user.service';
import SchoolService from './school.service';
import TeacherService from './teacher.service';
import SubjectService from './subject.service';
import ProjectService from './project.service';
import DelivrableService from './delivrable.service';
import QuestionService from './question.service';
import ResponseService from './response.service';
import CurriculumService from './curriculum.service';
import StudentClassService from './student-class.service';
import TeacherSchoolService from './teacher-school.service';

export  { 
    AuthService, 
    UserService, 
    SchoolService, 
    TeacherService,
    SubjectService,
    ProjectService,
    DelivrableService,
    QuestionService,
    ResponseService,
    CurriculumService,
    StudentClassService,
    TeacherSchoolService
}