// const getLocalRefreshToken = () => {
//     const user = JSON.parse(localStorage.getItem("accessToken"));
//     return user?.refreshToken;
//   };
  
  const getLocalAccessToken = async () => {
    const user = localStorage.getItem("accessToken")

    return user;
  };
  
  // const updateLocalAccessToken = (token) => {
  //   let user = JSON.parse(localStorage.getItem("accessToken"));
  //   user.token = token;
  //   localStorage.setItem("accessToken", JSON.stringify(user));
  // };
  
//   const getUser = () => {
//     return JSON.parse(localStorage.getItem("accessToken"));
//   };
  
  const setUser = (data: {accessToken: string}) => {
    localStorage.setItem("accessToken", data.accessToken);
  };
  
  const removeUser = () => {
    localStorage.removeItem("accessToken");
  };
  
  const TokenService = {
    getLocalAccessToken,
    setUser,
    removeUser,
    // updateLocalAccessToken
  };
  
  export default TokenService;