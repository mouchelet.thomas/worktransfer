import { JoinSchoolInvitationType } from "@work-transfert/types";
import api from "./api.service";
import createGenericService from "./generic.service";

const endPoint = "/teacher-schools";

const acceptInvitation = (acceptedInvitation: JoinSchoolInvitationType) => {
    return api.post(`${endPoint}/accept`, acceptedInvitation);
}

const TeacherSchoolService = {
    ...createGenericService(endPoint),
    acceptInvitation
};
  
export default TeacherSchoolService;