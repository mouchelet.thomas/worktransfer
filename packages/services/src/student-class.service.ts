import createGenericService from "./generic.service";

const endPoint = "/student-classes";

const StudentClassService = {
    ...createGenericService(endPoint),
};
  
export default StudentClassService;