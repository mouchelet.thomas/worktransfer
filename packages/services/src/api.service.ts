import TokenService from "./token.service";
import axios from "axios";

const VITE_API_APP_URL = import.meta.env.VITE_API_APP_URL;

const instance = axios.create({
  baseURL: `${VITE_API_APP_URL}/api`,
  headers: {
    "Content-Type": "application/json",
  },
});

instance.interceptors.request.use(
  async (config) => {
    const accessToken  = await TokenService.getLocalAccessToken();
  
    if (accessToken) {
      config.headers["Authorization"] = 'Bearer ' + accessToken;  // for Spring Boot back-end
    //   config.headers["x-access-token"] = token; // for Node.js Express back-end
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  (res) => {
    return res;
  },
  async (err) => {
    return Promise.reject(err);
  }
);

export default instance;