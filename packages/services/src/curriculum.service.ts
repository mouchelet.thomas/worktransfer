import createGenericService from "./generic.service";

const endPoint = "/curriculums";

const CurriculumService = {
    ...createGenericService(endPoint),
};
  
export default CurriculumService;