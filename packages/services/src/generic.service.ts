import api from "./api.service";

const createGenericService = (endPoint: string) => {
  const findOne = (id: string) => {
    return api.get(`${endPoint}/${id}`).then((res) => res.data);
  };

  const findAllBySchool = (schoolId: string, queries?: any) => {
    const queryParams = new URLSearchParams(queries).toString();
    return api.get(`${endPoint}/schools/${schoolId}?${queryParams}`).then((res) => res.data);
  };

  const create = (data: any) => {
    if(data instanceof Array) {
      return api.post(`${endPoint}/multiple`, data).then((res) => res.data);
    } else {
      return api.post(endPoint, data).then((res) => res.data);
    }
  };

  const update = (data: any) => {
    return api.patch(`${endPoint}/${data.id}`, data).then((res) => res.data);
  };

  const deleteAllByIds = (ids: string[]) => {
    return api.delete(`${endPoint}?ids=${ids}`).then((res) => res.data);
  };

  return {
    findOne,
    findAllBySchool,
    create,
    update,
    deleteAllByIds,
  };
};

export default createGenericService;