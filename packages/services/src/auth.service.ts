import api from "./api.service";
import TokenService from "./token.service";
import { SingninType, ForgotPasswordType, ResetPasswordType } from "@work-transfert/types"

const signup = (credentials: SingninType) => {
    return api.post("/auth/signup", credentials)
        .then((res) => {
            const data = res.data
            // if (data.accessToken) {
            //     TokenService.setUser(data);
            // }
            return data;
        })
};

const signin = (credentials: SingninType) => {
    return api.post("/auth/signin", credentials)
        .then((res) => {
            const data = res.data
            
            if (data && data.accessToken) {
                TokenService.setUser(data);
            }
            
            return data;
        })
};

const regenerateToken = () => {
    return api.get("/auth/regenerate-token")
        .then((res) => {
            const data = res.data
            
            if (data.accessToken) {
                TokenService.setUser(data);
            }
            
            return data;
        })
};

const signout = () => {
    TokenService.removeUser();
};

const forgotPassword = (credentials: ForgotPasswordType) => {
    return api.post("/auth/forgot-password", credentials)
        .then((res) => res.data)
}

const resetPassword = (credentials: ResetPasswordType) => {
    return api.post("/auth/reset-password", credentials)
        .then((res) => res.data)
}

const AuthService = {
    resetPassword,
    forgotPassword,
    signup,
    signin,
    signout, 
    regenerateToken
};
  
export default AuthService;