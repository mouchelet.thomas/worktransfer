import api from "./api.service";

const endPoint = "/schools";

const findAll = () => {
    return api.get(endPoint)
        .then(res => res.data)
};

const create = (data: any) => {
    return api.post(endPoint, data)
        .then(res => res.data)
}

const update = (data: any) => {
    return api.put(`${endPoint}/${data.id}`, data)
        .then(res => res.data)
}

const SchoolService = {
    findAll,
    create,
    update
};
  
export default SchoolService;