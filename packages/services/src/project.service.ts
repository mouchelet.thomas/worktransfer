import api from "./api.service";
import createGenericService from "./generic.service";

const endPoint = "/projects";

const ProjectService = {
    ...createGenericService(endPoint),
};
  
export default ProjectService;