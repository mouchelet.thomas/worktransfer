import { StudentClassType } from "@work-transfert/types";
import api from "./api.service";
import createGenericService from "./generic.service";

const endPoint = "/subjects";

const findAddlSubjectsByStudentClassId = (studentClasses: StudentClassType[]) => {
    let studentClassesIds: string[] = []
    studentClasses.map((studentClass) => {
        studentClassesIds.push(studentClass.id);
    });
    return api.get(`${endPoint}/student-classes?ids=${studentClassesIds}`);
}

const SubjectService = {
    ...createGenericService(endPoint),
    findAddlSubjectsByStudentClassId
};
  
export default SubjectService;