import React, { createContext, ReactNode, useContext, useEffect, useState } from "react";
import { SchoolType } from "@work-transfert/types";
import { SchoolService } from "@work-transfert/services";
import { UserContext } from "./UserContext";

interface SchoolContextType {
  school: any; // Remplacez `any` par le type approprié pour l'utilisateur
  setSchool: React.Dispatch<React.SetStateAction<any>>; // Remplacez `any` par le type approprié pour l'utilisateur
  schools: SchoolType[]
  fetchSchools: () => void;
}

interface SchoolProviderProps {
  children: ReactNode;
}

const SchoolContext = createContext<SchoolContextType>({
  school: null,
  setSchool: () => null,
  schools: [],
  fetchSchools: () => null
});

const SchoolContextProvider: React.FC<SchoolProviderProps>  = ({ children }) => {
  const [school, setSchool] = useState(null);
  const [schools, setSchools] = useState<SchoolType[]>([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    if(!user) return;
    fetchSchools()
  }, [user])

  const fetchSchools = async () => {
    try {
      const response = await SchoolService.findAll()
      setSchools([...response.data])
      setSchool(response.data[0])
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <SchoolContext.Provider 
      value={{ 
        school, 
        setSchool,
        schools,
        fetchSchools
      }}
    >
      {children}
    </SchoolContext.Provider>
  );
};

export { SchoolContext, SchoolContextProvider };