import React, { createContext, ReactNode, useState } from "react";

interface UserContextType {
  user: any; // Remplacez `any` par le type approprié pour l'utilisateur
  setUser: React.Dispatch<React.SetStateAction<any>>; // Remplacez `any` par le type approprié pour l'utilisateur
}

interface UserProviderProps {
  children: ReactNode;
}

const UserContext = createContext<UserContextType>({
  user: null,
  setUser: () => null,
});

const UserContextProvider: React.FC<UserProviderProps>  = ({ children }) => {
  const [user, setUser] = useState(null);

  return (
    <UserContext.Provider value={{ user, setUser }}>
      {children}
    </UserContext.Provider>
  );
};

export { UserContext, UserContextProvider };